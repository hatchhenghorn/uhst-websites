<!DOCTYPE html>
<html lang="en" class="light-style  customizer-hide" dir="ltr" data-theme="theme-default" data-assets-path="../../backend-assets/" data-template="vertical-menu-template">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title>403 - Pages | University of Heng Samrin Thbongkhmum</title>

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{ asset('backend-assets/img/favicon/favicon.ico') }}" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&amp;ampdisplay=swap" rel="stylesheet">

    <!-- Icons -->
    <link rel="stylesheet" href="{{ asset('backend-assets/vendor/fonts/materialdesignicons.css') }}" />
    <link rel="stylesheet" href="{{ asset('backend-assets/vendor/fonts/fontawesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('backend-assets/vendor/fonts/flag-icons.css') }}" />

    <!-- Menu waves for no-customizer fix -->
    <link rel="stylesheet" href="{{ asset('backend-assets/vendor/libs/node-waves/node-waves.css') }}" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="{{ asset('backend-assets/vendor/css/rtl/core.css') }}" class="template-customizer-core-css" />
    <link rel="stylesheet" href="{{ asset('backend-assets/vendor/css/rtl/theme-default.css') }}" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="{{ asset('backend-assets/css/demo.css') }}" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{{ asset('backend-assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') }}" />
    <link rel="stylesheet" href="{{ asset('backend-assets/vendor/libs/typeahead-js/typeahead.css') }}" />

    <!-- Page CSS -->
    <!-- Page -->
    <link rel="stylesheet" href="{{ asset('backend-assets/vendor/css/pages/page-misc.css') }}">

    <!-- Helpers -->
    <script src="{{ asset('backend-assets/vendor/js/helpers.js') }}"></script>
    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Template customizer: To hide customizer set displayCustomizer value false in config.js.  -->
    <script src="{{ asset('backend-assets/vendor/js/template-customizer.js') }}"></script>
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="{{ asset('backend-assets/js/config.js') }}"></script>

</head>
<body>
<!-- Content -->

<!-- Not Authorized -->
<div class="misc-wrapper">
    <h1 class="mb-2 mx-2" style="font-size: 6rem;">403</h1>
    <h4 class="mb-2">You are not authorized! 🔐</h4>
    <p class="mb-2 mx-2">You don’t have permission to access this page. Go Home!</p>
    <div class="d-flex justify-content-center mt-5">
        <img src="{{ asset('backend-assets/img/illustrations/tree-2.png') }}" alt="misc-tree" class="img-fluid misc-object d-none d-lg-inline-block" width="80">
        <img src="{{ asset('backend-assets/img/illustrations/misc-mask-light.png') }}" alt="misc-error" class="scaleX-n1-rtl misc-bg d-none d-lg-inline-block" data-app-light-img="illustrations/misc-mask-light.png" data-app-dark-img="illustrations/misc-mask-dark.png">
        <div class="d-flex flex-column align-items-center">
            <img src="{{ asset('backend-assets/img/illustrations/401.png') }}" alt="misc-error" class="misc-model img-fluid zindex-1" width="780">
            <div>
                <a href="{{ route('admin.dashboard') }}" class="btn btn-primary text-center my-4">Back to home</a>
            </div>
        </div>
    </div>
</div>
<!-- /Not Authorized -->

<!-- / Content -->

<!-- Core JS -->
<!-- build:js assets/vendor/js/core.js -->
<script src="{{ asset('backend-assets/vendor/libs/jquery/jquery.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/popper/popper.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/js/bootstrap.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/node-waves/node-waves.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/hammer/hammer.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/i18n/i18n.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/typeahead-js/typeahead.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/js/menu.js') }}"></script>

<!-- endbuild -->

<!-- Vendors JS -->


<!-- Main JS -->
<script src="{{ asset('backend-assets/js/main.js') }}"></script>

<!-- Page JS -->


</body>
</html>
