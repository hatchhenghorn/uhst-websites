<!-- Libs JS -->
<script src="{{ asset('frontend-assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('frontend-assets/libs/simplebar/dist/simplebar.min.js') }}"></script>
<script src="{{ asset('frontend-assets/libs/headhesive/dist/headhesive.min.js') }}"></script>

<!-- Theme JS -->
<script src="{{ asset('frontend-assets/js/theme.min.js') }}"></script>

<script src="{{ asset('frontend-assets/libs/scrollcue/scrollCue.min.js') }}"></script>
<script src="{{ asset('frontend-assets/js/vendors/scrollcue.js') }}"></script>

<script src="{{ asset('frontend-assets/libs/parallax-js/dist/parallax.min.js') }}"></script>
<script src="{{ asset('frontend-assets/js/vendors/parallax.js') }}"></script>
<script src="{{ asset('frontend-assets/libs/rellax/rellax.min.js') }}"></script>
<script src="{{ asset('frontend-assets/js/vendors/rellax.js') }}"></script>
<!-- Swiper JS -->
<script src="{{ asset('frontend-assets/libs/swiper/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('frontend-assets/js/vendors/swiper.js') }}"></script>
<script src="{{ asset('frontend-assets/libs/scrollcue/scrollCue.min.js') }}"></script>
<script src="{{ asset('frontend-assets/js/vendors/scrollcue.js') }}"></script>
