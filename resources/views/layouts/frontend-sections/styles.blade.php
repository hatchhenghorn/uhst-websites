<!-- Color modes -->
<script src="{{ asset('frontend-assets/js/vendors/color-modes.js') }}"></script>

<!-- Libs CSS -->
<link href="{{ asset('frontend-assets/libs/simplebar/dist/simplebar.min.css') }}" rel="stylesheet" />
<link href="{{ asset('frontend-assets/libs/bootstrap-icons/font/bootstrap-icons.min.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('frontend-assets/libs/swiper/swiper-bundle.min.css') }}" />

<!-- Scroll Cue -->
<link rel="stylesheet" href="{{ asset('frontend-assets/libs/scrollcue/scrollCue.css') }}" />

<!-- Box icons -->
<link rel="stylesheet" href="{{ asset('frontend-assets/fonts/css/boxicons.min.css') }}" />

<!-- Theme CSS -->
<link rel="stylesheet" href="{{ asset('frontend-assets/css/theme.min.css') }}" />
<link rel="stylesheet" href="{{ asset('frontend-assets/css/site.css') }}" />

