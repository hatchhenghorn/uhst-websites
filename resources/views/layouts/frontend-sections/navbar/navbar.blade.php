<!-- Navbar -->
<header>
    <nav class="navbar navbar-expand-lg transparent navbar-transparent navbar-dark">
        <div class="container-fluid px-3">
            <a class="navbar-brand d-flex" href="{{ route('home') }}">
                <span><img src="{{ asset('backend-assets/img/logo/uhst-logo.png') }}" width="30" alt="UHST Logo" /></span>
                <span class="ms-2 fw-bold">USHT</span>
            </a>
            <button class="navbar-toggler offcanvas-nav-btn" type="button">
                <i class="bi bi-list"></i>
            </button>
            <div class="offcanvas offcanvas-start offcanvas-nav" style="width: 20rem">
                <div class="offcanvas-header">
                    <a href="{{ route('home') }}" class="text-inverse d-flex align-items-center">
                        <img src="{{ asset('backend-assets/img/logo/uhst-logo.png') }}" width="30" alt="UHST Logo" />
                        <span class="ms-2 fw-bold">USHT</span>
                    </a>
                    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body pt-0 align-items-center">
                    <ul class="navbar-nav ms-auto align-items-lg-center">
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="{{ route('home') }}">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">About</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('history') }}">History</a></li>
                                <li><a class="dropdown-item" href="{{ route('mission-vision') }}">Mission & Vision</a></li>
                                <li><a class="dropdown-item" href="{{ route('president-message') }}">President Message</a></li>
                                <li><a class="dropdown-item" href="{{ route('partner') }}">Partner</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Faculty / Institute</a>
                            <ul class="dropdown-menu">
                                @php
                                    $faculties = \App\Models\Faculty::getFaculty();
                                    $institutes = \App\Models\Institute::getInstitute();
                                @endphp

                                @foreach($faculties as $faculty)
                                    <li><a class="dropdown-item" href="{{ route('faculty', $faculty->id) }}">{{ $faculty->title_en }}</a></li>
                                @endforeach
                                @foreach($institutes as $institute)
                                    <li><a class="dropdown-item" href="{{ route('institute', $institute->id) }}">{{ $institute->title_en }}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Study</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('admission') }}">Admission</a></li>
                                <li><a class="dropdown-item" href="">Scholarship</a></li>
                                <li><a class="dropdown-item" href="">Short Courses</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Research & Innovation</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route('project') }}">Projects</a></li>
                                <li><a class="dropdown-item" href="{{ route('publication-book') }}">Publications Book</a></li>
                                <li><a class="dropdown-item" href="{{ route('training-workshop') }}">Training & Workshop</a></li>
                                <li><a class="dropdown-item" href="{{ route('conference') }}">Conferences</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="{{ route('contact-location') }}">Contact & Location</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="{{ route('news-activity') }}">News & Activities</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="{{ route('career') }}">Careers</a>
                        </li>
                    </ul>
                    {{--<div class="mt-3 mt-lg-0 d-flex align-items-center">
                        <a href="" class="btn btn-light mx-2">Login</a>
                        <a href="" class="btn btn-primary">Create account</a>
                    </div>--}}
                </div>
            </div>
        </div>
    </nav>
</header>
