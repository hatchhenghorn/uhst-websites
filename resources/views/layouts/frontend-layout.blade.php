<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Favicon icon-->
    <link rel="shortcut icon" href="{{ asset('backend-assets/img/favicon/favicon.ico') }}" />
    <meta name="msapplication-TileColor" content="#8b3dff" />
    <meta name="msapplication-config" content="assets/images/favicon/tile.xml" />

    <!--Includes Styles  -->
    @include('layouts.frontend-sections.styles')

    <!-- Include Scripts for customizer, helper, analytics, config -->
    @include('layouts.frontend-sections.scripts-includes')

    <title>@yield('title') | University of Heng Samrin Thbongkhmum</title>
</head>

<body>
<!-- Includes Navbar -->
@include('layouts.frontend-sections.navbar.navbar')

<main>
    <!-- Includes Content -->
    @yield('content')
</main>

<!-- Includes Footer -->
@include('layouts.frontend-sections.footer.footer')

<!-- Scroll top -->
<div class="btn-scroll-top">
    <svg class="progress-square svg-content" width="100%" height="100%" viewBox="0 0 40 40">
        <path d="M8 1H32C35.866 1 39 4.13401 39 8V32C39 35.866 35.866 39 32 39H8C4.13401 39 1 35.866 1 32V8C1 4.13401 4.13401 1 8 1Z" />
    </svg>
</div>

<!-- Includes Scripts -->
@include('layouts.frontend-sections.scripts')

@yield('scripts')

</body>
</html>
