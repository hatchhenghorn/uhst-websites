<!-- Menu -->
<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
        <a href="{{ route('admin.dashboard') }}" class="app-brand-link">
            <span class="app-brand-logo demo me-1">
                <img src="{{ asset('backend-assets/img/logo/uhst-logo.png') }}" width="30" alt="uhst logo">
            </span>
            <span class="app-brand-text demo menu-text fw-semibold ms-2">UHST Website</span>
        </a>

        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto">
            <i class="mdi menu-toggle-icon d-xl-block align-middle mdi-20px"></i>
        </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
        <!-- Dashboard -->
        <li class="menu-item dashboard-main-menu">
            <a href="{{ route('admin.dashboard') }}" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-view-dashboard-outline"></i>
                <div data-i18n="Dashboard">Dashboard</div>
            </a>
        </li>
        <!-- Manage User -->
        @if(Auth::user()->canAny(['Staff.Menu']))
            <li class="menu-item administrator-main-menu">
                <a href="javascript:void(0);" class="menu-link menu-toggle">
                    <i class="menu-icon tf-icons mdi mdi-account-multiple-outline"></i>
                    <div data-i18n="Manage User">Manage User</div>
                </a>
                <ul class="menu-sub">
                    <!-- Staff -->
                    @if(Auth::user()->can('Staff.Menu'))
                        <li class="menu-item staff-sub-menu">
                            <a href="{{ route('staff') }}" class="menu-link">
                                <div data-i18n="Staff">Staff</div>
                            </a>
                        </li>
                    @endif
                    <!-- Staff Type -->
                    @if(Auth::user()->can('Staff-Type.Menu'))
                        <li class="menu-item staff-type-sub-menu">
                            <a href="{{ route('staff-types') }}" class="menu-link">
                                <div data-i18n="Staff Type">Staff Type</div>
                            </a>
                        </li>
                    @endif
                </ul>
            </li>
        @endif
        <!-- Role & Permission -->
        @if(Auth::user()->can('Role.Menu'))
            <li class="menu-item role-permission-main-menu">
                <a href="javascript:void(0);" class="menu-link menu-toggle">
                    <i class="menu-icon tf-icons mdi mdi-account-star-outline"></i>
                    <div data-i18n="Role & Permission">Role & Permission</div>
                </a>
                <ul class="menu-sub">
                    <!-- Role -->
                    <li class="menu-item role-sub-menu">
                        <a href="{{ route('roles') }}" class="menu-link">
                            <div data-i18n="Role">Role</div>
                        </a>
                    </li>
                    <!-- Permission -->
                    <li class="menu-item permission-sub-menu">
                        <a href="{{ route('permissions') }}" class="menu-link">
                            <div data-i18n="Permission">Permission</div>
                        </a>
                    </li>
                    <!-- Role in Permission -->
                    <li class="menu-item role-permission-sub-menu">
                        <a href="{{ route('role-permissions') }}" class="menu-link">
                            <div data-i18n="Role in Permission">Role in Permission</div>
                        </a>
                    </li>
                </ul>
            </li>
        @endif
        <!-- About Us -->
        @if(Auth::user()->canAny(['History.Menu', 'Mission-Vision.Menu', 'PresidentMessage.Menu', 'Partner.Menu']))
            <li class="menu-item about-us-main-menu">
                <a href="javascript:void(0);" class="menu-link menu-toggle">
                    <i class="menu-icon tf-icons mdi mdi-information-outline"></i>
                    <div data-i18n="About Us">About Us</div>
                </a>
                <ul class="menu-sub">
                    <!-- History -->
                    @if(Auth::user()->can('History.Menu'))
                        <li class="menu-item history-sub-menu">
                            <a href="{{ route('histories') }}" class="menu-link">
                                <div data-i18n="History">History</div>
                            </a>
                        </li>
                    @endif
                    <!-- Mission & Vision -->
                    @if(Auth::user()->can('Mission-Vision.Menu'))
                        <li class="menu-item mission-vision-sub-menu">
                            <a href="{{ route('mission-visions') }}" class="menu-link">
                                <div data-i18n="Mission & Vision">Mission & Vision</div>
                            </a>
                        </li>
                    @endif
                    <!-- President Message -->
                    @if(Auth::user()->can('PresidentMessage.Menu'))
                        <li class="menu-item president-message-sub-menu">
                            <a href="{{ route('president-messages') }}" class="menu-link">
                                <div data-i18n="President Message">President Message</div>
                            </a>
                        </li>
                    @endif
                    <!-- Partner -->
                    @if(Auth::user()->can('Partner.Menu'))
                        <li class="menu-item partner-sub-menu">
                            <a href="{{ route('partners') }}" class="menu-link">
                                <div data-i18n="Partner">Partner</div>
                            </a>
                        </li>
                    @endif
                </ul>
            </li>
        @endif
        <!-- Faculty -->
        @if(Auth::user()->can('Faculty.Menu'))
            <li class="menu-item faculty-main-menu">
                <a href="{{ route('faculties') }}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-office-building-outline"></i>
                    <div data-i18n="Faculty">Faculty</div>
                </a>
            </li>
        @endif
        <!-- Institute -->
        @if(Auth::user()->canAny(['Institute.Menu', 'Major.Menu', 'Course.Menu', 'Major-Course.Menu']))
            <li class="menu-item institute-main-menu">
                <a href="javascript:void(0);" class="menu-link menu-toggle">
                    <i class="menu-icon tf-icons mdi mdi mdi-domain"></i>
                    <div data-i18n="Institute">Institute</div>
                </a>
                <ul class="menu-sub">
                    <!-- Institute -->
                    @if(Auth::user()->can('Institute.Menu'))
                        <li class="menu-item institute-sub-menu">
                            <a href="{{ route('institutes') }}" class="menu-link">
                                <div data-i18n="Institute">Institute</div>
                            </a>
                        </li>
                    @endif
                    <!-- Major -->
                    @if(Auth::user()->can('Major.Menu'))
                        <li class="menu-item major-sub-menu">
                            <a href="{{ route('majors') }}" class="menu-link">
                                <div data-i18n="Major">Major</div>
                            </a>
                        </li>
                    @endif
                    <!-- Course -->
                    @if(Auth::user()->can('Course.Menu'))
                        <li class="menu-item course-sub-menu">
                            <a href="{{ route('courses') }}" class="menu-link">
                                <div data-i18n="Course">Course</div>
                            </a>
                        </li>
                    @endif
                    <!-- Major & Course -->
                    @if(Auth::user()->can('Major-Course.Menu'))
                        <li class="menu-item major-course-sub-menu">
                            <a href="{{ route('major-courses') }}" class="menu-link">
                                <div data-i18n="Major Course">Major & Course</div>
                            </a>
                        </li>
                    @endif
                </ul>
            </li>
        @endif
        <!-- Study -->
        @if(Auth::user()->canAny(['Admission.Menu']))
            <li class="menu-item study-main-menu">
                <a href="javascript:void(0);" class="menu-link menu-toggle">
                    <i class="menu-icon tf-icons mdi mdi-account-school-outline"></i>
                    <div data-i18n="Study">Study</div>
                </a>
                <ul class="menu-sub">
                    <!-- Admission -->
                    @if(Auth::user()->can('Admission.Menu'))
                        <li class="menu-item admission-sub-menu">
                            <a href="{{ route('admissions') }}" class="menu-link">
                                <div data-i18n="Admission">Admission</div>
                            </a>
                        </li>
                    @endif
                    <!-- Scholarship -->
                    {{--<li class="menu-item">
                        <a href="" class="menu-link">
                            <div data-i18n="Scholarship">Scholarship</div>
                        </a>
                    </li>
                    <!-- Associative Programs -->
                    <li class="menu-item">
                        <a href="" class="menu-link">
                            <div data-i18n="Associative Programs">Associative Programs</div>
                        </a>
                    </li>
                    <!-- Bachelor’s Programs -->
                    <li class="menu-item">
                        <a href="" class="menu-link">
                            <div data-i18n="Bachelor’s Programs">Bachelor’s Programs</div>
                        </a>
                    </li>
                    <!-- Master’s Programs -->
                    <li class="menu-item">
                        <a href="" class="menu-link">
                            <div data-i18n="Master’s Programs">Master’s Programs</div>
                        </a>
                    </li>
                    <!-- Short Courses -->
                    <li class="menu-item">
                        <a href="" class="menu-link">
                            <div data-i18n="Short Courses">Short Courses</div>
                        </a>
                    </li>--}}
                </ul>
            </li>
        @endif
        <!-- Research & Innovation -->
        @if(Auth::user()->canAny(['Project.Menu', 'PublicationBook.Menu', 'Training-Workshop.Menu', 'Conference.Menu']))
            <li class="menu-item research-innovation-main-menu">
                <a href="javascript:void(0);" class="menu-link menu-toggle">
                    <i class="menu-icon tf-icons mdi mdi-book-search-outline"></i>
                    <div data-i18n="Research & Innovation">Research & Innovation</div>
                </a>
                <ul class="menu-sub">
                    <!-- Projects -->
                    @if(Auth::user()->can('Project.Menu'))
                        <li class="menu-item project-sub-menu">
                            <a href="{{ route('projects') }}" class="menu-link">
                                <div data-i18n="Projects">Projects</div>
                            </a>
                        </li>
                    @endif
                    <!-- Publication Books -->
                    @if(Auth::user()->can('PublicationBook.Menu'))
                        <li class="menu-item publications-book-sub-menu">
                            <a href="{{ route('publication-books') }}" class="menu-link">
                                <div data-i18n="Publication Books">Publication Books</div>
                            </a>
                        </li>
                    @endif
                    <!-- Training & Workshop -->
                    @if(Auth::user()->can('Training-Workshop.Menu'))
                        <li class="menu-item training-workshop-sub-menu">
                            <a href="{{ route('training-workshops') }}" class="menu-link">
                                <div data-i18n="Training & Workshop">Training & Workshop</div>
                            </a>
                        </li>
                    @endif
                    <!-- Conferences -->
                    @if(Auth::user()->can('Conference.Menu'))
                        <li class="menu-item conferences-sub-menu">
                            <a href="{{ route('conferences') }}" class="menu-link">
                                <div data-i18n="Conferences">Conferences</div>
                            </a>
                        </li>
                    @endif
                </ul>
            </li>
        @endif
        <!-- Contact & Location -->
        @if(Auth::user()->can('Contact-Location.Menu'))
            <li class="menu-item contact-locations-main-menu">
                <a href="{{ route('contact-locations') }}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-card-account-phone-outline"></i>
                    <div data-i18n="Contact & Location">Contact & Location</div>
                </a>
            </li>
        @endif
        <!-- News & Activities -->
        @if(Auth::user()->can('News-Activities.Menu'))
            <li class="menu-item news-activities-main-menu">
                <a href="{{ route('news-activities') }}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-newspaper-variant-outline"></i>
                    <div data-i18n="News & Activities">News & Activities</div>
                </a>
            </li>
        @endif
        <!-- Careers -->
        @if(Auth::user()->can('Career.Menu'))
            <li class="menu-item career-main-menu">
                <a href="{{ route('careers') }}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-briefcase-account-outline"></i>
                    <div data-i18n="Careers">Careers</div>
                </a>
            </li>
        @endif
    </ul>
</aside>
<!-- / Menu -->
