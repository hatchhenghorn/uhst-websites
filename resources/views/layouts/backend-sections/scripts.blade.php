<!-- Core JS -->
<!-- build:js assets/vendor/js/core.js -->
<script src="{{ asset('backend-assets/vendor/libs/jquery/jquery.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/popper/popper.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/js/bootstrap.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/node-waves/node-waves.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/hammer/hammer.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/i18n/i18n.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/typeahead-js/typeahead.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/js/menu.js') }}"></script>

<!-- endbuild -->

<!-- Vendors JS -->
<script src="{{ asset('backend-assets/vendor/libs/toastr/toastr.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/sweetalert2/sweetalert2.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/flatpickr/flatpickr.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/dropify/js/dropify.min.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/filepond/js/filepond-plugin-image-preview.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/filepond/js/filepond.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/datatables-bs5/datatables-bootstrap5.js') }}"></script>
<script src="{{ asset('backend-assets/vendor/libs/select2/select2.js') }}"></script>

<!-- Main JS -->
<script src="{{ asset('backend-assets/js/main.js') }}"></script>


<!-- Page JS -->
<script>
    $(document).ready(function () {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        @if(Session::get('message'))
            let type = "{{ Session::get('alert-type', 'info') }}";

            switch (type) {
                case 'info':
                    toastr.info("{{ Session::get('message') }}");
                    break;
                case 'success':
                    toastr.success("{{ Session::get('message') }}");
                    break;
                case 'warning':
                    toastr.warning("{{ Session::get('message') }}");
                    break;
                case 'error':
                    toastr.error("{{ Session::get('message') }}");
                    break;
            }
        @endif

        $(document).on("click", ".btn-delete", function (e) {
            e.preventDefault();
            let link = $(this).attr("href");

            Swal.fire({
                title: 'Are you sure?',
                text: "You want to delete this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Delete!',
                customClass: {
                    confirmButton: 'btn btn-primary me-1 waves-effect waves-light',
                    cancelButton: 'btn btn-outline-secondary waves-effect'
                },
                buttonsStyling: false
            }).then(function(result) {
                if (result.value) {
                    window.location.href = link;
                    Swal.fire({
                        icon: 'success',
                        title: 'Deleted!',
                        text: 'Data deleted successfully.',
                        customClass: {
                            confirmButton: 'btn btn-success waves-effect'
                        }
                    });
                }
            });
        });
    });
</script>
