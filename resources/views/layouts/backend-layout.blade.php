<!DOCTYPE html>
<html lang="en" class="light-style layout-navbar-fixed layout-menu-fixed layout-compact" dir="ltr" data-theme="theme-default" data-assets-path="../../backend-assets/" data-template="vertical-menu-template">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title>@yield('title') | University of Heng Samrin Thbongkhmum</title>

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{ asset('backend-assets/img/favicon/favicon.ico') }}" />

    <!-- Include Styles -->
    @include('layouts.backend-sections.styles')

    <!-- Include Scripts for customizer, helper, analytics, config -->
    @include('layouts.backend-sections.scripts-includes')
</head>

<body>
<!-- Layout wrapper -->
<div class="layout-wrapper layout-content-navbar">
    <!-- Loading -->
    <div class="loading-content">
        <div class="loader"></div>
    </div>
    <div class="layout-container">
        <!-- Include Menu -->
        @include('layouts.backend-sections.menu.menu')

        <!-- Layout container -->
        <div class="layout-page">
            <!-- Include Navbar -->
            @include('layouts.backend-sections.navbar.navbar')

            <!-- Content wrapper -->
            <div class="content-wrapper">
                <!-- Include Content -->
                @yield('content')

                <!-- Include Footer -->
                @include('layouts.backend-sections.footer.footer')

                <div class="content-backdrop fade"></div>
            </div>
            <!-- Content wrapper -->
        </div>
        <!-- / Layout page -->
    </div>

    <!-- Overlay -->
    <div class="layout-overlay layout-menu-toggle"></div>

    <!-- Drag Target Area To SlideIn Menu On Small Screens -->
    <div class="drag-target"></div>

</div>
<!-- / Layout wrapper -->

<!-- Include Scripts -->
@include('layouts.backend-sections.scripts')
<script>
    $(document).ready(function () {
        $('.loading-content').addClass('d-none');
        // setTimeout(function () {
        //     $('.loading-content').addClass('d-none');
        // }, 1000);
    });
</script>
@yield('scripts')
</body>
</html>
