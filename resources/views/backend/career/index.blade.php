@extends('layouts.backend-layout')

@section('title', 'Career')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="card-title">Career</h5>
                @if(Auth::user()->can('Career.Create'))
                    <a href="{{ route('careers.create') }}" class="btn btn-primary">Add New</a>
                @endif
            </div>
            <div class="card-datatable table-responsive text-nowrap">
                <table class="table">
                    <thead class="table-light">
                        <tr>
                            <th>#</th>
                            <th>Actions</th>
                            <th>Image</th>
                            <th>Career Name(EN)</th>
                            <th>Career Name(KH)</th>
                            <th>Position</th>
                            <th>Company</th>
                            <th>Website</th>
                            <th>Email</th>
                            <th>Tel</th>
                            <th>Location</th>
                            <th>Total</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Description</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if($careers->count() > 0)
                        @foreach($careers as $key => $item)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>
                                    @if(Auth::user()->can('Career.Edit'))
                                        <a href="{{ route('careers.edit',$item->id) }}" class="btn btn-sm btn-icon me-2 btn-primary">
                                            <span class="tf-icons mdi mdi-pencil"></span>
                                        </a>
                                    @endif
                                    @if(Auth::user()->can('Career.Delete'))
                                        <a href="{{ route('careers.delete',$item->id) }}" class="btn btn-sm btn-icon me-2 btn-danger btn-delete">
                                            <span class="tf-icons mdi mdi-trash-can"></span>
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    <div class="avatar me-2">
                                        <img src="{{ url('uploads/career/'.$item->image) }}" onclick="openImageInNewTabCentered('{{ url('uploads/career/'.$item->image) }}')" alt="Avatar" class="rounded">
                                    </div>
                                </td>
                                <td>{{ $item->title_en }}</td>
                                <td>{{ $item->title_kh }}</td>
                                <td>{{ $item->position }}</td>
                                <td>{{ $item->company }}</td>
                                <td>{{ $item->webstie }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->tel }}</td>
                                <td>{{ $item->location }}</td>
                                <td>{{ $item->number_of_require }}</td>
                                <td>{{ date('d/M/Y', strtotime($item->start_date)) }}</td>
                                <td>{{ date('d/M/Y', strtotime($item->end_date)) }}</td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-outline-primary" onclick="previewDescription('{{ $item->description }}')">Preview Mission</button>
                                </td>
                                <td>
                                    <span class="badge {{ $item->activation === 1 ? 'bg-label-success' : 'bg-label-danger' }}">
                                        {{ $item->activation === 1 ? 'Active' : 'Inactive' }}
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="text-center" colspan="16">No data is available in table</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="mx-3">
                {!! $careers->withQueryString()->links('vendor.pagination.bootstrap-5') !!}
            </div>
        </div>

        <!-- Modal Preview -->
        <div class="modal fade" id="modalPreview" tabindex="-1" aria-labelledby="modalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalPreviewTitle">Preview Description</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="preview-data"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.career-main-menu').addClass('active');
        });

        function openImageInNewTabCentered(imageUrl) {
            // Define the width and height for the new window
            const newWindowWidth = 600; // Width in pixels
            const newWindowHeight = 400; // Height in pixels

            // Calculate the position to center the window
            const leftPosition = (screen.width - newWindowWidth) / 2;
            const topPosition = (screen.height - newWindowHeight) / 2;

            // Open the image URL in a new tab with centered position and specified dimensions
            window.open(imageUrl, '_blank', `width=${newWindowWidth}, height=${newWindowHeight}, top=${topPosition}, left=${leftPosition}`);
        }

        function previewDescription(value) {
            $('#modalPreview').modal('show');
            $('.preview-data').html(value);
        }
    </script>
@endsection
