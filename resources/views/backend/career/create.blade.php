@extends('layouts.backend-layout')

@section('title', 'Create Career')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="card-title">Create Career</h5>
                <a href="{{ route('careers') }}" class="btn btn-secondary">Back</a>
            </div>
            <div class="card-body">
                <form action="{{ route('careers.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-lg-12 col-md-12 mb-4">
                            <label for="input-file-max-fs">Maximum file upload size 2MB. (width-1080px & height-720px)</label>
                            <input
                                type="file"
                                id="input-file-max-fs"
                                name="image"
                                class="dropify"
                                data-max-file-size="2M"
                            />
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('title_en') is-invalid @enderror"
                                    id="title_en"
                                    name="title_en"
                                    value="{{ old('title_en') }}"
                                    placeholder="Enter Career Name(EN)"
                                />
                                <label for="title_en">Career Name(EN)</label>
                                @error('title_en')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('title_kh') is-invalid @enderror"
                                    id="title_kh"
                                    name="title_kh"
                                    value="{{ old('title_kh') }}"
                                    placeholder="Enter Career Name(KH)"
                                />
                                <label for="title_kh">Career Name(KH)</label>
                                @error('title_kh')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('position') is-invalid @enderror"
                                    id="position"
                                    name="position"
                                    value="{{ old('position') }}"
                                    placeholder="Enter Position"
                                />
                                <label for="position">Position</label>
                                @error('position')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('company') is-invalid @enderror"
                                    id="company"
                                    name="company"
                                    value="{{ old('company') }}"
                                    placeholder="Enter Company"
                                />
                                <label for="company">Company</label>
                                @error('position')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control"
                                    id="website"
                                    name="website"
                                    placeholder="Enter Website"
                                />
                                <label for="website">Website</label>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    id="email"
                                    name="email"
                                    class="form-control @error('email') is-invalid @enderror"
                                    value="{{ old('email') }}"
                                    placeholder="Enter Email"
                                    aria-label="Enter Email"
                                    aria-describedby="email2"
                                />
                                <label for="email">Email</label>
                                @error('email')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('tel') is-invalid @enderror"
                                    id="tel"
                                    name="tel"
                                    value="{{ old('tel') }}"
                                    placeholder="Enter Tel"
                                />
                                <label for="tel">Tel</label>
                                @error('tel')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('location') is-invalid @enderror"
                                    id="location"
                                    name="location"
                                    value="{{ old('location') }}"
                                    placeholder="Enter Location"
                                />
                                <label for="location">Location</label>
                                @error('location')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="number"
                                    class="form-control @error('number_of_require') is-invalid @enderror"
                                    id="number_of_require"
                                    name="number_of_require"
                                    value="{{ old('number_of_require') }}"
                                    placeholder="Enter Number of Require"
                                    min="0"
                                />
                                <label for="number_of_require">Number of Require</label>
                                @error('number_of_require')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('start_date') is-invalid @enderror flatpickr-date"
                                    id="start_date"
                                    name="start_date"
                                    value="{{ old('start_date') }}"
                                    placeholder="DD-MM-YYYY"
                                />
                                <label for="start_date">Start Date</label>
                                @error('start_date')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('end_date') is-invalid @enderror flatpickr-date"
                                    id="end_date"
                                    name="end_date"
                                    value="{{ old('end_date') }}"
                                    placeholder="DD-MM-YYYY"
                                />
                                <label for="end_date">End Date</label>
                                @error('end_date')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 mb-4">
                            <textarea
                                class="form-control @error('description') is-invalid @enderror"
                                id="description"
                                name="description"
                                placeholder="Enter Description"
                            >
                                {{ old('description') }}
                            </textarea>
                            @error('description')
                            <div class="invalid-feedback"> {{ $message }} </div>
                            @enderror
                        </div>
                        <div class="col-lg-12 col-md-12 mb-4">
                            <div class="form-check form-check-primary">
                                <input
                                    class="form-check-input"
                                    type="checkbox"
                                    value="1"
                                    id="activation"
                                    name="activation"
                                    checked
                                />
                                <label class="form-check-label" for="activation">Activation</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                </form>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.career-main-menu').addClass('active');

            /*FilePond.registerPlugin(
                FilePondPluginImagePreview,
                /!*FilePondPluginImageExifOrientation,
                FilePondPluginFileValidateSize,
                FilePondPluginImageEdit*!/
            );

            // Select the file input and use
            // create() to turn it into a pond
            FilePond.create(
                document.querySelector('.filepond')
            );*/

            $('.dropify').dropify();

            $('.flatpickr-date').flatpickr({
                monthSelectorType: "static",
                dateFormat: "d-m-Y"
            });

            ClassicEditor
                .create( document.querySelector( '#description' ) )
                .catch( error => {
                    console.error( error );
                } );
        });
    </script>
@endsection
