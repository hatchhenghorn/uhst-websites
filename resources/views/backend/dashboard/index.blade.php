@extends('layouts.backend-layout')

@section('title', 'Dashboard')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row gy-4">
            <a href="{{ route('faculties') }}" class="col-sm-6 col-lg-3">
                <div class="card card-border-shadow-primary h-100">
                    <div class="card-body">
                        <div class="d-flex align-items-center mb-2 pb-1">
                            <div class="avatar me-2">
                                <span class="avatar-initial rounded bg-label-primary"><i class="mdi mdi-office-building mdi-20px"></i></span>
                            </div>
                            <h4 class="ms-1 mb-0 display-6">{{ $facultyCount }}</h4>
                        </div>
                        <p class="mb-0 text-heading">Faculty</p>
                        <p class="mb-0">
                            <small class="text-muted">Total of Faculty</small>
                        </p>
                    </div>
                </div>
            </a>
            <a href="{{ route('institutes') }}" class="col-sm-6 col-lg-3">
                <div class="card card-border-shadow-success h-100">
                    <div class="card-body">
                        <div class="d-flex align-items-center mb-2 pb-1">
                            <div class="avatar me-2">
                                <span class="avatar-initial rounded bg-label-success"><i class="mdi mdi-domain mdi-20px"></i></span>
                            </div>
                            <h4 class="ms-1 mb-0 display-6">{{ $instituteCount }}</h4>
                        </div>
                        <p class="mb-0 text-heading">Institute</p>
                        <p class="mb-0">
                            <small class="text-muted">Total of Institute</small>
                        </p>
                    </div>
                </div>
            </a>
            <a href="{{ route('partners') }}" class="col-sm-6 col-lg-3">
                <div class="card card-border-shadow-warning h-100">
                    <div class="card-body">
                        <div class="d-flex align-items-center mb-2 pb-1">
                            <div class="avatar me-2">
                                <span class="avatar-initial rounded bg-label-warning">
                                    <i class='mdi mdi-handshake-outline mdi-20px'></i>
                                </span>
                            </div>
                            <h4 class="ms-1 mb-0 display-6">{{ $partnerCount }}</h4>
                        </div>
                        <p class="mb-0 text-heading">Partner</p>
                        <p class="mb-0">
                            <small class="text-muted">Total of Partner</small>
                        </p>
                    </div>
                </div>
            </a>
            <a href="{{ route('publication-books') }}" class="col-sm-6 col-lg-3">
                <div class="card card-border-shadow-danger h-100">
                    <div class="card-body">
                        <div class="d-flex align-items-center mb-2 pb-1">
                            <div class="avatar me-2">
                                <span class="avatar-initial rounded bg-label-danger">
                                  <i class='mdi mdi-book mdi-20px'></i>
                                </span>
                            </div>
                            <h4 class="ms-1 mb-0 display-6">{{ $publicationBookCount }}</h4>
                        </div>
                        <p class="mb-0 text-heading">Publication Book</p>
                        <p class="mb-0">
                            <small class="text-muted">Total of Publication Book</small>
                        </p>
                    </div>
                </div>
            </a>
            <a href="{{ route('careers') }}" class="col-sm-6 col-lg-3">
                <div class="card card-border-shadow-info h-100">
                    <div class="card-body">
                        <div class="d-flex align-items-center mb-2 pb-1">
                            <div class="avatar me-2">
                                <span class="avatar-initial rounded bg-label-info">
                                    <i class='mdi mdi-briefcase-outline mdi-20px'></i>
                                </span>
                            </div>
                            <h4 class="ms-1 mb-0 display-6">{{ $careerCount }}</h4>
                        </div>
                        <p class="mb-0 text-heading">Career</p>
                        <p class="mb-0">
                            <small class="text-muted">Total of Career</small>
                        </p>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.dashboard-main-menu').addClass('active');
        });
    </script>
@endsection
