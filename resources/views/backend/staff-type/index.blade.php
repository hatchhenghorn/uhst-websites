@extends('layouts.backend-layout')

@section('title', 'Staff Type')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="card-title">Staff Type</h5>
                @if(Auth::user()->can('Staff-Type.Create'))
                    <a href="{{ route('staff-types.create') }}" class="btn btn-primary">Add New</a>
                @endif
            </div>
            <div class="card-datatable table-responsive text-nowrap">
                <table class="table">
                    <thead class="table-light">
                    <tr>
                        <th>#</th>
                        <th>Actions</th>
                        <th>Type Name (EN)</th>
                        <th>Type Name (KH)</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($staffTypes->count() > 0)
                        @foreach($staffTypes as $key => $value)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>
                                    @if(Auth::user()->can('Staff-Type.Edit'))
                                        <a href="{{ route('staff-types.edit', $value->id) }}" class="btn btn-sm btn-icon me-2 btn-primary">
                                            <span class="tf-icons mdi mdi-pencil"></span>
                                        </a>
                                    @endif
                                    @if(Auth::user()->can('Staff-Type.Delete'))
                                        <a href="{{ route('staff-types.delete', $value->id) }}" class="btn btn-sm btn-icon me-2 btn-danger btn-delete">
                                            <span class="tf-icons mdi mdi-trash-can"></span>
                                        </a>
                                    @endif
                                </td>
                                <td>{{ $value->type_en }}</td>
                                <td>{{ $value->type_kh }}</td>
                                <td>
                                    <span class="badge {{ $value->activation === 1 ? 'bg-label-success' : 'bg-label-danger' }}">
                                        {{ $value->activation === 1 ? 'Active' : 'Inactive' }}
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="text-center" colspan="6">No data is available in table</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="mx-3">
                {!! $staffTypes->withQueryString()->links('vendor.pagination.bootstrap-5') !!}
            </div>
        </div>

        <!-- Modal Preview -->
        <div class="modal fade" id="modalPreview" tabindex="-1" aria-labelledby="modalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalPreviewTitle">Preview Description</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="preview-data"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.administrator-main-menu').addClass('active open');
            $('.staff-type-sub-menu').addClass('active');
        });
    </script>
@endsection
