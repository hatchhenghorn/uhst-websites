@extends('layouts.backend-layout')

@section('title', 'Create Staff Type')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="card-title">Create Staff Type</h5>
                <a href="{{ route('staff-types') }}" class="btn btn-secondary">Back</a>
            </div>
            <div class="card-body">
                <form action="{{ route('staff-types.store') }}" method="post" autocomplete="off">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('type_en') is-invalid @enderror"
                                    id="type_en"
                                    name="type_en"
                                    value="{{ old('type_en') }}"
                                    placeholder="Enter Staff Type Name(EN)"
                                />
                                <label for="type_en">Staff Type Name(EN)</label>
                                @error('type_en')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('type_kh') is-invalid @enderror"
                                    id="type_kh"
                                    name="type_kh"
                                    value="{{ old('type_kh') }}"
                                    placeholder="Enter Staff Type Name(KH)"
                                />
                                <label for="type_kh">Staff Type Name(KH)</label>
                                @error('type_kh')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 mb-4">
                            <div class="form-check form-check-primary">
                                <input
                                    class="form-check-input"
                                    type="checkbox"
                                    value="1"
                                    id="activation"
                                    name="activation"
                                    checked
                                />
                                <label class="form-check-label" for="activation">Activation</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                </form>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.administrator-main-menu').addClass('active open');
            $('.staff-type-sub-menu').addClass('active');
        });
    </script>
@endsection
