@extends('layouts.backend-layout')

@section('title', 'Institute')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="card-title">Institute</h5>
                @if(Auth::user()->can('Institute.Create'))
                    <a href="{{ route('institutes.create') }}" class="btn btn-primary">Add New</a>
                @endif
            </div>
            <div class="card-datatable table-responsive text-nowrap">
                <table class="table">
                    <thead class="table-light">
                    <tr>
                        <th>#</th>
                        <th>Actions</th>
                        <th>Institute Name(EN)</th>
                        <th>Institute Name(KH)</th>
                        <th>Faculty Name(EN)</th>
                        <th>Faculty Name(KH)</th>
                        <th>Description</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($institutes->count() > 0)
                        @foreach($institutes as $key => $item)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>
                                    @if(Auth::user()->can('Institute.Edit'))
                                        <a href="{{ route('institutes.edit', $item->id) }}" class="btn btn-sm btn-icon me-2 btn-primary">
                                            <span class="tf-icons mdi mdi-pencil"></span>
                                        </a>
                                    @endif
                                    @if(Auth::user()->can('Institute.Delete'))
                                        <a href="{{ route('institutes.delete', $item->id) }}" class="btn btn-sm btn-icon me-2 btn-danger btn-delete">
                                            <span class="tf-icons mdi mdi-trash-can"></span>
                                        </a>
                                    @endif
                                </td>
                                <td>{{ $item->title_en }}</td>
                                <td>{{ $item->title_kh }}</td>
                                <td>{{ $item->faculty->title_en }}</td>
                                <td>{{ $item->faculty->title_kh }}</td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-outline-primary" onclick="previewDescription('{{ $item->description }}')">Preview Description</button>
                                </td>
                                <td>
                                    <span class="badge {{ $item->activation === 1 ? 'bg-label-success' : 'bg-label-danger' }}">
                                        {{ $item->activation === 1 ? 'Active' : 'Inactive' }}
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="text-center" colspan="7">No data is available in table</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="mx-3">
                {!! $institutes->withQueryString()->links('vendor.pagination.bootstrap-5') !!}
            </div>
        </div>

        <!-- Modal Preview -->
        <div class="modal fade" id="modalPreview" tabindex="-1" aria-labelledby="modalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalPreviewTitle">Preview Description</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="preview-data"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.institute-main-menu').addClass('active open');
            $('.institute-sub-menu').addClass('active');
        });

        function previewDescription(value) {
            $('#modalPreview').modal('show');
            $('.preview-data').html(value);
        }
    </script>
@endsection
