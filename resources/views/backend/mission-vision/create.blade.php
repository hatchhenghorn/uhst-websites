@extends('layouts.backend-layout')

@section('title', 'Create Mission & Vision')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="card-title">Create Mission & Vision</h5>
                <a href="{{ route('mission-visions') }}" class="btn btn-secondary">Back</a>
            </div>
            <div class="card-body">
                <form action="{{ route('mission-visions.store') }}" method="post" autocomplete="off">
                    @csrf
                    <div class="row">
                        <div class="col-lg-12 col-md-12 mb-4">
                            <textarea
                                class="form-control @error('mission') is-invalid @enderror"
                                id="mission"
                                name="mission"
                                placeholder="Enter Mission"
                            >
                                {{ old('mission') }}
                            </textarea>
                            @error('mission')
                            <div class="invalid-feedback"> {{ $message }} </div>
                            @enderror
                        </div>
                        <div class="col-lg-12 col-md-12 mb-4">
                            <textarea
                                class="form-control @error('vision') is-invalid @enderror"
                                id="vision"
                                name="vision"
                                placeholder="Enter Vision"
                            >
                                {{ old('vision') }}
                            </textarea>
                            @error('vision')
                            <div class="invalid-feedback"> {{ $message }} </div>
                            @enderror
                        </div>
                        <div class="col-lg-12 col-md-12 mb-4">
                            <div class="form-check form-check-primary">
                                <input
                                    class="form-check-input"
                                    type="checkbox"
                                    value="1"
                                    id="activation"
                                    name="activation"
                                    checked
                                />
                                <label class="form-check-label" for="activation">Activation</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                </form>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.about-us-main-menu').addClass('active open');
            $('.history-sub-menu').addClass('active');

            ClassicEditor
                .create( document.querySelector( '#mission' ) )
                .catch( error => {
                    console.error( error );
                } );

            ClassicEditor
                .create( document.querySelector( '#vision' ) )
                .catch( error => {
                    console.error( error );
                } );
        });
    </script>
@endsection
