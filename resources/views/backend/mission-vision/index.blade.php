@extends('layouts.backend-layout')

@section('title', 'Mission & Vision')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="card-title">Mission & Vision</h5>
                @if(Auth::user()->can('Mission-Vision.Create'))
                    <a href="{{ route('mission-visions.create') }}" class="btn btn-primary">Add New</a>
                @endif
            </div>
            <div class="card-datatable table-responsive text-nowrap">
                <table class="table">
                    <thead class="table-light">
                    <tr>
                        <th>#</th>
                        <th>Actions</th>
                        <th>Mission</th>
                        <th>Vision</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($mission_visions->count() > 0)
                        @foreach($mission_visions as $key => $value)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>
                                    @if(Auth::user()->can('Mission-Vision.Edit'))
                                        <a href="{{ route('mission-visions.edit', $value->id) }}" class="btn btn-sm btn-icon me-2 btn-primary">
                                            <span class="tf-icons mdi mdi-pencil"></span>
                                        </a>
                                    @endif
                                    @if(Auth::user()->can('Mission-Vision.Delete'))
                                        <a href="{{ route('mission-visions.delete', $value->id) }}" class="btn btn-sm btn-icon me-2 btn-danger btn-delete">
                                            <span class="tf-icons mdi mdi-trash-can"></span>
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-outline-primary" onclick="previewMissionVision('{{ $value->mission }}', 'Preview Mission')">Preview Mission</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-outline-primary" onclick="previewMissionVision('{{ $value->vision }}', 'Preview Vision')">Preview Vision</button>
                                </td>
                                <td>
                                    <span class="badge {{ $value->activation === 1 ? 'bg-label-success' : 'bg-label-danger' }}">
                                        {{ $value->activation === 1 ? 'Active' : 'Inactive' }}
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="text-center" colspan="5">No data is available in table</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="mx-3">
                {!! $mission_visions->withQueryString()->links('vendor.pagination.bootstrap-5') !!}
            </div>
        </div>

        <!-- Modal Preview -->
        <div class="modal fade" id="modalPreview" tabindex="-1" aria-labelledby="modalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalPreviewTitle"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="preview-data"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.about-us-main-menu').addClass('active open');
            $('.mission-vision-sub-menu').addClass('active');
        });

        function previewMissionVision(value, type) {
            $('#modalPreviewTitle').text(type);
            $('#modalPreview').modal('show');
            $('.preview-data').html(value);
        }
    </script>
@endsection
