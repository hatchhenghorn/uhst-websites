@extends('layouts.backend-layout')

@section('title', 'Edit Publication Book')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="card-title">Edit Publication Book</h5>
                <a href="{{ route('publication-books') }}" class="btn btn-secondary">Back</a>
            </div>
            <div class="card-body">
                <form action="{{ route('publication-books.update',$publication_book->id) }}" method="post" enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    <div class="row">
                        <div class="col-lg-12 col-md-12 mb-4">
                            <label for="input-file-max-fs">Maximum file upload size 2MB. (width-1080px & height-720px)</label>
                            <input
                                type="file"
                                id="input-file-max-fs"
                                name="image"
                                class="dropify"
                                data-default-file="{{ url('uploads/publication-book/'.$publication_book->image) }}"
                                data-max-file-size="2M"
                            />
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('title_en') is-invalid @enderror"
                                    id="title_en"
                                    name="title_en"
                                    value="{{ $publication_book->title_en  }}"
                                    placeholder="Enter Book Name(EN)"
                                />
                                <label for="title_en">Book Name(EN)</label>
                                @error('title_en')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('title_kh') is-invalid @enderror"
                                    id="title_kh"
                                    name="title_kh"
                                    value="{{ $publication_book->title_kh }}"
                                    placeholder="Enter Book Name(KH)"
                                />
                                <label for="title_kh">Book Name(KH)</label>
                                @error('title_kh')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="url"
                                    class="form-control @error('url') is-invalid @enderror"
                                    id="url"
                                    name="url"
                                    value="{{ $publication_book->url }}"
                                    placeholder="Enter Url"
                                />
                                <label for="url">Url</label>
                                @error('url')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('date_published') is-invalid @enderror flatpickr-date"
                                    id="date_published"
                                    name="date_published"
                                    value="{{ date('d-m-Y', strtotime($publication_book->date_published)) }}"
                                    placeholder="DD-MM-YYYY"
                                />
                                <label for="date_published">Date Published</label>
                                @error('date_published')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('author') is-invalid @enderror"
                                    id="author"
                                    name="author"
                                    value="{{ $publication_book->author }}"
                                    placeholder="Enter Author"
                                />
                                <label for="author">Author</label>
                                @error('author')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('doi') is-invalid @enderror"
                                    id="doi"
                                    name="doi"
                                    value="{{ $publication_book->doi }}"
                                    placeholder="Enter DOI"
                                />
                                <label for="doi">DOI</label>
                                @error('doi')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 mb-4">
                            <textarea
                                class="form-control @error('description') is-invalid @enderror"
                                id="description"
                                name="description"
                                placeholder="Enter Description"
                            >
                                {{ $publication_book->description }}
                            </textarea>
                            @error('description')
                            <div class="invalid-feedback"> {{ $message }} </div>
                            @enderror
                        </div>
                        <div class="col-lg-12 col-md-12 mb-4">
                            <div class="form-check form-check-primary">
                                <input
                                    class="form-check-input"
                                    type="checkbox"
                                    value="1"
                                    id="activation"
                                    name="activation"
                                    {{ $publication_book->activation === 1 ? 'checked' : '' }}
                                />
                                <label class="form-check-label" for="activation">Activation</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                </form>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.research-innovation-main-menu').addClass('active open');
            $('.publications-book-sub-menu').addClass('active');

            $('.dropify').dropify();

            $('.flatpickr-date').flatpickr({
                monthSelectorType: "static",
                dateFormat: "d-m-Y"
            });

            ClassicEditor
                .create( document.querySelector( '#description' ) )
                .catch( error => {
                    console.error( error );
                } );
        });
    </script>
@endsection
