@extends('layouts.backend-layout')

@section('title', 'Major & Course')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="card-title">Major & Course</h5>
                @if(Auth::user()->can('Major-Course.Create'))
                    <a href="{{ route('major-courses.create') }}" class="btn btn-primary">Add New</a>
                @endif
            </div>
            <div class="card-datatable table-responsive text-nowrap">
                <table class="table">
                    <thead class="table-light">
                    <tr>
                        <th>#</th>
                        <th>Actions</th>
                        <th>Title</th>
                        <th>Major Name(EN)</th>
                        <th>Major Name(KH)</th>
                        <th>Course Name(EN)</th>
                        <th>Course Name(KH)</th>
                        <th>Year</th>
                        <th>Semester</th>
                        <th>Credit</th>
                        <th>Description</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($majorCourse->count() > 0)
                        @foreach($majorCourse as $key => $item)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>
                                    @if(Auth::user()->can('Major-Course.Edit'))
                                        <a href="{{ route('major-courses.edit', $item->id) }}" class="btn btn-sm btn-icon me-2 btn-primary">
                                            <span class="tf-icons mdi mdi-pencil"></span>
                                        </a>
                                    @endif
                                    @if(Auth::user()->can('Major-Course.Delete'))
                                        <a href="{{ route('major-courses.delete', $item->id) }}" class="btn btn-sm btn-icon me-2 btn-danger btn-delete">
                                            <span class="tf-icons mdi mdi-trash-can"></span>
                                        </a>
                                    @endif
                                </td>
                                <td>{{ $item->title }}</td>
                                <td>{{ $item->major->title_en }}</td>
                                <td>{{ $item->major->title_kh }}</td>
                                <td>{{ $item->course->title_en }}</td>
                                <td>{{ $item->course->title_kh }}</td>
                                <td>{{ $item->year }}</td>
                                <td>{{ $item->semester }}</td>
                                <td>{{ $item->credit }}</td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-outline-primary" onclick="previewDescription('{{ $item->description }}')">Preview Description</button>
                                </td>
                                <td>
                                    <span class="badge {{ $item->activation === 1 ? 'bg-label-success' : 'bg-label-danger' }}">
                                        {{ $item->activation === 1 ? 'Active' : 'Inactive' }}
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="text-center" colspan="9">No data is available in table</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="mx-3">
                {!! $majorCourse->withQueryString()->links('vendor.pagination.bootstrap-5') !!}
            </div>
        </div>

        <!-- Modal Preview -->
        <div class="modal fade" id="modalPreview" tabindex="-1" aria-labelledby="modalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalPreviewTitle">Preview Description</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="preview-data"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.institute-main-menu').addClass('active open');
            $('.major-course-sub-menu').addClass('active');
        });

        function previewDescription(value) {
            $('#modalPreview').modal('show');
            $('.preview-data').html(value);
        }
    </script>
@endsection
