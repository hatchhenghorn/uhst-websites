@extends('layouts.backend-layout')

@section('title', 'Create Major & Course')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <form class="app-ecommerce" action="{{ route('major-courses.store') }}" method="post" autocomplete="off">
            @csrf
            <div class="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center mb-3">
                <div class="d-flex flex-column justify-content-center">
                    <h4 class="mb-1 mt-3">Add a new Major & Course</h4>
                    <p>Please enter the field</p>
                </div>
                <div class="d-flex align-content-center flex-wrap gap-3">
                    <a href="{{ route('major-courses') }}" class="btn btn-outline-secondary">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row g-4">
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('title') is-invalid @enderror"
                                    id="title"
                                    value="{{ old('title') }}"
                                    placeholder="Enter Title"
                                    name="title"
                                />
                                <label for="title">Title <span class="text-danger">(Required)</span></label>
                                @error('title')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="number"
                                    class="form-control @error('year') is-invalid @enderror"
                                    id="year"
                                    value="{{ old('year') }}"
                                    placeholder="Enter Year"
                                    name="year"
                                />
                                <label for="year">Year <span class="text-danger">(Required)</span></label>
                                @error('year')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="number"
                                    class="form-control @error('semester') is-invalid @enderror"
                                    id="semester"
                                    value="{{ old('semester') }}"
                                    placeholder="Enter Semester"
                                    name="semester"
                                />
                                <label for="semester">Semester <span class="text-danger">(Required)</span></label>
                                @error('semester')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="number"
                                    class="form-control @error('credit') is-invalid @enderror"
                                    id="credit"
                                    value="{{ old('credit') }}"
                                    placeholder="Enter Credit"
                                    name="credit"
                                />
                                <label for="credit">Credit <span class="text-danger">(Required)</span></label>
                                @error('credit')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <select id="major_id" class="select2 form-select @error('major_id') is-invalid @enderror" name="major_id">
                                    <option value="">Select Major</option>
                                    @foreach($majors as $major)
                                        <option value="{{ $major->id }}">{{ $major->title_en }}</option>
                                    @endforeach
                                </select>
                                <label for="major_id">Major <span class="text-danger">(Required)</span></label>
                                @error('major_id')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <select id="course_id" class="select2 form-select @error('course_id') is-invalid @enderror" name="course_id">
                                    <option value="">Select Course</option>
                                    @foreach($courses as $course)
                                        <option value="{{ $course->id }}">{{ $course->title_en }}</option>
                                    @endforeach
                                </select>
                                <label for="course_id">Course <span class="text-danger">(Required)</span></label>
                                @error('course_id')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <textarea
                                class="form-control @error('description') is-invalid @enderror"
                                id="description"
                                name="description"
                                placeholder="Enter Description"
                            >
                                {{ old('description') }}
                            </textarea>
                            @error('description')
                            <div class="invalid-feedback"> {{ $message }} </div>
                            @enderror
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div class="form-check form-check-primary">
                                <input
                                    class="form-check-input"
                                    type="checkbox"
                                    value="1"
                                    id="activation"
                                    name="activation"
                                    checked
                                />
                                <label class="form-check-label" for="activation">Activation</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.institute-main-menu').addClass('active open');
            $('.major-course-sub-menu').addClass('active');

            ClassicEditor
                .create( document.querySelector( '#description' ) )
                .catch( error => {
                    console.error( error );
                } );

            //$('.select2').select2();
        });
    </script>
@endsection
