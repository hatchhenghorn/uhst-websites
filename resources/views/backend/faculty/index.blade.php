@extends('layouts.backend-layout')

@section('title', 'Faculty')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="card-title">Faculty</h5>
                @if(Auth::user()->can('Faculty.Create'))
                    <a href="{{ route('faculties.create') }}" class="btn btn-primary">Add New</a>
                @endif
            </div>
            <div class="card-datatable table-responsive text-nowrap">
                <table class="table">
                    <thead class="table-light">
                    <tr>
                        <th>#</th>
                        <th>Actions</th>
                        <th>Image</th>
                        <th>Faculty Name(EN)</th>
                        <th>Faculty Name(KH)</th>
                        <th>Description</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($faculties->count() > 0)
                        @foreach($faculties as $key => $item)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>
                                    @if(Auth::user()->can('Faculty.Edit'))
                                        <a href="{{ route('faculties.edit', $item->id) }}" class="btn btn-sm btn-icon me-2 btn-primary">
                                            <span class="tf-icons mdi mdi-pencil"></span>
                                        </a>
                                    @endif
                                    @if(Auth::user()->can('Faculty.Delete'))
                                        <a href="{{ route('faculties.delete', $item->id) }}" class="btn btn-sm btn-icon me-2 btn-danger btn-delete">
                                            <span class="tf-icons mdi mdi-trash-can"></span>
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    <div class="avatar me-2">
                                        <img src="{{ url('uploads/faculty/'.$item->image) }}" onclick="openImageInNewTabCentered('{{ url('uploads/faculty/'.$item->image) }}')" alt="Avatar" class="rounded">
                                    </div>
                                </td>
                                <td>{{ $item->title_en }}</td>
                                <td>{{ $item->title_kh }}</td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-outline-primary" onclick="previewDescription('{{ $item->description }}')">Preview Description</button>
                                </td>
                                <td>
                                    <span class="badge {{ $item->activation === 1 ? 'bg-label-success' : 'bg-label-danger' }}">
                                        {{ $item->activation === 1 ? 'Active' : 'Inactive' }}
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="text-center" colspan="6">No data is available in table</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="mx-3">
                {!! $faculties->withQueryString()->links('vendor.pagination.bootstrap-5') !!}
            </div>
        </div>

        <!-- Modal Preview -->
        <div class="modal fade" id="modalPreview" tabindex="-1" aria-labelledby="modalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalPreviewTitle">Preview Description</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="preview-data"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.faculty-main-menu').addClass('active');
        });

        function openImageInNewTabCentered(imageUrl) {
            // Define the width and height for the new window
            const newWindowWidth = 600; // Width in pixels
            const newWindowHeight = 400; // Height in pixels

            // Calculate the position to center the window
            const leftPosition = (screen.width - newWindowWidth) / 2;
            const topPosition = (screen.height - newWindowHeight) / 2;

            // Open the image URL in a new tab with centered position and specified dimensions
            window.open(imageUrl, '_blank', `width=${newWindowWidth}, height=${newWindowHeight}, top=${topPosition}, left=${leftPosition}`);
        }

        function previewDescription(value) {
            $('#modalPreview').modal('show');
            $('.preview-data').html(value);
        }
    </script>
@endsection
