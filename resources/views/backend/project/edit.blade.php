@extends('layouts.backend-layout')

@section('title', 'Edit Project')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="card-title">Edit Project</h5>
                <a href="{{ route('projects') }}" class="btn btn-secondary">Back</a>
            </div>
            <div class="card-body">
                <form action="{{ route('projects.update', $project->id) }}" method="post" enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    <div class="row">
                        <div class="col-lg-12 col-md-12 mb-4">
                            <label for="input-file-max-fs">Maximum file upload size 2MB. (width-1080px & height-720px)</label>
                            <input
                                type="file"
                                id="input-file-max-fs"
                                name="image"
                                class="dropify"
                                data-default-file="{{ url('uploads/project/'.$project->image) }}"
                                data-max-file-size="2M"
                            />
                        </div>
                        <div class="col-lg-12 col-md-12 mb-4">
                            <label for="input-file-max-fs">Sub Image (You are can be upload multiple image)</label>
                            <input
                                type="file"
                                id="sub_image"
                                name="sub_image[]"
                                class="form-control"
                                multiple
                            />
                        </div>
                        <div class="row mb-4">
                            @foreach($project_image as $image)
                                <div class="col-md-3">
                                    <div class="card border shadow-none h-100">
                                        <div class="card-body">
                                            <div class="bg-label-primary text-center mb-3 pt-2 rounded-3 h-px-150">
                                                <img class="img-fluid" src="{{ url('uploads/project/sub-images/'.$image->image) }}" style="width: 100%; height: 100%; object-fit: contain" alt="Card girl image">
                                            </div>
                                            <a href="{{ route('projects.remove',$image->id) }}" class="btn btn-danger btn-sm w-100 waves-effect waves-light btn-delete">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('title_en') is-invalid @enderror"
                                    id="title_en"
                                    name="title_en"
                                    value="{{ $project->title_en }}"
                                    placeholder="Enter Project Name(EN)"
                                />
                                <label for="title_en">Project Name(EN)</label>
                                @error('title_en')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('title_kh') is-invalid @enderror"
                                    id="title_kh"
                                    name="title_kh"
                                    value="{{ $project->title_kh }}"
                                    placeholder="Enter Project Name(KH)"
                                />
                                <label for="title_kh">Project Name(KH)</label>
                                @error('title_kh')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 mb-4">
                            <textarea
                                class="form-control @error('description') is-invalid @enderror"
                                id="description"
                                name="description"
                                placeholder="Enter Description"
                            >
                                {{ $project->description }}
                            </textarea>
                            @error('description')
                            <div class="invalid-feedback"> {{ $message }} </div>
                            @enderror
                        </div>
                        <div class="col-lg-12 col-md-12 mb-4">
                            <div class="form-check form-check-primary">
                                <input
                                    class="form-check-input"
                                    type="checkbox"
                                    value="1"
                                    id="activation"
                                    name="activation"
                                    checked
                                />
                                <label class="form-check-label" for="activation">Activation</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                </form>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.research-innovation-main-menu').addClass('active open');
            $('.project-sub-menu').addClass('active');

            $('.dropify').dropify();

            ClassicEditor
                .create( document.querySelector( '#description' ) )
                .catch( error => {
                    console.error( error );
                } );
        });
    </script>
@endsection
