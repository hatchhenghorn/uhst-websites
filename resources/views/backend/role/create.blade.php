@extends('layouts.backend-layout')

@section('title', 'Create Role')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <form class="app-ecommerce" action="{{ route('roles.store') }}" method="post" autocomplete="off" enctype="multipart/form-data">
            @csrf
            <div class="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center mb-3">
                <div class="d-flex flex-column justify-content-center">
                    <h4 class="mb-1 mt-3">Add a new Role</h4>
                    <p>Please enter the field</p>
                </div>
                <div class="d-flex align-content-center flex-wrap gap-3">
                    <a href="{{ route('roles') }}" class="btn btn-outline-secondary">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row g-4">
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('name') is-invalid @enderror"
                                    id="name"
                                    value="{{ old('name') }}"
                                    placeholder="Enter Role Name"
                                    name="name"
                                />
                                <label for="name">Role Name <span class="text-danger">(Required)</span></label>
                                @error('name')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.role-permission-main-menu').addClass('active open');
            $('.role-sub-menu').addClass('active');
        });
    </script>
@endsection
