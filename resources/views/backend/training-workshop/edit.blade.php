@extends('layouts.backend-layout')

@section('title', 'Edit Training & Workshop')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="card-title">Edit Training & Workshop</h5>
                <a href="{{ route('training-workshops') }}" class="btn btn-secondary">Back</a>
            </div>
            <div class="card-body">
                <form action="{{ route('training-workshops.update',$training_workshop->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-lg-12 col-md-12 mb-4">
                            <label for="input-file-max-fs">Maximum file upload size 2MB. (width-1080px & height-720px)</label>
                            <input
                                type="file"
                                id="input-file-max-fs"
                                name="image"
                                class="dropify"
                                data-default-file="{{ url('uploads/training-workshop/'.$training_workshop->image) }}"
                                data-max-file-size="2M"
                            />
                        </div>
                        <div class="col-lg-12 col-md-12 mb-4">
                            <label for="input-file-max-fs">Sub Image (You are can be upload multiple image)</label>
                            <input
                                type="file"
                                id="sub_image"
                                name="sub_image[]"
                                class="form-control"
                                multiple
                            />
                        </div>
                        <div class="row mb-4">
                            @foreach($training_workshop_image as $image)
                                <div class="col-md-3">
                                    <div class="card border shadow-none h-100">
                                        <div class="card-body">
                                            <div class="bg-label-primary text-center mb-3 pt-2 rounded-3 h-px-150">
                                                <img class="img-fluid" src="{{ url('uploads/training-workshop/sub-images/'.$image->image) }}" style="width: 100%; height: 100%; object-fit: contain" alt="Card girl image">
                                            </div>
                                            <a href="{{ route('training-workshops.remove',$image->id) }}" class="btn btn-danger btn-sm w-100 waves-effect waves-light btn-delete">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('title_en') is-invalid @enderror"
                                    id="title_en"
                                    name="title_en"
                                    value="{{ $training_workshop->title_en }}"
                                    placeholder="Enter Title English"
                                />
                                <label for="title_en">Title English</label>
                                @error('title_en')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('title_kh') is-invalid @enderror"
                                    id="title_kh"
                                    name="title_kh"
                                    value="{{ $training_workshop->title_kh }}"
                                    placeholder="Enter Title Khmer"
                                />
                                <label for="title_kh">Title Khmer</label>
                                @error('title_kh')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('sub_title_en') is-invalid @enderror"
                                    id="sub_title_en"
                                    name="sub_title_en"
                                    value="{{ $training_workshop->sub_title_en }}"
                                    placeholder="Enter Sub Title English"
                                />
                                <label for="sub_title_en">Sub Title English</label>
                                @error('sub_title_en')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('sub_title_kh') is-invalid @enderror"
                                    id="sub_title_kh"
                                    name="sub_title_kh"
                                    value="{{ $training_workshop->sub_title_kh }}"
                                    placeholder="Enter Sub Title Khmer"
                                />
                                <label for="sub_title_kh">Sub Title Khmer</label>
                                @error('sub_title_kh')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('start_date') is-invalid @enderror flatpickr-date"
                                    id="start_date"
                                    name="start_date"
                                    value="{{ date('d-m-Y', strtotime($training_workshop->start_date)) }}"
                                    placeholder="DD-MM-YYYY"
                                />
                                <label for="start_date">Start Date</label>
                                @error('start_date')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('end_date') is-invalid @enderror flatpickr-date"
                                    id="end_date"
                                    name="end_date"
                                    value="{{ date('d-m-Y', strtotime($training_workshop->end_date)) }}"
                                    placeholder="DD-MM-YYYY"
                                />
                                <label for="end_date">End Date</label>
                                @error('end_date')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 mb-4">
                            <textarea
                                class="form-control @error('description') is-invalid @enderror"
                                id="description"
                                name="description"
                                placeholder="Enter Description"
                            >
                                {{ $training_workshop->description }}
                            </textarea>
                            @error('description')
                            <div class="invalid-feedback"> {{ $message }} </div>
                            @enderror
                        </div>
                        <div class="col-lg-12 col-md-12 mb-4">
                            <div class="form-check form-check-primary">
                                <input
                                    class="form-check-input"
                                    type="checkbox"
                                    value="1"
                                    id="activation"
                                    name="activation"
                                    {{ $training_workshop->activation === 1 ? 'checked' : '' }}
                                />
                                <label class="form-check-label" for="activation">Activation</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                </form>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.research-innovation-main-menu').addClass('active open');
            $('.training-workshop-sub-menu').addClass('active');

            $('.dropify').dropify();

            $('.flatpickr-date').flatpickr({
                monthSelectorType: "static",
                dateFormat: "d-m-Y"
            });

            ClassicEditor
                .create( document.querySelector( '#description' ) )
                .catch( error => {
                    console.error( error );
                } );
        });
    </script>
@endsection
