@extends('layouts.backend-layout')

@section('title', 'Edit Contact & Location')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="card-title">Edit Contact & Location</h5>
                <a href="{{ route('contact-locations') }}" class="btn btn-secondary">Back</a>
            </div>
            <div class="card-body">
                <form action="{{ route('contact-locations.update', $contact_location->id) }}" method="post" autocomplete="off">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('name') is-invalid @enderror"
                                    id="name"
                                    name="name"
                                    value="{{ $contact_location->name }}"
                                    placeholder="Enter Name"
                                />
                                <label for="name">Name</label>
                                @error('name')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="email"
                                    class="form-control @error('email') is-invalid @enderror"
                                    id="email"
                                    name="email"
                                    value="{{ $contact_location->email }}"
                                    placeholder="Enter Email"
                                />
                                <label for="email">Email</label>
                                @error('email')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('tel') is-invalid @enderror"
                                    id="tel"
                                    name="tel"
                                    value="{{ $contact_location->tel }}"
                                    placeholder="Enter Tel"
                                />
                                <label for="tel">Tel</label>
                                @error('tel')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="url"
                                    class="form-control @error('location_map') is-invalid @enderror"
                                    id="location_map"
                                    name="location_map"
                                    value="{{ $contact_location->location_map }}"
                                    placeholder="Enter Location Map"
                                />
                                <label for="location_map">Location Map</label>
                                @error('location_map')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 mb-4">
                            <textarea
                                class="form-control @error('location_description') is-invalid @enderror"
                                id="location_description"
                                name="location_description"
                                placeholder="Enter Location Description"
                            >
                                {{ $contact_location->location_description }}
                            </textarea>
                            @error('location_description')
                            <div class="invalid-feedback"> {{ $message }} </div>
                            @enderror
                        </div>
                        <div class="col-lg-12 col-md-12 mb-4">
                            <div class="form-check form-check-primary">
                                <input
                                    class="form-check-input"
                                    type="checkbox"
                                    value="1"
                                    id="activation"
                                    name="activation"
                                    {{ $contact_location->activation === 1 ? 'checked' : '' }}
                                />
                                <label class="form-check-label" for="activation">Activation</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                </form>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.contact-locations-main-menu').addClass('active');

            ClassicEditor
                .create( document.querySelector( '#location_description' ) )
                .catch( error => {
                    console.error( error );
                } );
        });
    </script>
@endsection
