@extends('layouts.backend-layout')

@section('title', 'Edit Permission')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <form class="app-ecommerce" action="{{ route('permissions.update', $permission->id) }}" method="post" autocomplete="off" enctype="multipart/form-data">
            @csrf
            <div class="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center mb-3">
                <div class="d-flex flex-column justify-content-center">
                    <h4 class="mb-1 mt-3">Edit Permission</h4>
                    <p>Please enter the field</p>
                </div>
                <div class="d-flex align-content-center flex-wrap gap-3">
                    <a href="{{ route('permissions') }}" class="btn btn-outline-secondary">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row g-4">
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('name') is-invalid @enderror"
                                    id="name"
                                    value="{{ $permission->name }}"
                                    placeholder="Enter Permission Name"
                                    name="name"
                                />
                                <label for="name">Permission Name <span class="text-danger">(Required)</span></label>
                                @error('name')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <select name="group_name" id="group_name" class="form-select select2">
                                    <option value="">Select Group Name</option>
                                    <option value="staff" {{ $permission->group_name === 'staff' ? 'selected' : '' }}>Staff</option>
                                    <option value="staff-type" {{ $permission->group_name === 'staff-type' ? 'selected' : '' }}>Staff Type</option>
                                    <option value="role" {{ $permission->group_name === 'role' ? 'selected' : '' }}>Role & Permission</option>
                                    <option value="history" {{ $permission->group_name === 'history' ? 'selected' : '' }}>History</option>
                                    <option value="mission-vision" {{ $permission->group_name === 'mission-vision' ? 'selected' : '' }}>Mission & Vision</option>
                                    <option value="president-message" {{ $permission->group_name === 'president-message' ? 'selected' : '' }}>President Message</option>
                                    <option value="partner" {{ $permission->group_name === 'partner' ? 'selected' : '' }}>Partner</option>
                                    <option value="faculty" {{ $permission->group_name === 'faculty' ? 'selected' : '' }}>Faculty</option>
                                    <option value="institute" {{ $permission->group_name === 'institute' ? 'selected' : '' }}>Institute</option>
                                    <option value="major" {{ $permission->group_name === 'major' ? 'selected' : '' }}>Major</option>
                                    <option value="course" {{ $permission->group_name === 'course' ? 'selected' : '' }}>Course</option>
                                    <option value="major-course" {{ $permission->group_name === 'major-course' ? 'selected' : '' }}>Major & Course</option>
                                    <option value="admission" {{ $permission->group_name === 'admission' ? 'selected' : '' }}>Admission</option>
                                    <option value="project" {{ $permission->group_name === 'project' ? 'selected' : '' }}>Projects</option>
                                    <option value="publication-book" {{ $permission->group_name === 'publication-book' ? 'selected' : '' }}>Publication Books</option>
                                    <option value="training-workshop" {{ $permission->group_name === 'training-workshop' ? 'selected' : '' }}>Training & Workshop</option>
                                    <option value="conference" {{ $permission->group_name === 'conference' ? 'selected' : '' }}>Conferences</option>
                                    <option value="contact-location" {{ $permission->group_name === 'contact-location' ? 'selected' : '' }}>Contact & Location</option>
                                    <option value="news-activities" {{ $permission->group_name === 'news-activities' ? 'selected' : '' }}>News & Activities</option>
                                    <option value="career" {{ $permission->group_name === 'career' ? 'selected' : '' }}>Careers</option>
                                </select>
                                <label for="group_name">Group Name</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.role-permission-main-menu').addClass('active open');
            $('.permission-sub-menu').addClass('active');
        });
    </script>
@endsection
