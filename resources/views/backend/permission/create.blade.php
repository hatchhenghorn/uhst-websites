@extends('layouts.backend-layout')

@section('title', 'Create Permission')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <form class="app-ecommerce" action="{{ route('permissions.store') }}" method="post" autocomplete="off">
            @csrf
            <div class="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center mb-3">
                <div class="d-flex flex-column justify-content-center">
                    <h4 class="mb-1 mt-3">Add a new Permission</h4>
                    <p>Please enter the field</p>
                </div>
                <div class="d-flex align-content-center flex-wrap gap-3">
                    <a href="{{ route('permissions') }}" class="btn btn-outline-secondary">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row g-4">
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('name') is-invalid @enderror"
                                    id="name"
                                    value="{{ old('name') }}"
                                    placeholder="Enter Permission Name"
                                    name="name"
                                />
                                <label for="name">Permission Name <span class="text-danger">(Required)</span></label>
                                @error('name')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <div class="form-floating form-floating-outline">
                                    <select name="group_name" id="group_name" class="form-select select2">
                                        <option value="">Select Group Name</option>
                                        <option value="staff">Staff</option>
                                        <option value="staff-type">Staff Type</option>
                                        <option value="role">Role & Permission</option>
                                        <option value="history">History</option>
                                        <option value="mission-vision">Mission & Vision</option>
                                        <option value="president-message">President Message</option>
                                        <option value="partner">Partner</option>
                                        <option value="faculty">Faculty</option>
                                        <option value="institute">Institute</option>
                                        <option value="major">Major</option>
                                        <option value="course">Course</option>
                                        <option value="major-course">Major & Course</option>
                                        <option value="admission">Admission</option>
                                        <option value="project">Projects</option>
                                        <option value="publication-book">Publication Books</option>
                                        <option value="training-workshop">Training & Workshop</option>
                                        <option value="conference">Conferences</option>
                                        <option value="contact-location">Contact & Location</option>
                                        <option value="news-activities">News & Activities</option>
                                        <option value="career">Careers</option>
                                    </select>
                                    <label for="group_name">Group Name</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.role-permission-main-menu').addClass('active open');
            $('.permission-sub-menu').addClass('active');
        });
    </script>
@endsection
