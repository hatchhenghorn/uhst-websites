@extends('layouts.backend-layout')

@section('title', 'Edit News & Activities')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <form class="app-ecommerce" action="{{ route('news-activities.update', $news_activity->id) }}" method="post" autocomplete="off" enctype="multipart/form-data">
            @csrf
            <div class="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center mb-3">
                <div class="d-flex flex-column justify-content-center">
                    <h4 class="mb-1 mt-3">Edit News & Activity</h4>
                    <p>Please enter the field</p>
                </div>
                <div class="d-flex align-content-center flex-wrap gap-3">
                    <a href="{{ route('news-activities') }}" class="btn btn-outline-secondary">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row g-4">
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <label for="input-file-max-fs">Maximum file upload size 2MB. (width-1080px & height-720px)</label>
                            <input
                                type="file"
                                id="input-file-max-fs"
                                name="image"
                                class="dropify"
                                data-default-file="{{ url('uploads/news-activities/'.$news_activity->image) }}"
                                data-max-file-size="2M"
                            />
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('title_en') is-invalid @enderror"
                                    id="title_en"
                                    name="title_en"
                                    value="{{ $news_activity->title_en }}"
                                    placeholder="Enter Title English"
                                />
                                <label for="title_en">Title English</label>
                                @error('title_en')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('title_kh') is-invalid @enderror"
                                    id="title_kh"
                                    name="title_kh"
                                    value="{{ $news_activity->title_kh }}"
                                    placeholder="Enter Title Khmer"
                                />
                                <label for="title_kh">Title Khmer</label>
                                @error('title_kh')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('sub_title_en') is-invalid @enderror"
                                    id="sub_title_en"
                                    name="sub_title_en"
                                    value="{{ $news_activity->sub_title_en }}"
                                    placeholder="Enter Sub Title English"
                                />
                                <label for="sub_title_en">Sub Title English</label>
                                @error('sub_title_en')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('sub_title_kh') is-invalid @enderror"
                                    id="sub_title_kh"
                                    name="sub_title_kh"
                                    value="{{ $news_activity->sub_title_kh }}"
                                    placeholder="Enter Sub Title Khmer"
                                />
                                <label for="sub_title_kh">Sub Title Khmer</label>
                                @error('sub_title_kh')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('date_time') is-invalid @enderror flatpickr-date"
                                    id="date_time"
                                    name="date_time"
                                    value="{{ date('d-m-Y H:i', strtotime($news_activity->date_time)) }}"
                                    placeholder="DD-MM-YYYY HH:MM"
                                />
                                <label for="date_time">Date Time {{ $news_activity->date_time }}</label>
                                @error('date_time')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <textarea
                                class="form-control @error('description') is-invalid @enderror"
                                id="description"
                                name="description"
                                placeholder="Enter Description"
                            >
                                {{ $news_activity->description }}
                            </textarea>
                            @error('description')
                            <div class="invalid-feedback"> {{ $message }} </div>
                            @enderror
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-check form-check-primary">
                                <input
                                    class="form-check-input"
                                    type="checkbox"
                                    value="1"
                                    id="activation"
                                    name="activation"
                                    checked
                                />
                                <label class="form-check-label" for="activation">Activation</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.news-activities-main-menu').addClass('active');

            $('.dropify').dropify();

            $('.flatpickr-date').flatpickr({
                enableTime: true,
                time_24hr: true,
                dateFormat: "d-m-Y H:i"
            });

            ClassicEditor
                .create( document.querySelector( '#description' ) )
                .catch( error => {
                    console.error( error );
                } );
        });
    </script>
@endsection
