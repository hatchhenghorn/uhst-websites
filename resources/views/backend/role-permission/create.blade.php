@extends('layouts.backend-layout')

@section('title', 'Create Role in Permission')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <form class="app-ecommerce" action="{{ route('role-permissions.store') }}" method="post" autocomplete="off">
            @csrf
            <div class="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center mb-3">
                <div class="d-flex flex-column justify-content-center">
                    <h4 class="mb-1 mt-3">Add a new Role in Permission</h4>
                    <p>Please enter the field</p>
                </div>
                <div class="d-flex align-content-center flex-wrap gap-3">
                    <a href="{{ route('role-permissions') }}" class="btn btn-outline-secondary">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row g-4">
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <select name="role_id" id="role_id" class="form-select @error('role_id') is-invalid @enderror">
                                    <option value="">Select Role</option>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                                <label for="role_id">Role <span class="text-danger">(Required)</span></label>
                                @error('role_id')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <hr class="my-4 mx-n4">
                    <h6>Role Permissions</h6>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-flush-spacing">
                                <tbody>
                                <tr>
                                    <td class="text-nowrap fw-medium">Administrator Access <i class="mdi mdi-information-outline" data-bs-toggle="tooltip" data-bs-placement="top" title="Allows a full access to the system"></i></td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="selectAll" />
                                            <label class="form-check-label" for="selectAll">
                                                Select All
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                @foreach($permissionGroups as $group)
                                    <tr>
                                        <td class="text-nowrap text-capitalize fw-medium">{{ $group->group_name }}</td>
                                        @php
                                            $permissions = App\Models\User::getPermissionByGroupName($group->group_name);
                                        @endphp
                                        <td>
                                            <div class="d-flex">
                                                @foreach($permissions as $permission)
                                                    <div class="form-check me-3 me-lg-5">
                                                        <input
                                                            class="form-check-input"
                                                            type="checkbox"
                                                            id="{{ $permission->name }}"
                                                            value="{{ $permission->id }}"
                                                            name="permission[]"
                                                        />
                                                        <label class="form-check-label" for="{{ $permission->name }}">
                                                            {{ Str::of($permission->name)->explode('.')[1] ?? null }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.role-permission-main-menu').addClass('active open');
            $('.role-permission-sub-menu').addClass('active');

            selectAll.click(function () {
                if ($(this).is(':checked')) {
                    singleSelect.prop('checked', true);
                } else {
                    singleSelect.prop('checked', false);
                }
            });
        });

        // Initialize global variables
        const selectAll = $('#selectAll');
        const singleSelect = $('input[name="permission[]"]');
    </script>
@endsection
