@extends('layouts.backend-layout')

@section('title', 'Edit Staff')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <form class="app-ecommerce" action="{{ route('staff.update', $staff->id) }}" method="post" autocomplete="off" enctype="multipart/form-data">
            @csrf
            <!-- Add Staff -->
            <div class="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center mb-3">
                <div class="d-flex flex-column justify-content-center">
                    <h4 class="mb-1 mt-3">Edit Staff</h4>
                    <p>Please enter the field</p>
                </div>
                <div class="d-flex align-content-center flex-wrap gap-3">
                    <a href="{{ route('staff') }}" class="btn btn-outline-secondary">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h6>1. Staff Information</h6>
                    <div class="row g-4">
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <label for="input-file-max-fs">Maximum file upload size 2MB. (width-1080px & height-720px)</label>
                            <input
                                type="file"
                                id="input-file-max-fs"
                                name="photo"
                                class="dropify"
                                data-default-file="{{ url('uploads/staff/'.$staff->photo) }}"
                                data-max-file-size="2M"
                            />
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('first_name_en') is-invalid @enderror"
                                    id="first_name_en"
                                    value="{{ $staff->first_name_en }}"
                                    placeholder="Enter First Name (EN)"
                                    name="first_name_en"
                                />
                                <label for="first_name_en">First Name (EN) <span class="text-danger">(Required)</span></label>
                                @error('first_name_en')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control"
                                    id="middle_name_en"
                                    value="{{ $staff->middle_name_en }}"
                                    placeholder="Enter Middle Name (EN)"
                                    name="middle_name_en"
                                />
                                <label for="middle_name_en">Middle Name (EN)</label>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('last_name_en') is-invalid @enderror"
                                    id="last_name_en"
                                    value="{{ $staff->last_name_en }}"
                                    placeholder="Enter Last Name (EN)"
                                    name="last_name_en"
                                />
                                <label for="last_name_en">Last Name (EN) <span class="text-danger">(Required)</span></label>
                                @error('last_name_en')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('first_name_kh') is-invalid @enderror"
                                    id="first_name_kh"
                                    value="{{ $staff->first_name_kh }}"
                                    placeholder="Enter First Name (KH)"
                                    name="first_name_kh"
                                />
                                <label for="first_name_kh">First Name (KH) <span class="text-danger">(Required)</span></label>
                                @error('first_name_kh')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control"
                                    id="middle_name_kh"
                                    value="{{ $staff->middle_name_kh }}"
                                    placeholder="Enter Middle Name (KH)"
                                    name="middle_name_kh"
                                />
                                <label for="middle_name_kh">Middle Name (KH)</label>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('last_name_kh') is-invalid @enderror"
                                    id="last_name_kh"
                                    value="{{ $staff->last_name_kh }}"
                                    placeholder="Enter Last Name (KH)"
                                    name="last_name_kh"
                                />
                                <label for="last_name_kh">Last Name (KH) <span class="text-danger">(Required)</span></label>
                                @error('last_name_kh')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <select id="sex" name="sex" class="select2 form-select @error('sex') is-invalid @enderror">
                                    <option value="">Select Gender</option>
                                    <option value="Male" {{ $staff->sex === 'Male' ? 'selected' : '' }}>Male</option>
                                    <option value="Female" {{ $staff->sex === 'Female' ? 'selected' : '' }}>Female</option>
                                </select>
                                <label for="sex">Gender <span class="text-danger">(Required)</span></label>
                                @error('sex')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control flatpickr-date @error('dob') is-invalid @enderror"
                                    id="dob"
                                    value="{{ date('d-m-Y', strtotime($staff->dob)) }}"
                                    placeholder="DD-MM-YYYY"
                                    name="dob"
                                />
                                <label for="dob">Date of Birth <span class="text-danger">(Required)</span></label>
                                @error('dob')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="tel"
                                    class="form-control @error('tel') is-invalid @enderror"
                                    id="tel"
                                    value="{{ $staff->tel }}"
                                    placeholder="Enter Tel"
                                    name="tel"
                                />
                                <label for="tel">Tel <span class="text-danger">(Required)</span></label>
                                @error('tel')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control"
                                    id="email"
                                    value="{{ $staff->email }}"
                                    placeholder="Enter Email"
                                    name="email"
                                />
                                <label for="email">Email</label>
                            </div>
                        </div>
                        <div class="col-xxl-8 col-xl-8 col-lg-8 col-md-8 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control"
                                    id="birth_place"
                                    value="{{ $staff->birth_place }}"
                                    placeholder="Enter Birth Place"
                                    name="birth_place"
                                />
                                <label for="birth_place">Birth Place</label>
                            </div>
                        </div>
                        <div class="col-xxl-8 col-xl-8 col-lg-8 col-md-8 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control"
                                    id="present_address"
                                    value="{{ $staff->present_address }}"
                                    placeholder="Enter Present Address"
                                    name="present_address"
                                />
                                <label for="present_address">Present Address</label>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <select id="role" class="select2 form-select @error('role') is-invalid @enderror" name="role">
                                    <option value="">Select Role</option>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}" {{ $staff->hasRole($role->name) ? 'selected' : '' }}>{{ $role->name }}</option>
                                    @endforeach
                                </select>
                                <label for="role">Role <span class="text-danger">(Required)</span></label>
                                @error('role')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div class="form-check form-check-primary">
                                <input
                                    class="form-check-input"
                                    type="checkbox"
                                    value="1"
                                    id="activation"
                                    name="activation"
                                    {{ $staff->activation === 1 ? 'checked' : '' }}
                                />
                                <label class="form-check-label" for="activation">Activation</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.administrator-main-menu').addClass('active open');
            $('.staff-sub-menu').addClass('active');

            $('.dropify').dropify();

            $('.flatpickr-date').flatpickr({
                monthSelectorType: "static",
                dateFormat: "d-m-Y"
            });
        });
    </script>
@endsection
