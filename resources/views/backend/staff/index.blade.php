@extends('layouts.backend-layout')

@section('title', 'Staff')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="card-title">Staff</h5>
                @if(Auth::user()->can('Staff.Create'))
                    <a href="{{ route('staff.create') }}" class="btn btn-primary">Add New</a>
                @endif
            </div>
            <div class="card-datatable table-responsive text-nowrap">
                <table class="table">
                    <thead class="table-light">
                    <tr>
                        <th>#</th>
                        <th>Actions</th>
                        <th>Full Name (EN)</th>
                        <th>Full Name (KH)</th>
                        <th>Tel</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($staffs->count() > 0)
                        @foreach($staffs as $key => $value)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>
                                    @if(Auth::user()->can('Staff.Edit'))
                                        <a href="{{ route('staff.edit', $value->id) }}" class="btn btn-sm btn-icon me-2 btn-primary">
                                            <span class="tf-icons mdi mdi-pencil"></span>
                                        </a>
                                    @endif
                                    {{--<a href="{{ route('staff.delete', $value->id) }}" class="btn btn-sm btn-icon me-2 btn-danger btn-delete">
                                        <span class="tf-icons mdi mdi-trash-can"></span>
                                    </a>--}}
                                </td>
                                <td>{{ $value->last_name_en }} {{ $value->first_name_en }}</td>
                                <td>{{ $value->last_name_kh }} {{ $value->first_name_kh }}</td>
                                <td>{{ $value->tel }}</td>
                                <td>{{ $value->email }}</td>
                                <td class="text-capitalize">
                                    @foreach($value->roles as $role)
                                        <span class="badge bg-label-info">{{ $role->name }}</span>
                                    @endforeach
                                </td>
                                <td>
                                    <span class="badge {{ $value->activation === 1 ? 'bg-label-success' : 'bg-label-danger' }}">
                                        {{ $value->activation === 1 ? 'Active' : 'Inactive' }}
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="text-center" colspan="6">No data is available in table</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="mx-3">
                {!! $staffs->withQueryString()->links('vendor.pagination.bootstrap-5') !!}
            </div>
        </div>

        <!-- Modal Preview -->
        <div class="modal fade" id="modalPreview" tabindex="-1" aria-labelledby="modalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalPreviewTitle">Preview Description</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="preview-data"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.administrator-main-menu').addClass('active open');
            $('.staff-sub-menu').addClass('active');
        });
    </script>
@endsection
