@extends('layouts.backend-layout')

@section('title', 'Edit Course')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <form class="app-ecommerce" action="{{ route('courses.update', $course->id) }}" method="post" autocomplete="off" enctype="multipart/form-data">
            @csrf
            <div class="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center mb-3">
                <div class="d-flex flex-column justify-content-center">
                    <h4 class="mb-1 mt-3">Edit Course</h4>
                    <p>Please enter the field</p>
                </div>
                <div class="d-flex align-content-center flex-wrap gap-3">
                    <a href="{{ route('courses') }}" class="btn btn-outline-secondary">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row g-4">
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <label for="input-file-max-fs">Maximum file upload size 2MB. (width-1080px & height-720px)</label>
                            <input
                                type="file"
                                id="input-file-max-fs"
                                name="image"
                                class="dropify"
                                data-default-file="{{ url('uploads/course/', $course->image) }}"
                                data-max-file-size="2M"
                            />
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('title_en') is-invalid @enderror"
                                    id="title_en"
                                    value="{{ $course->title_en }}"
                                    placeholder="Enter Course Name (EN)"
                                    name="title_en"
                                />
                                <label for="title_en">Course Name (EN) <span class="text-danger">(Required)</span></label>
                                @error('title_en')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    class="form-control @error('title_kh') is-invalid @enderror"
                                    id="title_kh"
                                    value="{{ $course->title_kh }}"
                                    placeholder="Enter Course Name (KH)"
                                    name="title_kh"
                                />
                                <label for="title_kh">Course Name (KH) <span class="text-danger">(Required)</span></label>
                                @error('title_kh')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <textarea
                                class="form-control @error('description') is-invalid @enderror"
                                id="description"
                                name="description"
                                placeholder="Enter Description"
                            >
                                {{ $course->description }}
                            </textarea>
                            @error('description')
                            <div class="invalid-feedback"> {{ $message }} </div>
                            @enderror
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div class="form-check form-check-primary">
                                <input
                                    class="form-check-input"
                                    type="checkbox"
                                    value="1"
                                    id="activation"
                                    name="activation"
                                    {{ $course->activation === 1 ? 'checked' : '' }}
                                />
                                <label class="form-check-label" for="activation">Activation</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.institute-main-menu').addClass('active open');
            $('.course-sub-menu').addClass('active');

            $('.dropify').dropify();

            ClassicEditor
                .create( document.querySelector( '#description' ) )
                .catch( error => {
                    console.error( error );
                } );
        });
    </script>
@endsection
