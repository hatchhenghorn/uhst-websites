@extends('layouts.frontend-layout')

@section('title', 'Partner')
@section('content')
    <!-- Pageheader start-->
    <div class="pattern-square"></div>
    <section class="bg-light py-5 py-lg-8 bg-opacity-50">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div>
                        <h5 class="mb-0"><a href="{{ route('home') }}">Home</a> / Partner</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Pageheader end-->

    <!--save your time-->
    <section class="my-lg-5 my-5">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-3 col-md-12" data-cue="fadeIn">
                    <div class="text-center mb-lg-7 mb-5">
                        <h2 class="mt-5 px-lg-10 px-6">
                            Our
                            <span class="text-primary">Partner</span>
                        </h2>
                        <p class="mb-0">
                            Partner of
                            <span class="text-dark">UHST.</span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row g-5">
                @foreach($partner as $item)
                    <div class="col-lg-3 col-md-6">
                        <div class="card text-center card-lift">
                            <div class="card-body py-5">
                                <figure class="mb-4">
                                    <img src="{{ url('uploads/partner/'.$item->image) }}" alt="avatar" class="avatar avatar-xxl rounded-circle" />
                                </figure>

                                <h5 class="mb-1">{{ $item->title_en }}</h5>
                                {{--<small>Co-Founder & CEO</small>--}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!--save your time-->
@endsection
