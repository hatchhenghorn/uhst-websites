@extends('layouts.frontend-layout')

@section('title', 'Project')
@section('content')
    <!-- Pageheader start-->
    <div class="pattern-square"></div>
    <section class="bg-light py-5 py-lg-8 bg-opacity-50">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div>
                        <h5 class="mb-0"><a href="{{ route('home') }}">Home</a> / Project</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Pageheader end-->

    <!--project-->
    <section class="container my-xl-5 my-5">
        <div class="row">
            <div class="col-xl-6 offset-xl-3 col-md-12" data-cue="fadeIn">
                <div class="text-center mb-lg-7 mb-5">
                    <h2 class="mt-5 px-lg-10 px-6">
                        Our
                        <span class="text-primary">Project</span>
                    </h2>
                    <p class="mb-0">Project <span class="text-dark">.</span></p>
                </div>
            </div>
        </div>
        <div class="row gy-6 mb-6">
            @foreach($project as $item)
                <div class="col-lg-4 col-12">
                    <div class="card overflow-hidden card-lift">
                        <div class="card-body me-xl-8">
                            <h4>{{ $item->title_en }}</h4>
                            <div class="mb-0">{!! $item->description !!}</div>
                        </div>
                        <div class="text-end ms-4">
                            <figure>
                                <img src="{{ url('uploads/project/'.$item->image) }}" class="img-fluid" alt="Project Image" />
                            </figure>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <!--project end-->
@endsection
