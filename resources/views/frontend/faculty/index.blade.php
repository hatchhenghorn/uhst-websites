@extends('layouts.frontend-layout')

@section('title', 'Faculty')
@section('content')
    <!-- Pageheader start-->
    <div class="pattern-square"></div>
    <section class="bg-light py-5 py-lg-8 bg-opacity-50">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div>
                        <h5 class="mb-0"><a href="{{ route('home') }}">Home</a> / Faculty</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Pageheader end-->

    <!--faculty start-->
    <section class="my-xl-9 my-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="text-center mb-xl-7 mb-5">
                        <h2 class="mb-3">
                            Our
                            <span class="text-primary">Faculty</span>
                        </h2>
                        <p class="mb-0">
                            Faculty refers to the collective body of educators or teachers within an educational institution such as a university, college, or school.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-xl-5 col-md-6 col-12">
                    <div class="nav flex-column nav-pills mb-5 mb-lg-0"
                         id="v-pills-tab" role="tablist"
                         aria-orientation="vertical">
                        <a href="#"
                           class="nav-link active d-flex text-start align-items-center align-items-lg-start p-xl-4 p-3"
                           id="v-pills-small-business-tab"
                           data-bs-toggle="pill"
                           data-bs-target="#v-pills-small-business" role="tab"
                           aria-controls="v-pills-small-business"
                           aria-selected="true">
                            <div class="d-flex">
                                <div
                                    class="icon-md icon-shape rounded-circle bg-white shadow-sm">
                                    <svg xmlns="http://www.w3.org/2000/svg"
                                         width="18" height="18"
                                         fill="currentColor"
                                         class="bi bi-bank2 text-primary"
                                         viewBox="0 0 16 16">
                                        <path
                                            d="M8.277.084a.5.5 0 0 0-.554 0l-7.5 5A.5.5 0 0 0 .5 6h1.875v7H1.5a.5.5 0 0 0 0 1h13a.5.5 0 1 0 0-1h-.875V6H15.5a.5.5 0 0 0 .277-.916l-7.5-5zM12.375 6v7h-1.25V6h1.25zm-2.5 0v7h-1.25V6h1.25zm-2.5 0v7h-1.25V6h1.25zm-2.5 0v7h-1.25V6h1.25zM8 4a1 1 0 1 1 0-2 1 1 0 0 1 0 2zM.5 15a.5.5 0 0 0 0 1h15a.5.5 0 1 0 0-1H.5z"/>
                                    </svg>
                                </div>
                            </div>
                            <div class="ms-4">
                                <h4 class="mb-0">{{ $faculty->title_en }}</h4>
                                <div class="mb-0 mt-lg-3 d-none d-lg-block">{!! $faculty->description !!}</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xl-6 offset-xl-1 col-md-6 col-12">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active"
                             id="v-pills-small-business" role="tabpanel"
                             aria-labelledby="v-pills-small-business-tab"
                             tabindex="0">
                            <div class="position-relative scene"
                                 data-relative-input="true">
                                <figure>
                                    <img src="{{ url('uploads/faculty/'. $faculty->image) }}" alt="finance" class="img-fluid rounded-3">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--faculty end-->

    <!--institute start-->
    @if($institutes->count() > 0)
        <section class="my-lg-9 my-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                        <div class="text-center mb-xl-7 mb-5">
                            <h2 class="mb-0">Our Institute</h2>
                        </div>
                    </div>
                </div>
                <div class="row g-lg-7 gy-5">
                    @foreach($institutes as $institute)
                    <a href="{{ route('institute', $institute->id) }}" class="col-lg-4 col-md-4 text-body">
                        <div class="mb-5">
                            <svg xmlns="http://www.w3.org/2000/svg" width="32"
                                 height="32" fill="currentColor"
                                 class="bi bi-rocket-takeoff text-primary"
                                 viewBox="0 0 16 16">
                                <path d="M14.763.075A.5.5 0 0 1 15 .5v15a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5V14h-1v1.5a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5V10a.5.5 0 0 1 .342-.474L6 7.64V4.5a.5.5 0 0 1 .276-.447l8-4a.5.5 0 0 1 .487.022M6 8.694 1 10.36V15h5zM7 15h2v-1.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 .5.5V15h2V1.309l-7 3.5z"/>
                                <path d="M2 11h1v1H2zm2 0h1v1H4zm-2 2h1v1H2zm2 0h1v1H4zm4-4h1v1H8zm2 0h1v1h-1zm-2 2h1v1H8zm2 0h1v1h-1zm2-2h1v1h-1zm0 2h1v1h-1zM8 7h1v1H8zm2 0h1v1h-1zm2 0h1v1h-1zM8 5h1v1H8zm2 0h1v1h-1zm2 0h1v1h-1zm0-2h1v1h-1z"/>
                            </svg>
                        </div>

                        <h4>{{ $institute->title_en }}</h4>
                        <div class="mb-0">{!! Str::limit($institute->description, 100, '...') !!}</div>
                    </a>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
    <!--institute end-->
@endsection
