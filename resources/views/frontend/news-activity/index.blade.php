@extends('layouts.frontend-layout')

@section('title', 'News & Activities')
@section('content')
    <!-- Pageheader start-->
    <div class="pattern-square"></div>
    <section class="bg-light py-5 py-lg-8 bg-opacity-50">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div>
                        <h5 class="mb-0"><a href="{{ route('home') }}">Home</a> / News & Activities</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Pageheader end-->

    <!--news & activity start-->
    {{--<section class="my-xl-8 my-5">
        <div class="container">
            <div class="col-xl-6 offset-xl-3 col-md-12" data-cue="fadeIn">
                <div class="text-center mb-lg-7 mb-5">
                    <h2 class="mt-5 px-lg-10 px-6">
                        Our
                        <span class="text-primary">News & Activities</span>
                    </h2>
                    <p class="mb-0">
                        Looking
                        <span class="text-dark">News & Activities.</span>
                    </p>
                </div>
            </div>
            @foreach($newsActivity as $item)
                <div class="table-responsive-lg">
                    <div class="row g-5 flex-nowrap pb-4 pb-lg-0 me-5 me-lg-0">
                        <div class="col-lg-6 col-md-6 col-12" data-cue="zoomIn">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $item->title_en }}</h5>
                                    <h6 class="card-subtitle mb-2 text-body-secondary">{{ $item->sub_title_en }}</h6>
                                    <div class="card-text">{!! $item->description !!}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>--}}
    <!--news & activity end-->

    <!--All events start-->
    <section class="mb-xl-9 my-5">
        <div class="container">
            <div class="row mb-4">
                <div class="col-lg-12">
                    <div class="mb-4">
                        <h3 class="mb-4">News & Activities</h3>
                    </div>
                </div>
            </div>

            <div class="row g-5">
                @foreach($newsActivity as $item)
                <div class="col-md-6">
                    <div class="card shadow-sm h-100 border-0 card-lift overflow-hidden">
                        <div class="row h-100 g-0">
                            <a href="" class="col-lg-5 col-md-12" style="
                                 background-image: url({{ url('uploads/news-activities/'.$item->image) }});
                                 background-size: cover;
                                 background-repeat: no-repeat;
                                 background-position: center;
                                 min-height: 13rem;
                              ">
                            </a>
                            <div class="col-lg-7 col-md-12">
                                <div class="card-body h-100 d-flex align-items-start flex-column border rounded-end-lg-3 rounded-bottom-3 rounded-top-0 rounded-start-lg-0 border-start-lg-0 border-top-0 border-top-lg">
                                    <div class="mb-5">
                                        <h4 class="my-2"><a href="#" class="text-reset"></a>{{ $item->title_en }}</h4>
                                        <div>{!! Str::limit($item->description, 100, '...') !!}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!--All events end-->
@endsection
