@extends('layouts.frontend-layout')

@section('title', 'History')
@section('content')
    <!-- Pageheader start-->
    <div class="pattern-square"></div>
    <section class="bg-light py-5 py-lg-8 bg-opacity-50">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div>
                        <h5 class="mb-0"><a href="{{ route('home') }}">Home</a> / History</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Pageheader end-->

    <!--Visualize & plan start-->
    <section class="my-xl-7 py-5">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-6 col-12">
                    <div class="mb-5 mt-4">
                        <h2 class="mb-3">{{ $history[0]->title_en }}</h2>
                        <p class="mb-0 lead">{!! $history[0]->description !!}</p>
                    </div>
                </div>
                <div class="col-lg-6 offset-lg-1 col-md-6 col-12">
                    <div class="position-relative rellax" data-rellax-percentage="1" data-rellax-speed="0.8" data-disable-parallax-down="md">
                        <figure>
                            <img src="{{ asset('frontend-assets/images/hero/img-5.jpg') }}" alt="landing" class="img-fluid rounded-4" />
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Visualize & plan end-->
@endsection
