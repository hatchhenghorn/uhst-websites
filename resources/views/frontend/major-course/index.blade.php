@extends('layouts.frontend-layout')

@section('title', 'Major & Course')
@section('content')
    <!-- Pageheader start-->
    <div class="pattern-square"></div>
    <section class="bg-light py-5 py-lg-8 bg-opacity-50">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div>
                        <h5 class="mb-0"><a href="{{ route('home') }}">Home</a> / Major & Course</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Pageheader end-->

    <!--major course start-->
    <section class="my-xl-9 my-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="text-center mb-xl-7 mb-5">
                        <h2 class="mb-3">
                            Our
                            <span class="text-primary">Major & Course</span>
                        </h2>
                        <p class="mb-0">
                            Faculty refers to the collective body of educators or teachers within an educational institution such as a university, college, or school.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <!-- Nesting table -->
                    <table class="table table-bordered">
                        <thead class="bg-light">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Major</th>
                            <th scope="col">Course</th>
                            <th scope="col">Semester</th>
                            <th scope="col">Year</th>
                            <th scope="col">Credit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($majorCourses as $key => $majorCourse)
                            <tr>
                                <th scope="row">{{ ++$key }}</th>
                                <td>{{ $majorCourse->title }}</td>
                                <td>{{ $majorCourse->major->title_en }}</td>
                                <td>{{ $majorCourse->course->title_en }}</td>
                                <td>{{ $majorCourse->semester }}</td>
                                <td>{{ $majorCourse->year }}</td>
                                <td>{{ $majorCourse->credit }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--major course end-->
@endsection
