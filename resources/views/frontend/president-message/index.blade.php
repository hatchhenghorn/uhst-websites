@extends('layouts.frontend-layout')

@section('title', 'President Message')
@section('content')
    <!-- Pageheader start-->
    <div class="pattern-square"></div>
    <section class="bg-light py-5 py-lg-8 bg-opacity-50">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div>
                        <h5 class="mb-0"><a href="{{ route('home') }}">Home</a> / President Message</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Pageheader end-->

    <!-- president message -->
    <section class="my-9">
        <div class="container">
            <div class="row line-pattern bg-primary-gradient rounded-3 p-7 g-0" data-cue="fadeIn">
                <div class="col-lg-8 offset-lg-2 z-1">
                    <div class="py-md-7">
                        <h2 class="h1 text-white-stable">President Message</h2>
                        <div class="text-white-50">
                            {!! !empty($presidentMessage[0]->description) ? $presidentMessage[0]->description : '' !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- president message end -->
@endsection
