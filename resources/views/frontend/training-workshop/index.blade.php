@extends('layouts.frontend-layout')

@section('title', 'Training & Workshop')
@section('content')
    <!-- Pageheader start-->
    <div class="pattern-square"></div>
    <section class="bg-light py-5 py-lg-8 bg-opacity-50">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div>
                        <h5 class="mb-0"><a href="{{ route('home') }}">Home</a> / Training & Workshop</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Pageheader end-->

    <!--Training & Workshop start-->
    <section class="py-5 py-lg-5 bg-light-subtle">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-3 col-md-12" data-cue="fadeIn">
                    <div class="text-center mb-lg-7 mb-5">
                        <h2 class="mt-5 px-6">
                            Our
                            <span class="text-primary">Training & Workshop</span>
                        </h2>
                        <p class="mb-0">
                            Training & Workshop at
                            <span class="text-dark">UHST.</span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                {{--<div class="col-lg-10 offset-lg-1">
                    <div class="swiper sliderSwiper">
                        <div class="swiper-wrapper pb-6">
                            @foreach($trainingWorkshop as $item)
                                <div class="swiper-slide">
                                    <div class="card">
                                        <div class="row align-items-center">
                                            <div
                                                class="col-xl-4 col-lg-5 col-md-6 overflow-hidden d-none d-lg-block">
                                                <img src="{{ url('uploads/training-workshop/'.$item->image) }}" class="img-fluid rounded-start-md-3 rounded-top-md-0 rounded-top-3" alt="Training & Workshop" />
                                            </div>
                                            <div
                                                class="col-xl-8 col-lg-7 col-md-12">
                                                <div class="card-body p-4 px-xl-0">
                                                    <div class="mb-6 text-inverse">
                                                        <h4>{{ $item->title_en }}</h4>
                                                        <h5>{{ $item->sub_title_en }}</h5>
                                                    </div>
                                                    <p class="card-text lead mb-7">
                                                        "Lorem ipsum dolor sit amet,
                                                        consectetur adipiscing elit.
                                                        Suspendisse varius enim in
                                                        eros elementum tristique.
                                                        Duis cursus, mi quis viverra
                                                        ornare."
                                                    </p>

                                                    <h5 class="mb-0">Start & Date Time</h5>
                                                    <small class="text-body-tertiary">
                                                        {{ date('d-M-Y', strtotime($item->start_date)) }} / {{ date('d-M-Y', strtotime($item->end_date)) }}
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>--}}
                @foreach($trainingWorkshop as $item)
                    <div class="col-12 col-xxl-4 col-md-4 mb-4" data-cue="fadeIn">
                        <div class="card h-100">
                            <div class="card-body">
                                <div class="bg-label-primary text-center mb-3 pt-2 rounded-2">
                                    <img class="img-fluid w-px-150" src="{{ url('uploads/training-workshop/'.$item->image) }}" alt="Training & Workshop">
                                </div>
                                <h5 class="mb-2 pb-1">{{ $item->title_en }}</h5>
                                <p>{{ $item->sub_title_en }}</p>
                                <div class="row mb-4 g-3">
                                    <div class="col-6">
                                        <div class="d-flex">
                                            <div>
                                                <h6 class="mb-0 text-nowrap">{{ date('d M Y', strtotime($item->start_date)) }}</h6>
                                                <small>Start Date</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="d-flex">
                                            <div class="avatar flex-shrink-0 me-2">
                                                <span class="avatar-initial rounded bg-label-primary"><i class="mdi mdi-timer-outline mdi-24px"></i></span>
                                            </div>
                                            <div>
                                                <h6 class="mb-0 text-nowrap">{{ date('d M Y', strtotime($item->end_date)) }}</h6>
                                                <small>End Date</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!--Training & Workshop end-->
@endsection
