@extends('layouts.frontend-layout')

@section('title', 'Conference')
@section('content')
    <!-- Pageheader start-->
    <div class="pattern-square"></div>
    <section class="bg-light py-5 py-lg-8 bg-opacity-50">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div>
                        <h5 class="mb-0"><a href="{{ route('home') }}">Home</a> / Conference</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Pageheader end-->

    <!--conference-->
    <section class="container my-xl-5 my-5">
        <div class="row">
            <div class="col-xl-6 offset-xl-3 col-md-12" data-cue="fadeIn">
                <div class="text-center mb-lg-7 mb-5">
                    <h2 class="mt-5 px-lg-10 px-6">
                        Our
                        <span class="text-primary">Conference</span>
                    </h2>
                    <p class="mb-0">Conference <span class="text-dark">.</span></p>
                </div>
            </div>
        </div>
        <div class="row gy-6 mb-6">
            @foreach($conference as $item)
                <div class="col-lg-4 col-12">
                    <div class="card h-100">
                        <img class="card-img-top h-px-200" src="{{ url('uploads/conference/'.$item->image) }}" alt="cafe-bg" style="object-fit: cover;">
                        <div class="card-body">
                            <h5 class="mb-2">{{ $item->title_en }}</h5>
                            <p>{{ $item->sub_title_en }}</p>
                            <span class="badge bg-info rounded-pill p-2">{{ date('d M Y', strtotime($item->start_date)) }} - {{ date('d M Y', strtotime($item->end_date)) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <!--conference end-->
@endsection
