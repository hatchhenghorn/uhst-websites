@extends('layouts.frontend-layout')

@section('title', 'Institute')
@section('content')
    <!-- Pageheader start-->
    <div class="pattern-square"></div>
    <section class="bg-light py-5 py-lg-8 bg-opacity-50">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div>
                        <h5 class="mb-0"><a href="{{ route('home') }}">Home</a> / Institute</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Pageheader end-->

    <!--institute start-->
    <section class="my-xl-9 my-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="text-center mb-xl-7 mb-5">
                        <h2 class="mb-3">
                            Our
                            <span class="text-primary">Institute</span>
                        </h2>
                        <p class="mb-0">
                            Faculty refers to the collective body of educators or teachers within an educational institution such as a university, college, or school.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-xl-5 col-md-6 col-12">
                    <div class="nav flex-column nav-pills mb-5 mb-lg-0"
                         id="v-pills-tab" role="tablist"
                         aria-orientation="vertical">
                        <a href="#"
                           class="nav-link active d-flex text-start align-items-center align-items-lg-start p-xl-4 p-3"
                           id="v-pills-small-business-tab"
                           data-bs-toggle="pill"
                           data-bs-target="#v-pills-small-business" role="tab"
                           aria-controls="v-pills-small-business"
                           aria-selected="true">
                            <div class="d-flex">
                                <div
                                    class="icon-md icon-shape rounded-circle bg-white shadow-sm">
                                    <svg xmlns="http://www.w3.org/2000/svg"
                                         width="18" height="18"
                                         fill="currentColor"
                                         class="bi bi-bank2 text-primary"
                                         viewBox="0 0 16 16">
                                        <path
                                            d="M8.277.084a.5.5 0 0 0-.554 0l-7.5 5A.5.5 0 0 0 .5 6h1.875v7H1.5a.5.5 0 0 0 0 1h13a.5.5 0 1 0 0-1h-.875V6H15.5a.5.5 0 0 0 .277-.916l-7.5-5zM12.375 6v7h-1.25V6h1.25zm-2.5 0v7h-1.25V6h1.25zm-2.5 0v7h-1.25V6h1.25zm-2.5 0v7h-1.25V6h1.25zM8 4a1 1 0 1 1 0-2 1 1 0 0 1 0 2zM.5 15a.5.5 0 0 0 0 1h15a.5.5 0 1 0 0-1H.5z"/>
                                    </svg>
                                </div>
                            </div>
                            <div class="ms-4">
                                <h4 class="mb-0">{{ $institute->title_en }}</h4>
                                <div class="mb-0 mt-lg-3 d-none d-lg-block">{!! $institute->description !!}</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xl-6 offset-xl-1 col-md-6 col-12">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active"
                             id="v-pills-small-business" role="tabpanel"
                             aria-labelledby="v-pills-small-business-tab"
                             tabindex="0">
                            <div class="position-relative scene"
                                 data-relative-input="true">
                                <figure>
                                    <img src="{{ url('uploads/faculty/'. $institute->faculty->image) }}" alt="finance" class="img-fluid rounded-3">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--<div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Heading</th>
                                        <th scope="col">Heading</th>
                                        <th scope="col">Heading</th>
                                        <th scope="col">Heading</th>
                                        <th scope="col">Heading</th>
                                        <th scope="col">Heading</th>
                                        <th scope="col">Heading</th>
                                        <th scope="col">Heading</th>
                                        <th scope="col">Heading</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                        <td>Cell</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--}}
        </div>
    </section>
    <!--institute end-->

    <!--major start-->
    <section class="my-xl-9 my-5">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2">
                    <div class="text-center mb-xl-7 mb-5">
                        <h2 class="mb-3">
                            Major
                            <span class="text-primary">of Institute</span>
                        </h2>
                        {{--<p class="mb-0">
                            Designed to work better together erat velit eget hac
                            nulla nullam et id praesent nisi ornare risus risus
                            consequat nunc nisl pellentesque diam neque.
                        </p>--}}
                    </div>
                </div>
            </div>
            <div class="table-responsive-lg">
                <div class="row g-5">
                    @foreach($majors as $major)
                        <div class="col-lg-4 col-md-6">
                            <div class="card border-0 card-primary">
                                <div class="card-body p-5">
                                    <div
                                        class="position-relative d-inline-block mb-5">
                                        <img src="{{ url('uploads/major/'. $major->image) }}" alt="feature" class="avatar avatar-xl rounded-circle border-2 border border-white shadow-sm" />
                                        <div
                                            class="position-absolute bottom-0 end-0">
                                            <div class="icon-md icon-shape rounded-circle bg-white me-n2 mb-n2 shadow-sm">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                     width="18" height="18"
                                                     fill="currentColor"
                                                     class="bi bi-bank2 text-primary"
                                                     viewBox="0 0 16 16">
                                                    <path d="M8.277.084a.5.5 0 0 0-.554 0l-7.5 5A.5.5 0 0 0 .5 6h1.875v7H1.5a.5.5 0 0 0 0 1h13a.5.5 0 1 0 0-1h-.875V6H15.5a.5.5 0 0 0 .277-.916l-7.5-5zM12.375 6v7h-1.25V6h1.25zm-2.5 0v7h-1.25V6h1.25zm-2.5 0v7h-1.25V6h1.25zm-2.5 0v7h-1.25V6h1.25zM8 4a1 1 0 1 1 0-2 1 1 0 0 1 0 2zM.5 15a.5.5 0 0 0 0 1h15a.5.5 0 1 0 0-1H.5z" />
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-5">
                                        <h4 class="card-title">{{ $major->title_en }}</h4>
                                        <div class="mb-0 card-text">{!! Str::limit($major->description, 100, '...') !!}</div>
                                    </div>

                                    <a href="{{ route('major-course', $major->id) }}" class="icon-link icon-link-hover card-link">
                                        View More
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             width="14" height="14"
                                             fill="currentColor"
                                             class="bi bi-arrow-right"
                                             viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!--major end-->
@endsection
