@extends('layouts.frontend-layout')
@section('title', 'Home')
@section('content')
    <!--Hero start-->
    <div class="swiper sliderSwiper">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div data-cue="fadeIn" class="bg-dark" style="background-image: url({{ asset('frontend-assets/images/hero/img-8.jpg') }}); background-position: center; background-size: cover; background-repeat: no-repeat">
                    <section class="py-9">
                        <div class="container py-9">
                            <div class="row">
                                <div class="col-lg-10 offset-lg-1">
                                    <div class="text-center py-lg-8" data-cue="zoomIn">
                                        <span class="border border-warning text-warning px-3 rounded-pill py-1 fs-6">March 24 - 26, 2024 | San Diego, CA</span>
                                        <div class="mt-5 mx-xl-7">
                                            <h1 class="text-white-stable display-5 mb-3">Graduate School of Institute of Technology o Cambodia</h1>
                                            <p class="mb-0 lead px-xl-7 mx-xl-7 text-white-stable">
                                                Together for Excellence and Sustainable Growth
                                            </p>
                                        </div>
                                        <div class="mt-6 d-grid d-md-inline-flex mx-4">
                                            <!-- placement -->
                                            <a href="offcanvasRight.html" class="btn btn-primary me-md-2 mb-3 mb-md-0" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">
                                                Register Now
                                            </a>

                                            <a href="#" class="btn btn-outline-primary">
                                                Add to Calender
                                                <span class="ms-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-window-dock" viewBox="0 0 16 16">
                                       <path
                                           d="M3.5 11a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1Zm3.5.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1Zm4.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1Z" />
                                       <path
                                           d="M14 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h12ZM2 14h12a1 1 0 0 0 1-1V5H1v8a1 1 0 0 0 1 1ZM2 2a1 1 0 0 0-1 1v1h14V3a1 1 0 0 0-1-1H2Z" />
                                    </svg>
                                 </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="swiper-slide">
                <div data-cue="fadeIn" class="bg-dark" style="background-image: url({{ asset('frontend-assets/images/hero/img-10.jpg') }}); background-position: center; background-size: cover; background-repeat: no-repeat">
                    <section class="py-9">
                        <div class="container py-9">
                            <div class="row">
                                <div class="col-lg-10 offset-lg-1">
                                    <div class="text-center py-lg-8" data-cue="zoomIn">
                                        <span class="border border-warning text-warning px-3 rounded-pill py-1 fs-6">March 24 - 26, 2024 | San Diego, CA</span>
                                        <div class="mt-5 mx-xl-7">
                                            <h1 class="text-white-stable display-5 mb-3">Graduate School of Institute of Technology o Cambodia</h1>
                                            <p class="mb-0 lead px-xl-7 mx-xl-7 text-white-stable">
                                                Together for Excellence and Sustainable Growth
                                            </p>
                                        </div>
                                        <div class="mt-6 d-grid d-md-inline-flex mx-4">
                                            <!-- placement -->
                                            <a href="offcanvasRight.html" class="btn btn-primary me-md-2 mb-3 mb-md-0" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">
                                                Register Now
                                            </a>

                                            <a href="#" class="btn btn-outline-primary">
                                                Add to Calender
                                                <span class="ms-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-window-dock" viewBox="0 0 16 16">
                                       <path
                                           d="M3.5 11a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1Zm3.5.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1Zm4.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1Z" />
                                       <path
                                           d="M14 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h12ZM2 14h12a1 1 0 0 0 1-1V5H1v8a1 1 0 0 0 1 1ZM2 2a1 1 0 0 0-1 1v1h14V3a1 1 0 0 0-1-1H2Z" />
                                    </svg>
                                 </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="swiper-slide">
                <div data-cue="fadeIn" class="bg-dark" style="background-image: url({{ asset('frontend-assets/images/hero/img-9.jpg') }}); background-position: center; background-size: cover; background-repeat: no-repeat">
                    <section class="py-9">
                        <div class="container py-9">
                            <div class="row">
                                <div class="col-lg-10 offset-lg-1">
                                    <div class="text-center py-lg-8" data-cue="zoomIn">
                                        <span class="border border-warning text-warning px-3 rounded-pill py-1 fs-6">March 24 - 26, 2024 | San Diego, CA</span>
                                        <div class="mt-5 mx-xl-7">
                                            <h1 class="text-white-stable display-5 mb-3">Graduate School of Institute of Technology o Cambodia</h1>
                                            <p class="mb-0 lead px-xl-7 mx-xl-7 text-white-stable">
                                                Together for Excellence and Sustainable Growth
                                            </p>
                                        </div>
                                        <div class="mt-6 d-grid d-md-inline-flex mx-4">
                                            <!-- placement -->
                                            <a href="offcanvasRight.html" class="btn btn-primary me-md-2 mb-3 mb-md-0" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">
                                                Register Now
                                            </a>

                                            <a href="#" class="btn btn-outline-primary">
                                                Add to Calender
                                                <span class="ms-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-window-dock" viewBox="0 0 16 16">
                                       <path
                                           d="M3.5 11a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1Zm3.5.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1Zm4.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1Z" />
                                       <path
                                           d="M14 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h12ZM2 14h12a1 1 0 0 0 1-1V5H1v8a1 1 0 0 0 1 1ZM2 2a1 1 0 0 0-1 1v1h14V3a1 1 0 0 0-1-1H2Z" />
                                    </svg>
                                 </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="swiper-pagination"></div>
    </div>
    <!--Hero end-->

    <!--Preview image start-->
    <section class="mb-xl-9 my-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h3 class="mb-4">News & Activities</h3>
                </div>
            </div>
            <div class="row g-5">
                <div class="swiper mySwiper py-3">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="card border-0 shadow-sm h-100 card-lift">
                                <figure>
                                    <a href="">
                                        <img src="{{ asset('frontend-assets/images/hero/img-10.jpg') }}" alt="event" class="card-img-top">
                                    </a>
                                </figure>

                                <div class="card-body h-100 d-flex align-items-start flex-column border rounded-bottom-3 border-top-0">
                                    <div class="mb-5">
                                        <small class="text-uppercase fw-semibold ls-md">Webinar</small>
                                        <h4 class="my-2"><a href="" class="text-reset">How to build a blog with Astro and Contentful</a></h4>
                                        <small>June 22, 2024</small>
                                    </div>
                                    <div class="d-flex justify-content-between w-100 mt-auto">
                                        <small>9:00AM EDT</small>
                                        <small>Germany</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="card border-0 shadow-sm h-100 card-lift">
                                <figure>
                                    <a href="event-single.html">
                                        <img src="{{ asset('frontend-assets/images/hero/img-10.jpg') }}" alt="event" class="card-img-top">
                                    </a>
                                </figure>

                                <div class="card-body h-100 d-flex align-items-start flex-column border rounded-bottom-3 border-top-0">
                                    <div class="mb-5">
                                        <small class="text-uppercase fw-semibold ls-md">Webinar</small>
                                        <h4 class="my-2"><a href="event-single.html" class="text-reset">How to build a blog with Astro and Contentful</a></h4>
                                        <small>June 22, 2024</small>
                                    </div>
                                    <div class="d-flex justify-content-between w-100 mt-auto">
                                        <small>9:00AM EDT</small>
                                        <small>Germany</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="card border-0 shadow-sm h-100 card-lift">
                                <figure>
                                    <a href="event-single.html">
                                        <img src="{{ asset('frontend-assets/images/hero/img-10.jpg') }}" alt="event" class="card-img-top">
                                    </a>
                                </figure>

                                <div class="card-body h-100 d-flex align-items-start flex-column border rounded-bottom-3 border-top-0">
                                    <div class="mb-5">
                                        <small class="text-uppercase fw-semibold ls-md">Webinar</small>
                                        <h4 class="my-2"><a href="event-single.html" class="text-reset">How to build a blog with Astro and Contentful</a></h4>
                                        <small>June 22, 2024</small>
                                    </div>
                                    <div class="d-flex justify-content-between w-100 mt-auto">
                                        <small>9:00AM EDT</small>
                                        <small>Germany</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="card border-0 shadow-sm h-100 card-lift">
                                <figure>
                                    <a href="event-single.html">
                                        <img src="{{ asset('frontend-assets/images/hero/img-10.jpg') }}" alt="event" class="card-img-top">
                                    </a>
                                </figure>

                                <div class="card-body h-100 d-flex align-items-start flex-column border rounded-bottom-3 border-top-0">
                                    <div class="mb-5">
                                        <small class="text-uppercase fw-semibold ls-md">Webinar</small>
                                        <h4 class="my-2"><a href="event-single.html" class="text-reset">How to build a blog with Astro and Contentful</a></h4>
                                        <small>June 22, 2024</small>
                                    </div>
                                    <div class="d-flex justify-content-between w-100 mt-auto">
                                        <small>9:00AM EDT</small>
                                        <small>Germany</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-button-next shadow-lg"></div>
                    <div class="swiper-button-prev shadow-lg"></div>
                    <div class="swiper-pagination"></div>
                </div>
                <div class="text-center">
                    <a href="{{ route('news-activity') }}" class="icon-link icon-link-hover mt-3 mt-md-0">
                        <span>See all news & activity</span>
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"></path>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!--Preview image end-->

    <!--faculty & institute start-->
    <section class="my-xl-7 py-5">
        <div class="container mb-xl-7" data-cue="fadeIn">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="text-center mb-7 pb-2">
                        <h2 class="mb-3">
                            Our
                            <span class="text-primary">Faculties</span>
                        </h2>

                        <p class="mb-0">Faculties of UHST</p>
                    </div>
                </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gy-4">
                @foreach($faculty as $item)
                    <a href="{{ route('faculty', $item->id) }}" class="col" data-cue="zoomIn">
                        <div class="card card-lift">
                            <div class="card-body d-flex flex-column gap-4">
                                <div class="">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" fill="none">
                                        <path opacity="0.2" d="M28 10L16 17L4 10L16 3L28 10Z" fill="#8B3DFF" />
                                        <path d="M28.8638 21.5C28.9958 21.7292 29.0317 22.0014 28.9635 22.257C28.8954 22.5126 28.7287 22.7308 28.5 22.8638L16.5 29.8638C16.3471 29.9529 16.1733 29.9999 15.9963 29.9999C15.8192 29.9999 15.6454 29.9529 15.4925 29.8638L3.4925 22.8638C3.26712 22.7283 3.10415 22.5096 3.03888 22.2549C2.9736 22.0001 3.01128 21.73 3.14375 21.5028C3.27622 21.2757 3.49282 21.1099 3.74666 21.0413C4.0005 20.9727 4.27114 21.0068 4.5 21.1363L16 27.8425L27.5 21.1363C27.7292 21.0042 28.0014 20.9683 28.257 21.0365C28.5126 21.1047 28.7308 21.2713 28.8638 21.5ZM27.5 15.1363L16 21.8425L4.5 15.1363C4.27231 15.0229 4.00997 15.0006 3.76638 15.0738C3.5228 15.147 3.31627 15.3103 3.18884 15.5305C3.06141 15.7506 3.02266 16.011 3.08046 16.2587C3.13827 16.5064 3.28829 16.7228 3.5 16.8638L15.5 23.8638C15.6529 23.9529 15.8267 23.9999 16.0037 23.9999C16.1808 23.9999 16.3546 23.9529 16.5075 23.8638L28.5075 16.8638C28.6228 16.7986 28.7239 16.7111 28.8051 16.6065C28.8863 16.5019 28.9459 16.3822 28.9804 16.2543C29.015 16.1265 29.0238 15.9931 29.0064 15.8618C28.9889 15.7305 28.9456 15.604 28.8789 15.4896C28.8122 15.3752 28.7234 15.2752 28.6177 15.1954C28.5121 15.1156 28.3916 15.0576 28.2633 15.0247C28.135 14.9918 28.0015 14.9848 27.8705 15.0039C27.7394 15.0231 27.6135 15.0681 27.5 15.1363ZM3 10C3.0004 9.82487 3.04679 9.6529 3.13454 9.50131C3.22229 9.34973 3.34831 9.22385 3.5 9.13627L15.5 2.13627C15.6529 2.04711 15.8267 2.00012 16.0037 2.00012C16.1808 2.00012 16.3546 2.04711 16.5075 2.13627L28.5075 9.13627C28.6585 9.22434 28.7837 9.35044 28.8707 9.50199C28.9578 9.65354 29.0036 9.82525 29.0036 10C29.0036 10.1748 28.9578 10.3465 28.8707 10.4981C28.7837 10.6496 28.6585 10.7757 28.5075 10.8638L16.5075 17.8638C16.3546 17.9529 16.1808 17.9999 16.0037 17.9999C15.8267 17.9999 15.6529 17.9529 15.5 17.8638L3.5 10.8638C3.34831 10.7762 3.22229 10.6503 3.13454 10.4987C3.04679 10.3471 3.0004 10.1752 3 10ZM5.985 10L16 15.8425L26.015 10L16 4.15752L5.985 10Z" fill="#8B3DFF" />
                                    </svg>
                                </div>
                                <div class="d-flex flex-column gap-2">
                                    <h3 class="mb-0 fs-4">{{ $item->title_en }}</h3>
                                    <div class="mb-0">{!! Str::limit($item->description, 100, '...') !!}</div>
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach
                @foreach($institute as $item)
                    <a href="{{ route('institute', $item->id) }}" class="col" data-cue="zoomIn">
                        <div class="card card-lift">
                            <div class="card-body d-flex flex-column gap-4">
                                <div class="">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" fill="none">
                                        <path opacity="0.2" d="M28 10L16 17L4 10L16 3L28 10Z" fill="#8B3DFF" />
                                        <path d="M28.8638 21.5C28.9958 21.7292 29.0317 22.0014 28.9635 22.257C28.8954 22.5126 28.7287 22.7308 28.5 22.8638L16.5 29.8638C16.3471 29.9529 16.1733 29.9999 15.9963 29.9999C15.8192 29.9999 15.6454 29.9529 15.4925 29.8638L3.4925 22.8638C3.26712 22.7283 3.10415 22.5096 3.03888 22.2549C2.9736 22.0001 3.01128 21.73 3.14375 21.5028C3.27622 21.2757 3.49282 21.1099 3.74666 21.0413C4.0005 20.9727 4.27114 21.0068 4.5 21.1363L16 27.8425L27.5 21.1363C27.7292 21.0042 28.0014 20.9683 28.257 21.0365C28.5126 21.1047 28.7308 21.2713 28.8638 21.5ZM27.5 15.1363L16 21.8425L4.5 15.1363C4.27231 15.0229 4.00997 15.0006 3.76638 15.0738C3.5228 15.147 3.31627 15.3103 3.18884 15.5305C3.06141 15.7506 3.02266 16.011 3.08046 16.2587C3.13827 16.5064 3.28829 16.7228 3.5 16.8638L15.5 23.8638C15.6529 23.9529 15.8267 23.9999 16.0037 23.9999C16.1808 23.9999 16.3546 23.9529 16.5075 23.8638L28.5075 16.8638C28.6228 16.7986 28.7239 16.7111 28.8051 16.6065C28.8863 16.5019 28.9459 16.3822 28.9804 16.2543C29.015 16.1265 29.0238 15.9931 29.0064 15.8618C28.9889 15.7305 28.9456 15.604 28.8789 15.4896C28.8122 15.3752 28.7234 15.2752 28.6177 15.1954C28.5121 15.1156 28.3916 15.0576 28.2633 15.0247C28.135 14.9918 28.0015 14.9848 27.8705 15.0039C27.7394 15.0231 27.6135 15.0681 27.5 15.1363ZM3 10C3.0004 9.82487 3.04679 9.6529 3.13454 9.50131C3.22229 9.34973 3.34831 9.22385 3.5 9.13627L15.5 2.13627C15.6529 2.04711 15.8267 2.00012 16.0037 2.00012C16.1808 2.00012 16.3546 2.04711 16.5075 2.13627L28.5075 9.13627C28.6585 9.22434 28.7837 9.35044 28.8707 9.50199C28.9578 9.65354 29.0036 9.82525 29.0036 10C29.0036 10.1748 28.9578 10.3465 28.8707 10.4981C28.7837 10.6496 28.6585 10.7757 28.5075 10.8638L16.5075 17.8638C16.3546 17.9529 16.1808 17.9999 16.0037 17.9999C15.8267 17.9999 15.6529 17.9529 15.5 17.8638L3.5 10.8638C3.34831 10.7762 3.22229 10.6503 3.13454 10.4987C3.04679 10.3471 3.0004 10.1752 3 10ZM5.985 10L16 15.8425L26.015 10L16 4.15752L5.985 10Z" fill="#8B3DFF" />
                                    </svg>
                                </div>
                                <div class="d-flex flex-column gap-2">
                                    <h3 class="mb-0 fs-4">{{ $item->title_en }}</h3>
                                    <div class="mb-0">{!! Str::limit($item->description, 100, '...') !!}</div>
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </section>
    <!--faculty & institute end-->

    <!--behind the block-->
    <section class="mb-lg-9 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 xol-12">
                    <div class="text-center mb-7">
                        <h2 class="mt-5 px-lg-10 px-6">
                            Our
                            <span class="text-primary">Leader</span>
                        </h2>
                        <p class="mb-0">Working from all around the world to build the Web of tomorrow.</p>
                    </div>
                </div>
            </div>
            <div class="row gy-5">
                <div class="col-lg-4 col-md-6 col-12">
                    <figure class="mb-4 zoom-img">
                        <img src="../frontend-assets/images/team/team-img-4.jpg" alt="team" class="rounded-3 img-fluid" />
                    </figure>
                    <div class="mb-4">
                        <h4 class="mb-1"><a href="#!" class="text-reset">Pin Vannaro</a></h4>
                        <span class="fs-6">CEO</span>
                    </div>

                    <span>
                        <a href="#!" class="btn btn-telegram btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telegram" viewBox="0 0 16 16">
                              <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M8.287 5.906q-1.168.486-4.666 2.01-.567.225-.595.442c-.03.243.275.339.69.47l.175.055c.408.133.958.288 1.243.294q.39.01.868-.32 3.269-2.206 3.374-2.23c.05-.012.12-.026.166.016s.042.12.037.141c-.03.129-1.227 1.241-1.846 1.817-.193.18-.33.307-.358.336a8 8 0 0 1-.188.186c-.38.366-.664.64.015 1.088.327.216.589.393.85.571.284.194.568.387.936.629q.14.092.27.187c.331.236.63.448.997.414.214-.02.435-.22.547-.82.265-1.417.786-4.486.906-5.751a1.4 1.4 0 0 0-.013-.315.34.34 0 0 0-.114-.217.53.53 0 0 0-.31-.093c-.3.005-.763.166-2.984 1.09"/>
                            </svg>
                        </a>
                    </span>
                    <span>
                        <a href="#!" class="btn btn-facebook btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-facebook" viewBox="0 0 16 16">
                              <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951"/>
                            </svg>
                        </a>
                    </span>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <figure class="mb-4 zoom-img">
                        <img src="../frontend-assets/images/team/team-img-2.jpg" alt="team" class="rounded-3 img-fluid" />
                    </figure>
                    <div class="mb-4">
                        <h4 class="mb-1">
                            <a href="#!" class="text-reset">Pin Vannara</a>
                        </h4>
                        <span class="fs-6">Co-Founder</span>
                    </div>

                    <span>
                        <a href="#!" class="btn btn-telegram btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telegram" viewBox="0 0 16 16">
                              <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M8.287 5.906q-1.168.486-4.666 2.01-.567.225-.595.442c-.03.243.275.339.69.47l.175.055c.408.133.958.288 1.243.294q.39.01.868-.32 3.269-2.206 3.374-2.23c.05-.012.12-.026.166.016s.042.12.037.141c-.03.129-1.227 1.241-1.846 1.817-.193.18-.33.307-.358.336a8 8 0 0 1-.188.186c-.38.366-.664.64.015 1.088.327.216.589.393.85.571.284.194.568.387.936.629q.14.092.27.187c.331.236.63.448.997.414.214-.02.435-.22.547-.82.265-1.417.786-4.486.906-5.751a1.4 1.4 0 0 0-.013-.315.34.34 0 0 0-.114-.217.53.53 0 0 0-.31-.093c-.3.005-.763.166-2.984 1.09"/>
                            </svg>
                        </a>
                    </span>
                    <span>
                        <a href="#!" class="btn btn-facebook btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-facebook" viewBox="0 0 16 16">
                              <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951"/>
                            </svg>
                        </a>
                    </span>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <figure class="mb-4 zoom-img">
                        <img src="../frontend-assets/images/team/team-img-3.jpg"
                             alt="team" class="rounded-3 img-fluid" />
                    </figure>
                    <div class="mb-4">
                        <h4 class="mb-1"><a href="#!" class="text-reset">Chin Yok</a></h4>
                        <span class="fs-6">Creative Director</span>
                    </div>

                    <span>
                        <a href="#!" class="btn btn-telegram btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telegram" viewBox="0 0 16 16">
                              <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M8.287 5.906q-1.168.486-4.666 2.01-.567.225-.595.442c-.03.243.275.339.69.47l.175.055c.408.133.958.288 1.243.294q.39.01.868-.32 3.269-2.206 3.374-2.23c.05-.012.12-.026.166.016s.042.12.037.141c-.03.129-1.227 1.241-1.846 1.817-.193.18-.33.307-.358.336a8 8 0 0 1-.188.186c-.38.366-.664.64.015 1.088.327.216.589.393.85.571.284.194.568.387.936.629q.14.092.27.187c.331.236.63.448.997.414.214-.02.435-.22.547-.82.265-1.417.786-4.486.906-5.751a1.4 1.4 0 0 0-.013-.315.34.34 0 0 0-.114-.217.53.53 0 0 0-.31-.093c-.3.005-.763.166-2.984 1.09"/>
                            </svg>
                        </a>
                    </span>
                    <span>
                        <a href="#!" class="btn btn-facebook btn-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-facebook" viewBox="0 0 16 16">
                              <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951"/>
                            </svg>
                        </a>
                    </span>
                </div>
            </div>
        </div>
    </section>
    <!--behind the block-->

    <!--partner start-->
    <section class="py-5 bg-light-subtle">
        <div class="container" data-cue="fadeIn">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="text-center mb-7 pb-2">
                        <h2 class="mt-5 px-lg-10 px-6">
                            Our
                            <span class="text-primary">Partner</span>
                        </h2>
                        <p class="mb-0">Partner of UHST</p>
                    </div>
                </div>
            </div>
            <div class="row mb-7 pb-2 text-center justify-content-center g-0">
                <div class="col-lg-12 col-12">
                    <div class="marquee" data-cue="slideInLeft">
                        <div class="track">
                            @foreach($partner as $item)
                                <a href="#" class="btn btn-light rounded-pill me-1 mb-3 btn-logo btn-lift">
                                    <span><img src="{{ url('uploads/partner/', $item->image) }}" alt="logo" class="icon-xs" /></span>
                                    <span class="ms-1 d-none d-lg-inline-flex">{{ $item->title_en }}</span>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>{{--
                <div class="col-lg-12 col-12 px-lg-7">
                    <div class="marquee" data-cue="slideInRight">
                        <div class="track-2">
                            <a href="#" class="btn btn-light rounded-pill me-1 mb-3 btn-logo btn-lift">
                                <span><img src="frontend-assets/images/integration-logo/integrate-logo-7.svg" alt="logo" class="icon-xs" /></span>
                                <span class="ms-1 d-none d-lg-inline-flex">MS Teams</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>--}}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <a href="{{ route('partner') }}" class="icon-link icon-link-hover mt-3 mt-md-0">
                                <span>See all partner</span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--partner end-->
@endsection

@section('scripts')
    <script type="text/javascript">
        <!-- Initialize Swiper -->
        var swiper = new Swiper(".mySwiper", {
            slidesPerView: 3,
            spaceBetween: 30,
            freeMode: true,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
    </script>
@endsection
