@extends('layouts.frontend-layout')

@section('title', 'Career')
@section('content')
    <!-- Pageheader start-->
    <div class="pattern-square"></div>
    <section class="bg-light py-5 py-lg-8 bg-opacity-50">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div>
                        <h5 class="mb-0"><a href="{{ route('home') }}">Home</a> / Career</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Pageheader end-->

    <!--career start-->
    <section class="my-xl-5 my-5">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-3 col-md-12" data-cue="fadeIn">
                    <div class="text-center mb-lg-7 mb-5">
                        <h2 class="mt-5 px-lg-10 px-6">
                            Our
                            <span class="text-primary">Career</span>
                        </h2>
                        <p class="mb-0">
                            We are hiring the new job
                            <span class="text-dark">Urgently.</span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="table-responsive-xl pb-5">
                <div class="row">
                    @foreach($career as $item)
                        <div class="col-lg-4 col-md-6">
                            <div class="card border-0 shadow-sm h-100 card-lift" data-cue="fadeIn">
                                <figure>
                                    <img src="{{ url('uploads/career/'.$item->image) }}" alt="event" class="card-img-top">
                                </figure>

                                <div class="card-body h-100 d-flex align-items-start flex-column border rounded-bottom-3 border-top-0">
                                    <div class="d-flex border-bottom pb-3">
                                        <div>
                                            <h4 class="card-title mb-1">{{ $item->position }}</h4>
                                            <small class="mb-0"><i class="bx bx-map"></i> {{ $item->location }}</small>
                                            <br>
                                            <small class="mb-0"><i class="bx bx-building"></i> {{ $item->company }}</small>
                                            <br>
                                            <small class="mb-0"><i class="bx bx-calendar"></i> {{ date('M d, Y', strtotime($item->start_date)) }} - {{ date('M d, Y', strtotime($item->end_date)) }}</small>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="d-flex mt-3 gap-2">
                                            <div>
                                                <p class="small mb-0">Email: {{ $item->email }}</p>
                                                <p class="small mb-0">Tel: {{ $item->tel }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!--career end-->
@endsection
