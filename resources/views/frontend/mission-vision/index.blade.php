@extends('layouts.frontend-layout')

@section('title', 'Mission & Vision')
@section('content')
    <!-- Pageheader start-->
    <div class="pattern-square"></div>
    <section class="bg-light py-5 py-lg-8 bg-opacity-50">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div>
                        <h5 class="mb-0"><a href="{{ route('home') }}">Home</a> / Mission & Vision</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Pageheader end-->

    <!--Portfolio start-->
    <section class="my-xl-5 my-5">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-3 col-md-12" data-cue="fadeIn">
                    <div class="text-center mb-lg-7 mb-5">
                        <h2 class="mt-5 px-lg-10 px-6">
                            Our
                            <span class="text-primary">Mission & Vision</span>
                        </h2>
                        <p class="mb-0">Mission & Vision <span class="text-dark">.</span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="table-responsive-xl pb-5">
                <div class="row flex-nowrap">
                    <div class="col-lg-6 col-md-6">
                        <div class="card card-lift">
                            <div class="card-body pb-0">
                                <div class="mb-6">
                                    <h3 class="h4 mb-4">Mission</h3>
                                    {!! !empty($missionVision[0]->mission) ? $missionVision[0]->mission : '' !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="card card-lift">
                            <div class="card-body pb-0">
                                <div class="mb-6">
                                    <h3 class="h4 mb-4">Vision</h3>
                                    {!! !empty($missionVision[0]->vision) ? $missionVision[0]->vision : '' !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Portfolio end-->
@endsection
