@extends('layouts.backend-layout')

@section('title', 'Edit Profile')
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-pills flex-column flex-md-row mb-4 gap-2 gap-lg-0">
                    <li class="nav-item"><a class="nav-link active" href="javascript:void(0);"><i class="mdi mdi-account-outline mdi-20px me-1"></i>Account</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('admin.change.password') }}"><i class="mdi mdi-lock-open-outline mdi-20px me-1"></i>Security</a></li>
                </ul>
                <form class="card mb-4" action="{{ route('admin.profile.update') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    <h4 class="card-header">Profile Details</h4>
                    <!-- Account -->
                    <div class="card-body">
                        <div class="d-flex align-items-start align-items-sm-center gap-4">
                            <img src="{{ !empty($profileData->photo) ? url('uploads/staff/'.$profileData->photo) : '../../backend-assets/img/avatars/19.jpg' }}" alt="user-avatar" class="d-block w-px-120 h-px-120 rounded" id="uploadedAvatar" />
                            <div class="button-wrapper">
                                <label for="upload" class="btn btn-primary me-2 mb-3" tabindex="0">
                                    <span class="d-none d-sm-block">Upload new photo</span>
                                    <i class="mdi mdi-tray-arrow-up d-block d-sm-none"></i>
                                    <input type="file" id="upload" name="photo" class="account-file-input" hidden accept="image/png, image/jpeg" />
                                </label>

                                <div class="text-muted small">Allowed JPG, GIF or PNG. Max size of 800K</div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-2 mt-1">
                        <div id="formAccountSettings">
                            <div class="row mt-2 gy-4">
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline">
                                        <input
                                            class="form-control @error('first_name_en') is-invalid @enderror"
                                            type="text"
                                            id="first_name_en"
                                            name="first_name_en"
                                            value="{{ $profileData->first_name_en }}"
                                            placeholder="Enter First Name (EN)"
                                            autofocus
                                        />
                                        <label for="first_name_en">First Name (EN)</label>
                                        @error('first_name_en')
                                        <div class="invalid-feedback"> {{ $message }} </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline">
                                        <input
                                            class="form-control"
                                            type="text"
                                            id="middle_name_en"
                                            name="middle_name_en"
                                            value="{{ $profileData->middle_name_en }}"
                                            placeholder="Enter Middle Name (EN)"
                                        />
                                        <label for="middle_name_en">Middle Name (EN)</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline">
                                        <input
                                            class="form-control @error('last_name_en') is-invalid @enderror"
                                            type="text"
                                            name="last_name_en"
                                            id="last_name_en"
                                            value="{{ $profileData->last_name_en }}"
                                            placeholder="Enter Last Name (EN)"
                                        />
                                        <label for="last_name_en">Last Name</label>
                                        @error('last_name_en')
                                        <div class="invalid-feedback"> {{ $message }} </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline">
                                        <input
                                            class="form-control @error('first_name_kh') is-invalid @enderror"
                                            type="text"
                                            id="first_name_kh"
                                            name="first_name_kh"
                                            value="{{ $profileData->first_name_kh }}"
                                            placeholder="Enter First Name (KH)"
                                        />
                                        <label for="first_name_kh">First Name (KH)</label>
                                        @error('first_name_kh')
                                        <div class="invalid-feedback"> {{ $message }} </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline">
                                        <input
                                            class="form-control"
                                            type="text"
                                            id="middle_name_kh"
                                            name="middle_name_kh"
                                            value="{{ $profileData->middle_name_kh }}"
                                            placeholder="Enter Middle Name (KH)"
                                        />
                                        <label for="middle_name_kh">Middle Name (KH)</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline">
                                        <input
                                            class="form-control @error('last_name_kh') is-invalid @enderror"
                                            type="text"
                                            name="last_name_kh"
                                            id="last_name_kh"
                                            value="{{ $profileData->last_name_kh }}"
                                            placeholder="Enter Last Name (KH)"
                                        />
                                        <label for="last_name_kh">Last Name (KH)</label>
                                        @error('last_name_kh')
                                        <div class="invalid-feedback"> {{ $message }} </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline">
                                        <select name="sex" id="sex" class="form-select @error('sex') is-invalid @enderror">
                                            <option value="">Select Gender</option>
                                            <option value="Male" {{ $profileData->sex === 'Male' ? 'selected' : '' }}>Male</option>
                                            <option value="Female" {{ $profileData->sex === 'Female' ? 'selected' : '' }}>Female</option>
                                        </select>
                                        <label for="sex">Gender</label>
                                        @error('last_name_kh')
                                        <div class="invalid-feedback"> {{ $message }} </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline">
                                        <input
                                            class="form-control flatpickr-date @error('dob') is-invalid @enderror"
                                            type="text"
                                            name="dob"
                                            id="dob"
                                            value="{{ date('d-m-Y', strtotime($profileData->dob)) }}"
                                            placeholder="DD-MM-YYYY"
                                        />
                                        <label for="dob">Date of Birth</label>
                                        @error('dob')
                                        <div class="invalid-feedback"> {{ $message }} </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group input-group-merge">
                                        <div class="form-floating form-floating-outline">
                                            <input
                                                type="text"
                                                id="tel"
                                                name="tel"
                                                class="form-control @error('tel') is-invalid @enderror"
                                                value="{{ $profileData->tel }}"
                                                placeholder="Enter Phone Number"
                                            />
                                            <label for="tel">Phone Number</label>
                                            @error('tel')
                                            <div class="invalid-feedback"> {{ $message }} </div>
                                            @enderror
                                        </div>
                                        <span class="input-group-text">KH (+855)</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-floating form-floating-outline">
                                        <input
                                            class="form-control"
                                            type="text"
                                            id="email"
                                            name="email"
                                            value="{{ $profileData->email }}"
                                            placeholder="Enter Email"
                                        />
                                        <label for="email">E-mail</label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-floating form-floating-outline">
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="birth_place"
                                            name="birth_place"
                                            value="{{ $profileData->birth_place }}"
                                            placeholder="Enter Birth Place"
                                        />
                                        <label for="birth_place">Birth Place</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-floating form-floating-outline">
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="present_address"
                                            name="present_address"
                                            value="{{ $profileData->present_address }}"
                                            placeholder="Enter Present Address"
                                        />
                                        <label for="present_address">Present Address</label>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-4">
                                <button type="submit" class="btn btn-primary me-2">Save changes</button>
                                <button type="reset" class="btn btn-outline-secondary">Reset</button>
                            </div>
                        </div>
                    </div>
                    <!-- /Account -->
                </form>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#upload').change(function (e) {
               let reader = new FileReader();
               reader.onload = function (e) {
                   $('#uploadedAvatar').attr('src', e.target.result);
               }
               reader.readAsDataURL(e.target.files[0]);
            });

            $('.flatpickr-date').flatpickr({
                monthSelectorType: "static",
                dateFormat: "d-m-Y"
            });
        });
    </script>
@endsection
