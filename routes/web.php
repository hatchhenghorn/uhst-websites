<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

require __DIR__ . '/auth.php';

/* Backend Routes */
Route::middleware('guest')->group(function () {
    Route::get('/admin/login', [\App\Http\Controllers\Backend\UserController::class, 'login'])->name('admin.login');
});

Route::middleware(['auth', 'roles:admin'])->group(function () {
    //-> User Profile
    Route::controller(\App\Http\Controllers\Backend\ProfileController::class)->group(function () {
        Route::get('/admin/change/profile', 'edit')->name('admin.change.profile');
        Route::post('/admin/profile/update', 'update')->name('admin.profile.update');
        Route::get('/admin/change/password', 'editPassword')->name('admin.change.password');
        Route::post('/admin/update-password', 'updatePassword')->name('admin.update-password');
    });

    //-> User Route
    Route::controller(\App\Http\Controllers\Backend\UserController::class)->group(function () {
        Route::get('/admin/logout', 'destroy')->name('admin.logout');
    });

    //-> Dashboard Route
    Route::controller(\App\Http\Controllers\Backend\DashboardController::class)->group(function () {
        Route::get('/admin/dashboard', 'index')->name('admin.dashboard');
    });

    //-> Staff Type Route
    Route::controller(\App\Http\Controllers\Backend\StaffTypeController::class)->group(function () {
        Route::get('/staff-types', 'index')->name('staff-types')->middleware('permission:Staff-Type.Read');
        Route::get('/staff-types/create', 'create')->name('staff-types.create')->middleware('permission:Staff-Type.Create');
        Route::post('/staff-types/store', 'store')->name('staff-types.store');
        Route::get('/staff-types/edit/{id}', 'edit')->name('staff-types.edit')->middleware('permission:Staff-Type.Edit');
        Route::post('/staff-types/update/{id}', 'update')->name('staff-types.update');
        Route::get('/staff-types/delete/{id}', 'destroy')->name('staff-types.delete')->middleware('permission:Staff-Type.Delete');
    });

    //-> Staff Route
    Route::controller(\App\Http\Controllers\Backend\StaffController::class)->group(function () {
        Route::get('/staff', 'index')->name('staff')->middleware('permission:Staff.Read');
        Route::get('/staff/create', 'create')->name('staff.create')->middleware('permission:Staff.Create');
        Route::post('/staff/store', 'store')->name('staff.store');
        Route::get('/staff/edit/{id}', 'edit')->name('staff.edit')->middleware('permission:Staff.Edit');
        Route::post('/staff/update/{id}', 'update')->name('staff.update');
        Route::get('/staff/delete/{id}', 'destroy')->name('staff.delete')->middleware('permission:Staff.Delete');
    });

    //-> Role Route
    Route::controller(\App\Http\Controllers\Backend\RoleController::class)->group(function () {
        Route::get('/roles', 'index')->name('roles')->middleware('permission:Role.Read');
        Route::get('/roles/create', 'create')->name('roles.create')->middleware('permission:Role.Create');
        Route::post('/roles/store', 'store')->name('roles.store');
        Route::get('/roles/edit/{id}', 'edit')->name('roles.edit')->middleware('permission:Role.Edit');
        Route::post('/roles/update/{id}', 'update')->name('roles.update');
        Route::get('/roles/delete/{id}', 'destroy')->name('roles.delete')->middleware('permission:Role.Delete');
    });

    //-> Permission Route
    Route::controller(\App\Http\Controllers\Backend\PermissionController::class)->group(function () {
        Route::get('/permissions', 'index')->name('permissions')->middleware('permission:Role.Read');
        Route::get('/permissions/create', 'create')->name('permissions.create')->middleware('permission:Role.Create');
        Route::post('/permissions/store', 'store')->name('permissions.store');
        Route::get('/permissions/edit/{id}', 'edit')->name('permissions.edit')->middleware('permission:Role.Edit');
        Route::post('/permissions/update/{id}', 'update')->name('permissions.update');
        Route::get('/permissions/delete/{id}', 'destroy')->name('permissions.delete')->middleware('permission:Role.Delete');
    });

    //-> Role Permission Route
    Route::controller(\App\Http\Controllers\Backend\RolePermissionController::class)->group(function () {
        Route::get('/role-permissions', 'index')->name('role-permissions')->middleware('permission:Role.Read');
        Route::get('/role-permissions/create', 'create')->name('role-permissions.create')->middleware('permission:Role.Create');
        Route::post('/role-permissions/store', 'store')->name('role-permissions.store');
        Route::get('/role-permissions/edit/{id}', 'edit')->name('role-permissions.edit')->middleware('permission:Role.Edit');
        Route::post('/role-permissions/update/{id}', 'update')->name('role-permissions.update');
        Route::get('/role-permissions/delete/{id}', 'destroy')->name('role-permissions.delete')->middleware('permission:Role.Delete');
    });

    //-> History Route
    Route::controller(\App\Http\Controllers\Backend\HistoryController::class)->group(function () {
        Route::get('/histories', 'index')->name('histories')->middleware('permission:History.Read');
        Route::get('/histories/create', 'create')->name('histories.create')->middleware('permission:History.Create');
        Route::post('/histories/store', 'store')->name('histories.store')->middleware('permission:History.Create');
        Route::get('/histories/edit/{id}', 'edit')->name('histories.edit')->middleware('permission:History.Edit');
        Route::post('/histories/update/{id}', 'update')->name('histories.update');
        Route::get('/histories/delete/{id}', 'destroy')->name('histories.delete')->middleware('permission:History.Delete');
    });

    //-> Mission & Vision Route
    Route::controller(\App\Http\Controllers\Backend\MissionVisionController::class)->group(function () {
        Route::get('/mission-visions', 'index')->name('mission-visions')->middleware('permission:Mission-Vision.Read');
        Route::get('/mission-visions/create', 'create')->name('mission-visions.create')->middleware('permission:Mission-Vision.Create');
        Route::post('/mission-visions/store', 'store')->name('mission-visions.store');
        Route::get('/mission-visions/edit/{id}', 'edit')->name('mission-visions.edit')->middleware('permission:Mission-Vision.Edit');
        Route::post('/mission-visions/update/{id}', 'update')->name('mission-visions.update');
        Route::get('/mission-visions/delete/{id}', 'destroy')->name('mission-visions.delete')->middleware('permission:Mission-Vision.Delete');
    });

    //-> President Message Route
    Route::controller(\App\Http\Controllers\Backend\PresidentMessageController::class)->group(function () {
        Route::get('/president-messages', 'index')->name('president-messages')->middleware('permission:PresidentMessage.Read');
        Route::get('/president-messages/create', 'create')->name('president-messages.create')->middleware('permission:PresidentMessage.Create');
        Route::post('/president-messages/store', 'store')->name('president-messages.store');
        Route::get('/president-messages/edit/{id}', 'edit')->name('president-messages.edit')->middleware('permission:PresidentMessage.Edit');
        Route::post('/president-messages/update/{id}', 'update')->name('president-messages.update');
        Route::get('/president-messages/delete/{id}', 'destroy')->name('president-messages.delete')->middleware('permission:PresidentMessage.Delete');
    });

    //-> Partner Route
    Route::controller(\App\Http\Controllers\Backend\PartnerController::class)->group(function () {
        Route::get('/partners', 'index')->name('partners')->middleware('permission:Partner.Read');
        Route::get('/partners/create', 'create')->name('partners.create')->middleware('permission:Partner.Create');
        Route::post('/partners/store', 'store')->name('partners.store');
        Route::get('/partners/edit/{id}', 'edit')->name('partners.edit')->middleware('permission:Partner.Edit');
        Route::post('/partners/update/{id}', 'update')->name('partners.update');
        Route::get('/partners/delete/{id}', 'destroy')->name('partners.delete')->middleware('permission:Partner.Delete');
    });

    //-> Faculty Route
    Route::controller(\App\Http\Controllers\Backend\FacultyController::class)->group(function () {
        Route::get('/faculties', 'index')->name('faculties')->middleware('permission:Faculty.Read');
        Route::get('/faculties/create', 'create')->name('faculties.create')->middleware('permission:Faculty.Create');
        Route::post('/faculties/store', 'store')->name('faculties.store');
        Route::get('/faculties/edit/{id}', 'edit')->name('faculties.edit')->middleware('permission:Faculty.Edit');
        Route::post('/faculties/update/{id}', 'update')->name('faculties.update');
        Route::get('/faculties/delete/{id}', 'destroy')->name('faculties.delete')->middleware('permission:Faculty.Delete');
    });

    //-> Institute Route
    Route::controller(\App\Http\Controllers\Backend\InstituteController::class)->group(function () {
        Route::get('/institutes', 'index')->name('institutes')->middleware('permission:Institute.Read');
        Route::get('/institutes/create', 'create')->name('institutes.create')->middleware('permission:Institute.Create');
        Route::post('/institutes/store', 'store')->name('institutes.store');
        Route::get('/institutes/edit/{id}', 'edit')->name('institutes.edit')->middleware('permission:Institute.Edit');
        Route::post('/institutes/update/{id}', 'update')->name('institutes.update');
        Route::get('/institutes/delete/{id}', 'destroy')->name('institutes.delete')->middleware('permission:Institute.Delete');
    });

    //-> Major Route
    Route::controller(\App\Http\Controllers\Backend\MajorController::class)->group(function () {
        Route::get('/majors', 'index')->name('majors')->middleware('permission:Major.Read');
        Route::get('/majors/create', 'create')->name('majors.create')->middleware('permission:Major.Create');
        Route::post('/majors/store', 'store')->name('majors.store');
        Route::get('/majors/edit/{id}', 'edit')->name('majors.edit')->middleware('permission:Major.Edit');
        Route::post('/majors/update/{id}', 'update')->name('majors.update');
        Route::get('/majors/delete/{id}', 'destroy')->name('majors.delete')->middleware('permission:Major.Delete');
    });

    //-> Course Route
    Route::controller(\App\Http\Controllers\Backend\CourseController::class)->group(function () {
        Route::get('/courses', 'index')->name('courses')->middleware('permission:Course.Read');
        Route::get('/courses/create', 'create')->name('courses.create')->middleware('permission:Course.Create');
        Route::post('/courses/store', 'store')->name('courses.store');
        Route::get('/courses/edit/{id}', 'edit')->name('courses.edit')->middleware('permission:Course.Edit');
        Route::post('/courses/update/{id}', 'update')->name('courses.update');
        Route::get('/courses/delete/{id}', 'destroy')->name('courses.delete')->middleware('permission:Course.Delete');
    });

    //-> Major Course Route
    Route::controller(\App\Http\Controllers\Backend\MajorCourseController::class)->group(function () {
        Route::get('/major-courses', 'index')->name('major-courses')->middleware('permission:Major-Course.Read');
        Route::get('/major-courses/create', 'create')->name('major-courses.create')->middleware('permission:Major-Course.Create');
        Route::post('/major-courses/store', 'store')->name('major-courses.store');
        Route::get('/major-courses/edit/{id}', 'edit')->name('major-courses.edit')->middleware('permission:Major-Course.Edit');
        Route::post('/major-courses/update/{id}', 'update')->name('major-courses.update');
        Route::get('/major-courses/delete/{id}', 'destroy')->name('major-courses.delete')->middleware('permission:Major-Course.Delete');
    });

    //-> Admission Route
    Route::controller(\App\Http\Controllers\Backend\AdmissionController::class)->group(function () {
        Route::get('/admissions', 'index')->name('admissions')->middleware('permission:Admission.Read');
        Route::get('/admissions/create', 'create')->name('admissions.create')->middleware('permission:Admission.Create');
        Route::post('/admissions/store', 'store')->name('admissions.store');
        Route::get('/admissions/edit/{id}', 'edit')->name('admissions.edit')->middleware('permission:Admission.Edit');
        Route::post('/admissions/update/{id}', 'update')->name('admissions.update');
        Route::get('/admissions/delete/{id}', 'destroy')->name('admissions.delete')->middleware('permission:Admission.Delete');
    });

    //-> Project Route
    Route::controller(\App\Http\Controllers\Backend\ProjectController::class)->group(function () {
        Route::get('/projects', 'index')->name('projects')->middleware('permission:Project.Read');
        Route::get('/projects/create', 'create')->name('projects.create')->middleware('permission:Project.Create');
        Route::post('/projects/store', 'store')->name('projects.store');
        Route::get('/projects/edit/{id}', 'edit')->name('projects.edit')->middleware('permission:Project.Edit');
        Route::post('/projects/update/{id}', 'update')->name('projects.update');
        Route::get('/projects/delete/{id}', 'destroy')->name('projects.delete')->middleware('permission:Project.Delete');
        Route::get('/projects/remove/{id}', 'destroyImage')->name('projects.remove');
    });

    //-> Publication Book Route
    Route::controller(\App\Http\Controllers\Backend\PublicationBookController::class)->group(function () {
        Route::get('/publication-books', 'index')->name('publication-books')->middleware('permission:PublicationBook.Read');
        Route::get('/publication-books/create', 'create')->name('publication-books.create')->middleware('permission:PublicationBook.Create');
        Route::post('/publication-books/store', 'store')->name('publication-books.store');
        Route::get('/publication-books/edit/{id}', 'edit')->name('publication-books.edit')->middleware('permission:PublicationBook.Edit');
        Route::post('/publication-books/update/{id}', 'update')->name('publication-books.update');
        Route::get('/publication-books/delete/{id}', 'destroy')->name('publication-books.delete')->middleware('permission:PublicationBook.Delete');
    });

    //-> Training Workshop Route
    Route::controller(\App\Http\Controllers\Backend\TrainingWorkshopController::class)->group(function () {
        Route::get('/training-workshops', 'index')->name('training-workshops')->middleware('permission:Training-Workshop.Read');
        Route::get('/training-workshops/create', 'create')->name('training-workshops.create')->middleware('permission:Training-Workshop.Create');
        Route::post('/training-workshops/store', 'store')->name('training-workshops.store');
        Route::get('/training-workshops/edit/{id}', 'edit')->name('training-workshops.edit')->middleware('permission:Training-Workshop.Edit');
        Route::post('/training-workshops/update/{id}', 'update')->name('training-workshops.update');
        Route::get('/training-workshops/delete/{id}', 'destroy')->name('training-workshops.delete')->middleware('permission:Training-Workshop.Delete');
        Route::get('/training-workshops/remove/{id}', 'destroyImage')->name('training-workshops.remove');
    });

    //-> Conference Route
    Route::controller(\App\Http\Controllers\Backend\ConferenceController::class)->group(function () {
        Route::get('/conferences', 'index')->name('conferences')->middleware('permission:Conference.Read');
        Route::get('/conferences/create', 'create')->name('conferences.create')->middleware('permission:Conference.Create');
        Route::post('/conferences/store', 'store')->name('conferences.store');
        Route::get('/conferences/edit/{id}', 'edit')->name('conferences.edit')->middleware('permission:Conference.Edit');
        Route::post('/conferences/update/{id}', 'update')->name('conferences.update');
        Route::get('/conferences/delete/{id}', 'destroy')->name('conferences.delete')->middleware('permission:Conference.Delete');
        Route::get('/conferences/remove/{id}', 'destroyImage')->name('conferences.remove');
    });

    //-> Contact & Location Route
    Route::controller(\App\Http\Controllers\Backend\ContactLocationController::class)->group(function () {
        Route::get('/contact-locations', 'index')->name('contact-locations')->middleware('permission:Contact-Location.Read');
        Route::get('/contact-locations/create', 'create')->name('contact-locations.create')->middleware('permission:Contact-Location.Create');
        Route::post('/contact-locations/store', 'store')->name('contact-locations.store');
        Route::get('/contact-locations/edit/{id}', 'edit')->name('contact-locations.edit')->middleware('permission:Contact-Location.Edit');
        Route::post('/contact-locations/update/{id}', 'update')->name('contact-locations.update');
        Route::get('/contact-locations/delete/{id}', 'destroy')->name('contact-locations.delete')->middleware('permission:Contact-Location.Delete');
    });

    //-> News & Activities Route
    Route::controller(\App\Http\Controllers\Backend\NewsActivitiesController::class)->group(function () {
        Route::get('/news-activities', 'index')->name('news-activities')->middleware('permission:News-Activities.Read');
        Route::get('/news-activities/create', 'create')->name('news-activities.create')->middleware('permission:News-Activities.Create');
        Route::post('/news-activities/store', 'store')->name('news-activities.store');
        Route::get('/news-activities/edit/{id}', 'edit')->name('news-activities.edit')->middleware('permission:News-Activities.Edit');
        Route::post('/news-activities/update/{id}', 'update')->name('news-activities.update');
        Route::get('/news-activities/delete/{id}', 'destroy')->name('news-activities.delete')->middleware('permission:News-Activities.Delete');
    });

    //-> Career Route
    Route::controller(\App\Http\Controllers\Backend\CareerController::class)->group(function () {
        Route::get('/careers', 'index')->name('careers')->middleware('permission:Career.Read');
        Route::get('/careers/create', 'create')->name('careers.create')->middleware('permission:Career.Create');
        Route::post('/careers/store', 'store')->name('careers.store');
        Route::get('/careers/edit/{id}', 'edit')->name('careers.edit')->middleware('permission:Career.Edit');
        Route::post('/careers/update/{id}', 'update')->name('careers.update');
        Route::get('/careers/delete/{id}', 'destroy')->name('careers.delete')->middleware('permission:Career.Delete');
    });
});

/* Frontend Routes */
//-> Home Route
Route::controller(\App\Http\Controllers\Frontend\HomeController::class)->group(function () {
    Route::get('/', 'index');
    Route::get('/home', 'index')->name('home');
});

//-> History
Route::get('/history', [\App\Http\Controllers\Frontend\HistoryController::class, 'index'])->name('history');

//-> Mission & Vision
Route::get('/mission-vision', [\App\Http\Controllers\Frontend\MissionVisionController::class, 'index'])->name('mission-vision');

//-> President Message
Route::get('/president-message', [\App\Http\Controllers\Frontend\PresidentMessageController::class, 'index'])->name('president-message');

//-> Partner
Route::get('/partner', [\App\Http\Controllers\Frontend\PartnerController::class, 'index'])->name('partner');

//-> Faculty
Route::get('/faculty/{id}', [\App\Http\Controllers\Frontend\FacultyController::class, 'index'])->name('faculty');

//-> Institute
Route::get('/institute/{id}', [\App\Http\Controllers\Frontend\InstituteController::class, 'index'])->name('institute');

//-> Major Course
Route::get('/major/course/{id}', [\App\Http\Controllers\Frontend\MajorCourseController::class, 'index'])->name('major-course');

//-> Admission
Route::get('/admission', [\App\Http\Controllers\Frontend\AdmissionController::class, 'index'])->name('admission');

//-> Project
Route::get('/project', [\App\Http\Controllers\Frontend\ProjectController::class, 'index'])->name('project');

//-> Publication Book
Route::get('/publication-book', [\App\Http\Controllers\Frontend\PublicationBookController::class, 'index'])->name('publication-book');

//-> Training & Workshop
Route::get('/training-workshop', [\App\Http\Controllers\Frontend\TrainingWorkshopController::class, 'index'])->name('training-workshop');

//-> Conference
Route::get('/conference', [\App\Http\Controllers\Frontend\ConferenceController::class, 'index'])->name('conference');

//-> Contact & Location Route
Route::get('/contact-location', [\App\Http\Controllers\Frontend\ContactLocationController::class, 'index'])->name('contact-location');

//-> News & Activities Route
Route::get('/news-activity', [\App\Http\Controllers\Frontend\NewsActivitiesController::class, 'index'])->name('news-activity');

//-> Career Route
Route::get('/career', [\App\Http\Controllers\Frontend\CareerController::class, 'index'])->name('career');


