<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Faculty extends Model
{
    use HasFactory;
    protected $guarded = [];

    public static function getFaculty()
    {
        return DB::table('faculties')
            ->select('id', 'title_en')
            ->get();
    }
}
