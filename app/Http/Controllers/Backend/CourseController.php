<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $courses = Course::latest()->paginate(10);

        return view('backend.course.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.course.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'description' => 'required',
        ]);

        $course = new Course();

        $course->title_en = $request->title_en;
        $course->title_kh = $request->title_kh;
        $course->description = $request->description;
        $course->activation = $request->has('activation') ? $request->activation : '0';

        if ($request->file('image')) {
            $file = $request->file('image');
            $fileName = 'course-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/course/'), $fileName);
            $course->image = $fileName;
        }

        $course->save();

        $notification = array(
            'message' => 'Course created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('courses')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $course = Course::findOrFail($id);

        return view('backend.course.edit', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'description' => 'required',
        ]);

        $course = Course::findOrFail($id);

        $course->title_en = $request->title_en;
        $course->title_kh = $request->title_kh;
        $course->description = $request->description;
        $course->activation = $request->has('activation') ? $request->activation : '0';

        if ($request->file('image')) {
            $file = $request->file('image');
            @unlink(public_path('uploads/course/'. $course->image));
            $fileName = 'course-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/course/'), $fileName);
            $course->image = $fileName;
        }

        $course->save();

        $notification = array(
            'message' => 'Course updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('courses')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $course = Course::findOrFail($id);

        $imagePath = public_path('uploads/course/'. $course->image);

        if (file_exists($imagePath)) {
            unlink($imagePath);
        }

        $course->delete();

        $notification = array(
            'message' => 'Course deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
