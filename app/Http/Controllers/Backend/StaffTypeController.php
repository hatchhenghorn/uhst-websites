<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\StaffType;
use Illuminate\Http\Request;

class StaffTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $staffTypes = StaffType::latest()->paginate(10);

        return view('backend.staff-type.index', compact('staffTypes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.staff-type.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'type_en' => 'required|string|max:255',
            'type_kh' => 'required|string|max:255'
        ]);

        $staffType = new StaffType();

        $staffType->type_en = $request->type_en;
        $staffType->type_kh = $request->type_kh;
        $staffType->activation = $request->has('activation') ? $request->activation : '0';

        $staffType->save();

        $notification = array(
            'message' => 'Data created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('staff-types')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $staffType = StaffType::findOrFail($id);

        return view('backend.staff-type.edit', compact('staffType'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'type_en' => 'required|string|max:255',
            'type_kh' => 'required|string|max:255'
        ]);

        $staffType = StaffType::findOrFail($id);

        $staffType->type_en = $request->type_en;
        $staffType->type_kh = $request->type_kh;
        $staffType->activation = $request->has('activation') ? $request->activation : '0';

        $staffType->save();

        $notification = array(
            'message' => 'Data updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('staff-types')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $staffType = StaffType::findOrFail($id);

        $staffType->delete();

        $notification = array(
            'message' => 'Data deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
