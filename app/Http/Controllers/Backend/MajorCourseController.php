<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Major;
use App\Models\MajorCourse;
use Illuminate\Http\Request;

class MajorCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $majorCourse = MajorCourse::latest()->paginate(10);

        return view('backend.major-course.index', compact('majorCourse'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $majors = Major::where('activation', 1)->get();
        $courses = Course::where('activation', 1)->get();

        return view('backend.major-course.create', compact('majors', 'courses'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'year' => 'required|integer',
            'semester' => 'required|integer',
            'credit' => 'required|integer',
            'major_id' => 'required|integer',
            'course_id' => 'required|integer',
            'description' => 'required'
        ]);

        $majorCourse = new MajorCourse();

        $majorCourse->title = $request->title;
        $majorCourse->year = $request->year;
        $majorCourse->semester = $request->semester;
        $majorCourse->credit = $request->credit;
        $majorCourse->major_id = $request->major_id;
        $majorCourse->course_id = $request->course_id;
        $majorCourse->description = $request->description;
        $majorCourse->activation = $request->has('activation') ? $request->activation : '0';

        $majorCourse->save();

        $notification = array(
            'message' => 'Major & Course created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('major-courses')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $majorCourse = MajorCourse::findOrFail($id);
        $majors = Major::where('activation', 1)->get();
        $courses = Course::where('activation', 1)->get();

        return view('backend.major-course.edit', compact('majorCourse', 'majors', 'courses'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'year' => 'required|integer',
            'semester' => 'required|integer',
            'credit' => 'required|integer',
            'major_id' => 'required|integer',
            'course_id' => 'required|integer',
            'description' => 'required'
        ]);

        $majorCourse = MajorCourse::findOrFail($id);

        $majorCourse->title = $request->title;
        $majorCourse->year = $request->year;
        $majorCourse->semester = $request->semester;
        $majorCourse->credit = $request->credit;
        $majorCourse->major_id = $request->major_id;
        $majorCourse->course_id = $request->course_id;
        $majorCourse->description = $request->description;
        $majorCourse->activation = $request->has('activation') ? $request->activation : '0';

        $majorCourse->save();

        $notification = array(
            'message' => 'Major & Course updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('major-courses')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $majorCourse = MajorCourse::findOrFail($id);

        $majorCourse->delete();

        $notification = array(
            'message' => 'Major & Course deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
