<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ResearchInnovationImage;
use App\Models\TrainingWorkshop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\Diff\Exception;

class TrainingWorkshopController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $training_workshops = TrainingWorkshop::latest()->paginate(10);

        return view('backend.training-workshop.index', compact('training_workshops'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.training-workshop.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // Wrap database operations within a transaction
        DB::beginTransaction();

        try {
            $request->validate([
                'title_en' => 'required|string|max:255',
                'title_kh' => 'required|string|max:255',
                'sub_title_en' => 'required|string|max:255',
                'sub_title_kh' => 'required|string|max:255',
                'start_date' => 'required',
                'end_date' => 'required',
                'description' => 'required',
            ]);

            // Create the training workshop
            $training_workshop = new TrainingWorkshop();

            $training_workshop->title_en = $request->title_en;
            $training_workshop->title_kh = $request->title_kh;
            $training_workshop->sub_title_en = $request->sub_title_en;
            $training_workshop->sub_title_kh = $request->sub_title_kh;
            $training_workshop->start_date = date('Y-m-d', strtotime($request->start_date));
            $training_workshop->end_date = date('Y-m-d', strtotime($request->end_date));
            $training_workshop->description = $request->description;
            $training_workshop->activation = $request->has('activation') ? $request->activation : '0';

            if ($request->file('image')) {
                $file = $request->file('image');
                $filename = 'training-workshop-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
                $file->move(public_path('uploads/training-workshop/'), $filename);
                $training_workshop->image = $filename;
            }

            $training_workshop->save();

            // Handle uploaded images
            if ($request->file('sub_image')) {
                foreach ($request->file('sub_image') as $key => $image) {
                    $filename = 'training-workshop-img-'.date('YmdHi').$key.'.'.$image->getClientOriginalExtension();
                    $image->move(public_path('uploads/training-workshop/sub-images/'), $filename);

                    // Create the research & innovation image
                    $research_innovation_image = new ResearchInnovationImage();
                    $research_innovation_image->title = $request->title_en;
                    $research_innovation_image->description = $request->description;
                    $research_innovation_image->training_workshop_id = $training_workshop->id;
                    $research_innovation_image->image = $filename;

                    $research_innovation_image->save();
                }
            }

            // Commit the transaction if all operations succeed
            DB::commit();

            $notification = array(
                'message' => 'Data created successfully.',
                'alert-type' => 'success'
            );

            return redirect()->route('training-workshops')->with($notification);
        } catch (Exception $exception) {
            // Rollback the transaction if an error occurs
            DB::rollBack();

            $notification = array(
                'message' => 'Data create failed.'.$exception->getMessage(),
                'alert-type' => 'error'
            );

            return redirect()->back()->with($notification);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $training_workshop = TrainingWorkshop::findOrFail($id);

        $training_workshop_image = ResearchInnovationImage::where('training_workshop_id', $id)->get();

        return view('backend.training-workshop.edit', compact('training_workshop', 'training_workshop_image'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        // Wrap database operations within a transaction
        DB::beginTransaction();

        try {
            $request->validate([
                'title_en' => 'required|string|max:255',
                'title_kh' => 'required|string|max:255',
                'sub_title_en' => 'required|string|max:255',
                'sub_title_kh' => 'required|string|max:255',
                'start_date' => 'required',
                'end_date' => 'required',
                'description' => 'required',
            ]);

            // Create the training workshop
            $training_workshop = TrainingWorkshop::findOrFail($id);

            $training_workshop->title_en = $request->title_en;
            $training_workshop->title_kh = $request->title_kh;
            $training_workshop->sub_title_en = $request->sub_title_en;
            $training_workshop->sub_title_kh = $request->sub_title_kh;
            $training_workshop->start_date = date('Y-m-d', strtotime($request->start_date));
            $training_workshop->end_date = date('Y-m-d', strtotime($request->end_date));
            $training_workshop->description = $request->description;
            $training_workshop->activation = $request->has('activation') ? $request->activation : '0';

            if ($request->file('image')) {
                $file = $request->file('image');
                unlink(public_path('uploads/training-workshop/'.$training_workshop->image));
                $filename = 'training-workshop-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
                $file->move(public_path('uploads/training-workshop/'), $filename);
                $training_workshop->image = $filename;
            }

            $training_workshop->save();

            // Handle uploaded images
            if ($request->file('sub_image')) {
                foreach ($request->file('sub_image') as $key => $image) {
                    $filename = 'training-workshop-img-'.date('YmdHi').$key.'.'.$image->getClientOriginalExtension();
                    $image->move(public_path('uploads/training-workshop/sub-images/'), $filename);

                    // Create the research & innovation image
                    $research_innovation_image = new ResearchInnovationImage();
                    $research_innovation_image->title = $request->title_en;
                    $research_innovation_image->description = $request->description;
                    $research_innovation_image->training_workshop_id = $training_workshop->id;
                    $research_innovation_image->image = $filename;

                    $research_innovation_image->save();
                }
            }

            // Commit the transaction if all operations succeed
            DB::commit();

            $notification = array(
                'message' => 'Data updated successfully.',
                'alert-type' => 'success'
            );

            return redirect()->route('training-workshops')->with($notification);
        } catch (Exception $exception) {
            // Rollback the transaction if an error occurs
            DB::rollBack();

            $notification = array(
                'message' => 'Data update failed.'.$exception->getMessage(),
                'alert-type' => 'error'
            );

            return redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $training_workshop = TrainingWorkshop::findOrFail($id);

        $image_path = public_path('uploads/training-workshop/'.$training_workshop->image);

        if (file_exists($image_path)) {
            unlink($image_path);
        }

        $research_innovation_image = ResearchInnovationImage::where('training_workshop_id', $id)->get();

        foreach ($research_innovation_image as $image) {
            $image_path2 = public_path('uploads/training-workshop/sub-images/'.$image->image);

            if (file_exists($image_path2)) {
                unlink($image_path2);
            }

            $image->delete();
        }

        $training_workshop->delete();

        $notification = array(
            'message' => 'Data deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroyImage(string $id)
    {
        $research_innovation_image = ResearchInnovationImage::findOrFail($id);

        $image_path = public_path('uploads/training-workshop/sub-images/'.$research_innovation_image->image);

        if (file_exists($image_path)) {
            unlink($image_path);
        }

        $research_innovation_image->delete();

        $notification = array(
            'message' => 'Image removed successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
