<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\NewsActivities;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NewsActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $news_activities = NewsActivities::latest()->paginate(10);

        return view('backend.news-activities.index', compact('news_activities'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.news-activities.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'sub_title_en' => 'required|string|max:255',
            'sub_title_kh' => 'required|string|max:255',
            'date_time' => 'required',
            'description' => 'required',
        ]);

        $news_activity = new NewsActivities();

        $news_activity->title_en = $request->title_en;
        $news_activity->title_kh = $request->title_kh;
        $news_activity->sub_title_en = $request->sub_title_en;
        $news_activity->sub_title_kh = $request->sub_title_kh;
        $news_activity->date_time = Carbon::createFromFormat('d-m-Y H:i', $request->date_time)->format('Y-m-d H:i');
        $news_activity->description = $request->description;
        $news_activity->activation = $request->has('activation') ? $request->activation : '0';

        if ($request->file('image')) {
            $file = $request->file('image');
            $filename = 'news-activities-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/news-activities/'), $filename);
            $news_activity->image = $filename;
        }

        $news_activity->save();

        $notification = array(
            'message' => 'News & Activities created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('news-activities')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $news_activity = NewsActivities::findOrFail($id);

        return view('backend.news-activities.edit', compact('news_activity'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'sub_title_en' => 'required|string|max:255',
            'sub_title_kh' => 'required|string|max:255',
            'date_time' => 'required',
            'description' => 'required',
        ]);

        $news_activity = NewsActivities::findOrFail($id);

        $news_activity->title_en = $request->title_en;
        $news_activity->title_kh = $request->title_kh;
        $news_activity->sub_title_en = $request->sub_title_en;
        $news_activity->sub_title_kh = $request->sub_title_kh;
        $news_activity->date_time = Carbon::createFromFormat('d-m-Y H:i', $request->date_time)->format('Y-m-d H:i');
        $news_activity->description = $request->description;
        $news_activity->activation = $request->has('activation') ? $request->activation : '0';

        if ($request->file('image')) {
            $file = $request->file('image');
            @unlink(public_path('uploads/news-activities/'. $news_activity->image));
            $filename = 'news-activities-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/news-activities/'), $filename);
            $news_activity->image = $filename;
        }

        $news_activity->save();

        $notification = array(
            'message' => 'News & Activities updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('news-activities')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $news_activity = NewsActivities::findOrFail($id);

        $imagePath = public_path('uploads/news-activities/'. $news_activity->image);

        if (file_exists($imagePath)) {
            unlink($imagePath);
        }

        $news_activity->delete();

        $notification = array(
            'message' => 'News & Activities deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
