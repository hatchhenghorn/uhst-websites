<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $partners = Partner::latest()->paginate(10);

        return view('backend.partner.index', compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.partner.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'description' => 'required',
        ]);

        $partner = new Partner();

        $partner->title_en = $request->title_en;
        $partner->title_kh = $request->title_kh;
        $partner->description = $request->description;
        $partner->activation = $request->has('activation') ? $request->activation : '0';

        if ($request->file('image')) {
            $file = $request->file('image');
            $file_name = 'partner-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/partner/'), $file_name);
            $partner->image = $file_name;
        }

        $partner->save();

        $notification = array(
            'message' => 'Data created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('partners')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $partner = Partner::findOrFail($id);

        return view('backend.partner.edit', compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'description' => 'required',
        ]);

        $partner = Partner::findOrFail($id);

        $partner->title_en = $request->title_en;
        $partner->title_kh = $request->title_kh;
        $partner->description = $request->description;
        $partner->activation = $request->has('activation') ? $request->activation : '0';

        if ($request->file('image')) {
            $file = $request->file('image');
            @unlink(public_path('uploads/partner/'.$partner->image));
            $file_name = 'partner-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/partner/'), $file_name);
            $partner->image = $file_name;
        }

        $partner->save();

        $notification = array(
            'message' => 'Data updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('partners')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $partner = Partner::findOrFail($id);

        $image_path = public_path('uploads/partner/'.$partner->image);

        if (file_exists($image_path)) {
            unlink($image_path);
        }

        $partner->delete();

        $notification = array(
            'message' => 'Data deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
