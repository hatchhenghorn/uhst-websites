<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Institute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InstituteController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $institutes = Institute::latest()->with('faculty')->paginate(10);

        return view('backend.institute.index', compact('institutes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $faculty = DB::table('faculties')
            ->select('id', 'title_en')
            ->where('activation', '=', '1')
            ->orderBy('title_en')
            ->get();

        return view('backend.institute.create', compact('faculty'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'faculty_id' => 'required|integer',
            'description' => 'required'
        ]);

        $institute = new Institute();

        $institute->title_en = $request->title_en;
        $institute->title_kh = $request->title_kh;
        $institute->faculty_id = $request->faculty_id;
        $institute->description = $request->description;
        $institute->activation = $request->has('activation') ? $request->activation : '0';

        $institute->save();

        $notification = array(
            'message' => 'Data created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('institutes')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $institute = Institute::findOrFail($id);

        $faculty = DB::table('faculties')
            ->select('id', 'title_en')
            ->where('activation', '=', '1')
            ->orderBy('title_en')
            ->get();

        return view('backend.institute.edit', compact('institute', 'faculty'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'faculty_id' => 'required|integer',
            'description' => 'required'
        ]);

        $institute = Institute::findOrFail($id);

        $institute->title_en = $request->title_en;
        $institute->title_kh = $request->title_kh;
        $institute->faculty_id = $request->faculty_id;
        $institute->description = $request->description;
        $institute->activation = $request->has('activation') ? $request->activation : '0';

        $institute->save();

        $notification = array(
            'message' => 'Data updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('institutes')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $institute = Institute::findOrFail($id);

        $institute->delete();

        $notification = array(
            'message' => 'Data deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
