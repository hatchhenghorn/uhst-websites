<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Institute;
use App\Models\Major;
use Illuminate\Http\Request;

class MajorController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $majors = Major::latest()->paginate(10);

        return view('backend.major.index', compact('majors'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $institutes = Institute::where('activation', 1)->get();

        return view('backend.major.create', compact('institutes'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'institute_id' => 'required|integer',
            'description' => 'required',
        ]);

        $major = new Major();

        $major->title_en = $request->title_en;
        $major->title_kh = $request->title_kh;
        $major->institute_id = $request->institute_id;
        $major->description = $request->description;
        $major->activation = $request->has('activation') ? $request->activation : '0';

        if ($request->file('image')) {
            $file = $request->file('image');
            $fileName = 'major-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/major/'), $fileName);
            $major->image = $fileName;
        }

        $major->save();

        $notification = array(
            'message' => 'Major created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('majors')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $major = Major::findOrFail($id);
        $institutes = Institute::where('activation', 1)->get();

        return view('backend.major.edit', compact('major', 'institutes'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'institute_id' => 'required|integer',
            'description' => 'required',
        ]);

        $major = Major::findOrFail($id);

        $major->title_en = $request->title_en;
        $major->title_kh = $request->title_kh;
        $major->institute_id = $request->institute_id;
        $major->description = $request->description;
        $major->activation = $request->has('activation') ? $request->activation : '0';

        if ($request->file('image')) {
            $file = $request->file('image');
            @unlink(public_path('uploads/major/'.$major->image));
            $fileName = 'major-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/major/'), $fileName);
            $major->image = $fileName;
        }

        $major->save();

        $notification = array(
            'message' => 'Major updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('majors')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $major = Major::findOrFail($id);

        $imagePath = public_path('uploads/major/'.$major->image);

        if (file_exists($imagePath)) {
            unlink($imagePath);
        }

        $major->delete();

        $notification = array(
            'message' => 'Major deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
