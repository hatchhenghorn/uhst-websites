<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolePermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $rolePermissions = Role::latest()->paginate(10);

        return view('backend.role-permission.index', compact('rolePermissions'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $roles = Role::all();
        $permissions = Permission::all();
        $permissionGroups = User::getPermissionGroup();

        return view('backend.role-permission.create', compact('roles', 'permissions', 'permissionGroups'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'role_id' => 'required|numeric'
        ], [
            'role_id.required' => 'The role field is required.',
            'role_id.numeric' => 'The role ID must be a numeric value.',
        ]);

        $data = array();
        $permissions = $request->permission;

        foreach ($permissions as $item) {
            $data['role_id'] = $request->role_id;
            $data['permission_id'] = $item;

            DB::table('role_has_permissions')->insert($data);
        }

        $notification = array(
            'message' => 'Role Permission created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('role-permissions')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $role = Role::findOrFail($id);
        $permissions = Permission::all();
        $permissionGroups = User::getPermissionGroup();

        return view('backend.role-permission.edit', compact('role', 'permissions', 'permissionGroups'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $role = Role::findOrFail($id);
        /*$permissions = $request->permission;

        if (!empty($permissions)) {
            $role->syncPermissions($permissions);
        }*/

        $permissions = $request->input('permission');

        $permissions = array_map(function ($item) {
            return (int)$item;
        }, $permissions);

        if (!empty($permissions)){
            $role->syncPermissions($permissions);
        }

        $notification = array(
            'message' => 'Role Permission updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('role-permissions')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $role = Role::findOrFail($id);
        if (!is_null($role)) {
            $role->delete();
        }

        $notification = array(
            'message' => 'Role Permission deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
