<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Career;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CareerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $careers = Career::latest()->paginate(10);

        return view('backend.career.index', compact('careers'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.career.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'position' => 'required|string|max:255',
            'company' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'tel' => 'required|string|max:255',
            'location' => 'required|string|max:255',
            'number_of_require' => 'required|numeric',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'description' => 'required',
        ]);

        $career = new Career();

        $career->title_en = $request->title_en;
        $career->title_kh = $request->title_kh;
        $career->position = $request->position;
        $career->company = $request->company;
        $career->website = $request->website;
        $career->email = $request->email;
        $career->tel = $request->tel;
        $career->location = $request->location;
        $career->number_of_require = $request->number_of_require;
        $career->start_date = Carbon::createFromFormat('d-m-Y', $request->start_date)->format('Y-m-d');
        $career->end_date = Carbon::createFromFormat('d-m-Y', $request->end_date)->format('Y-m-d');
        $career->description = $request->description;
        $career->activation = $request->has('activation') ? $request->activation : '0';

        if ($request->file('image')) {
            $file = $request->file('image');
            $filename = 'career-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/career/'), $filename);
            $career->image = $filename;
        }

        $career->save();

        $notification = array(
            'message' => 'Career created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('careers')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $career = Career::findOrFail($id);

//        dd($career);

        return view('backend.career.edit', compact('career'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'position' => 'required|string|max:255',
            'company' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'tel' => 'required|string|max:255',
            'location' => 'required|string|max:255',
            'number_of_require' => 'required|numeric',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'description' => 'required',
        ]);

        $career = Career::findOrFail($id);

        $career->title_en = $request->title_en;
        $career->title_kh = $request->title_kh;
        $career->position = $request->position;
        $career->company = $request->company;
        $career->website = $request->website;
        $career->email = $request->email;
        $career->tel = $request->tel;
        $career->location = $request->location;
        $career->number_of_require = $request->number_of_require;
        $career->start_date = Carbon::createFromFormat('d-m-Y', $request->start_date)->format('Y-m-d');
        $career->end_date = Carbon::createFromFormat('d-m-Y', $request->end_date)->format('Y-m-d');
        $career->description = $request->description;
        $career->activation = $request->has('activation') ? $request->activation : '0';

        if ($request->file('image')) {
            $file = $request->file('image');
            @unlink(public_path('uploads/career/'.$career->image));
            $filename = 'career-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/career/'), $filename);
            $career->image = $filename;
        }

        $career->save();

        $notification = array(
            'message' => 'Career updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('careers')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $career = Career::findOrFail($id);

        $image_path = public_path('uploads/career/'.$career->image);

        if (file_exists($image_path)) {
            unlink($image_path);
        }

        $career->delete();

        $notification = array(
            'message' => 'Career deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
