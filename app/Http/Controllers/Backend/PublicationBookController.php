<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\PublicationBook;
use Illuminate\Http\Request;

class PublicationBookController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $publication_books = PublicationBook::latest()->paginate(10);

        return view('backend.publication-book.index', compact('publication_books'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.publication-book.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'url' => 'required|url',
            'author' => 'required|string|max:255',
            'date_published' => 'required|date',
            'doi' => 'required|string|max:255',
            'description' => 'required',
        ]);

        $publication_book = new PublicationBook();
        $publication_book->title_en = $request->title_en;
        $publication_book->title_kh = $request->title_kh;
        $publication_book->url = $request->url;
        $publication_book->author = $request->author;
        $publication_book->date_published = date('Y-m-d', strtotime($request->date_published));
        $publication_book->doi = $request->doi;
        $publication_book->description = $request->description;
        $publication_book->activation = $request->has('activation') ? $request->activation : '0';

        if ($request->file('image')) {
            $file = $request->file('image');
            $file_name = 'publication-book-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/publication-book/'),$file_name);
            $publication_book->image = $file_name;
        }

        $publication_book->save();

        $notification = array(
            'message' => 'Data created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('publication-books')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $publication_book = PublicationBook::findOrFail($id);

        return view('backend.publication-book.edit', compact('publication_book'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'url' => 'required|url',
            'author' => 'required|string|max:255',
            'date_published' => 'required|date',
            'doi' => 'required|string|max:255',
            'description' => 'required',
        ]);

        $publication_book = PublicationBook::findOrFail($id);

        $publication_book->title_en = $request->title_en;
        $publication_book->title_kh = $request->title_kh;
        $publication_book->url = $request->url;
        $publication_book->author = $request->author;
        $publication_book->date_published = date('Y-m-d', strtotime($request->date_published));
        $publication_book->doi = $request->doi;
        $publication_book->description = $request->description;
        $publication_book->activation = $request->has('activation') ? $request->activation : '0';

        if ($request->file('image')) {
            $file = $request->file('image');
            @unlink(public_path('uploads/publication-book/'.$publication_book->image));
            $file_name = 'publication-book-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/publication-book/'),$file_name);
            $publication_book->image = $file_name;
        }

        $publication_book->save();

        $notification = array(
            'message' => 'Data updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('publication-books')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $publication_book = PublicationBook::findOrFail($id);

        $image_path = public_path('uploads/publication-book/'.$publication_book->image);

        if (file_exists($image_path)) {
            unlink($image_path);
        }

        $publication_book->delete();

        $notification = array(
            'message' => 'Data deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
