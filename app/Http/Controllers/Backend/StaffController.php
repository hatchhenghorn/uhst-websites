<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\StaffType;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $staffs = User::latest()->paginate(10);

        return view('backend.staff.index', compact('staffs'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $staffTypes = StaffType::latest()->get();
        $roles = Role::latest()->get();

        return view('backend.staff.create', compact('staffTypes', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name_en' => 'required|string|max:255',
            'first_name_kh' => 'required|string|max:255',
            'last_name_en' => 'required|string|max:255',
            'last_name_kh' => 'required|string|max:255',
            'sex' => 'required|string',
            'dob' => 'required|date',
            'tel' => 'required|numeric',
            'role' => 'required',
            'password' => 'required|string|max:255',
        ]);

        $staff = new User();

        $staff->first_name_en = $request->first_name_en;
        $staff->middle_name_en = $request->middle_name_en;
        $staff->last_name_en = $request->last_name_en;
        $staff->first_name_kh = $request->first_name_kh;
        $staff->middle_name_kh = $request->middle_name_kh;
        $staff->last_name_kh = $request->last_name_kh;
        $staff->sex = $request->sex;
        $staff->dob = date('Y-m-d', strtotime($request->dob));
        $staff->tel = $request->tel;
        $staff->email = $request->email;
        $staff->birth_place = $request->birth_place;
        $staff->present_address = $request->present_address;
        $staff->role = 'admin';
        $staff->password = Hash::make($request->password);
        $staff->activation = $request->has('activation') ? $request->activation : '0';

        if ($request->file('photo')) {
            $file = $request->file('photo');
            $filename = 'staff-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/staff/'), $filename);
            $staff->photo = $filename;
        }

        $staff->save();

        if ($request->role) {
            $staff->assignRole($request->role);
        }

        $notification = array(
            'message' => 'Staff created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('staff')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $staff = User::findOrFail($id);
        $staffTypes = StaffType::latest()->get();
        $roles = Role::latest()->get();

        return view('backend.staff.edit', compact('staff', 'staffTypes', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'first_name_en' => 'required|string|max:255',
            'first_name_kh' => 'required|string|max:255',
            'last_name_en' => 'required|string|max:255',
            'last_name_kh' => 'required|string|max:255',
            'sex' => 'required|string',
            'dob' => 'required|date',
            'tel' => 'required|numeric',
            'role' => 'required',
        ]);

        $staff = User::findOrFail($id);

        $staff->first_name_en = $request->first_name_en;
        $staff->middle_name_en = $request->middle_name_en;
        $staff->last_name_en = $request->last_name_en;
        $staff->first_name_kh = $request->first_name_kh;
        $staff->middle_name_kh = $request->middle_name_kh;
        $staff->last_name_kh = $request->last_name_kh;
        $staff->sex = $request->sex;
        $staff->dob = date('Y-m-d', strtotime($request->dob));
        $staff->tel = $request->tel;
        $staff->email = $request->email;
        $staff->birth_place = $request->birth_place;
        $staff->present_address = $request->present_address;
        $staff->role = 'admin';
        $staff->activation = $request->has('activation') ? $request->activation : '0';

        if ($request->file('photo')) {
            $file = $request->file('photo');
            @unlink(public_path('uploads/staff/'.$staff->photo));
            $filename = 'staff-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/staff/'), $filename);
            $staff->photo = $filename;
        }

        $staff->save();

        $staff->roles()->detach();
        if ($request->role) {
            $staff->assignRole($request->role);
        }

        $notification = array(
            'message' => 'Staff updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('staff')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $staff = User::findOrFail($id);

        if (!is_null($staff)) {
            $staff->delete();
        }

        $imagePath = public_path('uploads/staff/'.$staff->photo);

        if (file_exists($imagePath)) {
            unlink($imagePath);
        }

        $notification = array(
            'message' => 'Staff deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
