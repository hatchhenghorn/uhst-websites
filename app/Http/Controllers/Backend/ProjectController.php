<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\ResearchInnovationImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PHPUnit\Exception;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $projects = Project::latest()->paginate(10);

        return view('backend.project.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.project.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // Wrap database operations within a transaction
        DB::beginTransaction();

        try {
            $request->validate([
                'title_en' => 'required|string|max:255',
                'title_kh' => 'required|string|max:255',
                'description' => 'required',
            ]);

            $project = new Project();

            $project->title_en = $request->title_en;
            $project->title_kh = $request->title_kh;
            $project->description = $request->description;
            $project->activation = $request->has('activation') ? $request->activation : '0';

            if ($request->file('image')) {
                $file = $request->file('image');
                $file_name = 'project-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
                $file->move(public_path('uploads/project/'), $file_name);
                $project->image = $file_name;
            }

            $project->save();

            // Handle uploaded images
            if ($request->file('sub_image')) {
                foreach ($request->file('sub_image') as $key => $image) {
                    $filename = 'project-img-'.date('YmdHi').$key.'.'.$image->getClientOriginalExtension();
                    $image->move(public_path('uploads/project/sub-images/'), $filename);

                    // Create the research & innovation image
                    $research_innovation_image = new ResearchInnovationImage();
                    $research_innovation_image->title = $request->title_en;
                    $research_innovation_image->description = $request->description;
                    $research_innovation_image->project_id = $project->id;
                    $research_innovation_image->image = $filename;

                    $research_innovation_image->save();
                }
            }

            // Commit the transaction if all operations succeed
            DB::commit();

            $notification = array(
                'message' => 'Data created successfully.',
                'alert-type' => 'success'
            );

            return redirect()->route('projects')->with($notification);
        } catch (Exception $exception) {
            DB::rollBack();

            $notification = array(
                'message' => 'Data create failed.'.$exception->getMessage(),
                'alert-type' => 'error'
            );

            return redirect()->back()->with($notification);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $project = Project::findOrFail($id);

        $project_image = ResearchInnovationImage::where('project_id', $id)->get();

        return view('backend.project.edit', compact('project', 'project_image'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        // Wrap database operations within a transaction
        DB::beginTransaction();

        try {
            $request->validate([
                'title_en' => 'required|string|max:255',
                'title_kh' => 'required|string|max:255',
                'description' => 'required',
            ]);

            $project = Project::findOrFail($id);

            $project->title_en = $request->title_en;
            $project->title_kh = $request->title_kh;
            $project->description = $request->description;
            $project->activation = $request->has('activation') ? $request->activation : '0';

            if ($request->file('image')) {
                $file = $request->file('image');
                @unlink(public_path('uploads/project/'.$project->image));
                $file_name = 'project-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
                $file->move(public_path('uploads/project/'), $file_name);
                $project->image = $file_name;
            }

            $project->save();

            // Handle uploaded images
            if ($request->file('sub_image')) {
                foreach ($request->file('sub_image') as $key => $image) {
                    $filename = 'project-img-'.date('YmdHi').$key.'.'.$image->getClientOriginalExtension();
                    $image->move(public_path('uploads/project/sub-images/'), $filename);

                    // Create the research & innovation image
                    $research_innovation_image = new ResearchInnovationImage();
                    $research_innovation_image->title = $request->title_en;
                    $research_innovation_image->description = $request->description;
                    $research_innovation_image->project_id = $project->id;
                    $research_innovation_image->image = $filename;

                    $research_innovation_image->save();
                }
            }

            // Commit the transaction if all operations succeed
            DB::commit();

            $notification = array(
                'message' => 'Data created successfully.',
                'alert-type' => 'success'
            );

            return redirect()->route('projects')->with($notification);
        } catch (Exception $exception) {
            DB::rollBack();

            $notification = array(
                'message' => 'Data create failed.'.$exception->getMessage(),
                'alert-type' => 'error'
            );

            return redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $project = Project::findOrFail($id);

        $image_path = public_path('uploads/project/'.$project->image);

        if (file_exists($image_path)) {
            unlink($image_path);
        }

        $research_innovation_image = ResearchInnovationImage::where('project_id', $id)->get();

        foreach ($research_innovation_image as $image) {
            $image_path2 = public_path('uploads/project/sub-images/'.$image->image);

            if (file_exists($image_path2)) {
                unlink($image_path2);
            }

            $image->delete();
        }

        $project->delete();

        $notification = array(
            'message' => 'Data deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroyImage(string $id)
    {
        $research_innovation_image = ResearchInnovationImage::findOrFail($id);

        $image_path = public_path('uploads/project/sub-images/'.$research_innovation_image->image);

        if (file_exists($image_path)) {
            unlink($image_path);
        }

        $research_innovation_image->delete();

        $notification = array(
            'message' => 'Image removed successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
