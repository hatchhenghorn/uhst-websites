<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ContactLocation;
use Illuminate\Http\Request;

class ContactLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $contact_locations = ContactLocation::latest()->paginate(10);

        return view('backend.contact-location.index', compact('contact_locations'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.contact-location.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'tel' => 'required|string|max:255',
            'location_map' => 'required|url|max:255',
            'location_description' => 'required',
        ]);

        $contact_location = new ContactLocation();

        $contact_location->name = $request->name;
        $contact_location->email = $request->email;
        $contact_location->tel = $request->tel;
        $contact_location->location_map = $request->location_map;
        $contact_location->location_description = $request->location_description;
        $contact_location->activation = $request->has('activation') ? $request->activation : '0';

        $contact_location->save();

        $notification = array(
            'message' => 'Data created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('contact-locations')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $contact_location = ContactLocation::findOrFail($id);

        return view('backend.contact-location.edit', compact('contact_location'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'tel' => 'required|string|max:255',
            'location_map' => 'required|url|max:255',
            'location_description' => 'required',
        ]);

        $contact_location = ContactLocation::findOrFail($id);

        $contact_location->name = $request->name;
        $contact_location->email = $request->email;
        $contact_location->tel = $request->tel;
        $contact_location->location_map = $request->location_map;
        $contact_location->location_description = $request->location_description;
        $contact_location->activation = $request->has('activation') ? $request->activation : '0';

        $contact_location->save();

        $notification = array(
            'message' => 'Data updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('contact-locations')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $contact_location = ContactLocation::findOrFail($id);

        $contact_location->delete();

        $notification = array(
            'message' => 'Data deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
