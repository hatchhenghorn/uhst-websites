<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Admission;
use Illuminate\Http\Request;

class AdmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $admissions = Admission::latest()->paginate(10);

        return view('backend.admission.index', compact('admissions'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.admission.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'description' => 'required'
        ]);

        $admission = new Admission();

        $admission->title_en = $request->title_en;
        $admission->title_kh = $request->title_kh;
        $admission->description = $request->description;
        $admission->activation = $request->has('activation') ? $request->activation : '0';

        $admission->save();

        $notification = array(
            'message' => 'Data created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('admissions')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $admission = Admission::findOrFail($id);

        return view('backend.admission.edit', compact('admission'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'description' => 'required'
        ]);

        $admission = Admission::findOrFail($id);

        $admission->title_en = $request->title_en;
        $admission->title_kh = $request->title_kh;
        $admission->description = $request->description;
        $admission->activation = $request->has('activation') ? $request->activation : '0';

        $admission->save();

        $notification = array(
            'message' => 'Data updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('admissions')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $admission = Admission::findOrFail($id);

        $admission->delete();

        $notification = array(
            'message' => 'Data deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
