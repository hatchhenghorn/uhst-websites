<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Career;
use App\Models\Faculty;
use App\Models\Partner;
use App\Models\PublicationBook;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $facultyCount = Faculty::where('activation', 1)->get()->count();
        $instituteCount = Faculty::where('activation', 1)->get()->count();
        $partnerCount = Partner::where('activation', 1)->get()->count();
        $publicationBookCount = PublicationBook::where('activation', 1)->get()->count();
        $careerCount = Career::where('activation', 1)->get()->count();

        return view('backend.dashboard.index', compact('facultyCount', 'instituteCount', 'partnerCount', 'publicationBookCount', 'careerCount'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
