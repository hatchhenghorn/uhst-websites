<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit()
    {
        $id = Auth::user()->id;
        $profileData = User::find($id);

        return view('profile.change-profile', compact('profileData'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $request->validate([
            'first_name_en' => 'required|string|max:255',
            'first_name_kh' => 'required|string|max:255',
            'last_name_en' => 'required|string|max:255',
            'last_name_kh' => 'required|string|max:255',
            'sex' => 'required|string',
            'dob' => 'required|date',
            'tel' => 'required|numeric',
        ]);

        $id = Auth::user()->id;
        $staff = User::findOrFail($id);

        $staff->first_name_en = $request->first_name_en;
        $staff->middle_name_en = $request->middle_name_en;
        $staff->last_name_en = $request->last_name_en;
        $staff->first_name_kh = $request->first_name_kh;
        $staff->middle_name_kh = $request->middle_name_kh;
        $staff->last_name_kh = $request->last_name_kh;
        $staff->sex = $request->sex;
        $staff->dob = date('Y-m-d', strtotime($request->dob));
        $staff->tel = $request->tel;
        $staff->email = $request->email;
        $staff->birth_place = $request->birth_place;
        $staff->present_address = $request->present_address;

        if ($request->file('photo')) {
            $file = $request->file('photo');
            @unlink(public_path('uploads/staff/'.$staff->photo));
            $filename = 'staff-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/staff/'), $filename);
            $staff->photo = $filename;
        }

        $staff->save();

        $notification = array(
            'message' => 'Profile updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    /**
     * Update the specified resource in storage.
     */
    public function editPassword()
    {
        return view('profile.change-password');
    }

    /**
     * Update the specified resource in storage.
     */
    public function updatePassword(Request $request)
    {
        $request->validate([
            'old_password' => 'required',
            'new_password' => 'required|confirmed'
        ]);

        if (!Hash::check($request->old_password, auth::user()->password)) {
            $notification = array(
                'message' => 'Old password does not match!',
                'alert-type' => 'error'
            );

            return redirect()->back()->with($notification);
        }

        User::whereId(auth()->user()->id)->update([
            'password' => Hash::make($request->new_password)
        ]);

        $notification = array(
            'message' => 'Password changed successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
