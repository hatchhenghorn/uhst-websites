<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\MissionVision;
use Illuminate\Http\Request;

class MissionVisionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $mission_visions = MissionVision::latest()->paginate(10);

        return view('backend.mission-vision.index', compact('mission_visions'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.mission-vision.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'mission' => 'required',
            'vision' => 'required'
        ]);

        $mission_vision = new MissionVision();

        $mission_vision->mission = $request->mission;
        $mission_vision->vision = $request->vision;
        $mission_vision->activation = $request->has('activation') ? $request->activation : '0';

        $mission_vision->save();

        $notification = array(
            'message' => 'Data created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('mission-visions')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $mission_vision = MissionVision::findOrFail($id);

        return view('backend.mission-vision.edit', compact('mission_vision'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'mission' => 'required',
            'vision' => 'required'
        ]);

        $mission_vision = MissionVision::findOrFail($id);

        $mission_vision->mission = $request->mission;
        $mission_vision->vision = $request->vision;
        $mission_vision->activation = $request->has('activation') ? $request->activation : '0';

        $mission_vision->save();

        $notification = array(
            'message' => 'Data updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('mission-visions')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $mission_vision = MissionVision::findOrFail($id);

        $mission_vision->delete();

        $notification = array(
            'message' => 'Data deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
