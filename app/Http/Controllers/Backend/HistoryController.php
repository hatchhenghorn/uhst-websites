<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\History;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $histories = History::latest()->paginate(10);

        return view('backend.history.index', compact('histories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.history.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'description' => 'required'
        ]);

        $history = new History();

        $history->title_en = $request->title_en;
        $history->title_kh = $request->title_kh;
        $history->description = $request->description;
        $history->activation = $request->has('activation') ? $request->activation : '0';

        $history->save();

        $notification = array(
            'message' => 'Data created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('histories')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $history = History::findOrFail($id);

        return view('backend.history.edit', compact('history'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'description' => 'required'
        ]);

        $history = History::findOrFail($id);

        $history->title_en = $request->title_en;
        $history->title_kh = $request->title_kh;
        $history->description = $request->description;
        $history->activation = $request->has('activation') ? $request->activation : '0';

        $history->save();

        $notification = array(
            'message' => 'Data updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('histories')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $history = History::findOrFail($id);

        $history->delete();

        $notification = array(
            'message' => 'Data deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
