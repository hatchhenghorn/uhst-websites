<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Conference;
use App\Models\ResearchInnovationImage;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $conferences = Conference::latest()->paginate(10);

        return view('backend.conference.index', compact('conferences'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.conference.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // Wrap database operations within a transaction
        DB::beginTransaction();

        try {
            $request->validate([
                'title_en' => 'required|string|max:255',
                'title_kh' => 'required|string|max:255',
                'sub_title_en' => 'required|string|max:255',
                'sub_title_kh' => 'required|string|max:255',
                'start_date' => 'required',
                'end_date' => 'required',
                'description' => 'required',
            ]);

            // Create the training workshop
            $conference = new Conference();

            $conference->title_en = $request->title_en;
            $conference->title_kh = $request->title_kh;
            $conference->sub_title_en = $request->sub_title_en;
            $conference->sub_title_kh = $request->sub_title_kh;
            $conference->start_date = date('Y-m-d', strtotime($request->start_date));
            $conference->end_date = date('Y-m-d', strtotime($request->end_date));
            $conference->description = $request->description;
            $conference->activation = $request->has('activation') ? $request->activation : '0';

            if ($request->file('image')) {
                $file = $request->file('image');
                $filename = 'conference-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
                $file->move(public_path('uploads/conference/'), $filename);
                $conference->image = $filename;
            }

            $conference->save();

            // Handle uploaded images
            if ($request->file('sub_image')) {
                foreach ($request->file('sub_image') as $key => $image) {
                    $filename = 'conference-img-'.date('YmdHi').$key.'.'.$image->getClientOriginalExtension();
                    $image->move(public_path('uploads/conference/sub-images/'), $filename);

                    // Create the research & innovation image
                    $research_innovation_image = new ResearchInnovationImage();
                    $research_innovation_image->title = $request->title_en;
                    $research_innovation_image->description = $request->description;
                    $research_innovation_image->conference_id = $conference->id;
                    $research_innovation_image->image = $filename;

                    $research_innovation_image->save();
                }
            }

            // Commit the transaction if all operations succeed
            DB::commit();

            $notification = array(
                'message' => 'Data created successfully.',
                'alert-type' => 'success'
            );

            return redirect()->route('conferences')->with($notification);
        } catch (Exception $exception) {
            // Rollback the transaction if an error occurs
            DB::rollBack();

            $notification = array(
                'message' => 'Data create failed.'.$exception->getMessage(),
                'alert-type' => 'error'
            );

            return redirect()->back()->with($notification);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $conference = Conference::findOrFail($id);

        $conference_image = ResearchInnovationImage::where('conference_id', $id)->get();

        return view('backend.conference.edit', compact('conference', 'conference_image'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        // Wrap database operations within a transaction
        DB::beginTransaction();

        try {
            $request->validate([
                'title_en' => 'required|string|max:255',
                'title_kh' => 'required|string|max:255',
                'sub_title_en' => 'required|string|max:255',
                'sub_title_kh' => 'required|string|max:255',
                'start_date' => 'required',
                'end_date' => 'required',
                'description' => 'required',
            ]);

            // Create the training workshop
            $conference = Conference::findOrFail($id);

            $conference->title_en = $request->title_en;
            $conference->title_kh = $request->title_kh;
            $conference->sub_title_en = $request->sub_title_en;
            $conference->sub_title_kh = $request->sub_title_kh;
            $conference->start_date = date('Y-m-d', strtotime($request->start_date));
            $conference->end_date = date('Y-m-d', strtotime($request->end_date));
            $conference->description = $request->description;
            $conference->activation = $request->has('activation') ? $request->activation : '0';

            if ($request->file('image')) {
                $file = $request->file('image');
                @unlink(public_path('uploads/conference/'.$conference->image));
                $filename = 'conference-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
                $file->move(public_path('uploads/conference/'), $filename);
                $conference->image = $filename;
            }

            $conference->save();

            // Handle uploaded images
            if ($request->file('sub_image')) {
                foreach ($request->file('sub_image') as $key => $image) {
                    $filename = 'conference-img-'.date('YmdHi').$key.'.'.$image->getClientOriginalExtension();
                    $image->move(public_path('uploads/conference/sub-images/'), $filename);

                    // Create the research & innovation image
                    $research_innovation_image = new ResearchInnovationImage();
                    $research_innovation_image->title = $request->title_en;
                    $research_innovation_image->description = $request->description;
                    $research_innovation_image->conference_id = $conference->id;
                    $research_innovation_image->image = $filename;

                    $research_innovation_image->save();
                }
            }

            // Commit the transaction if all operations succeed
            DB::commit();

            $notification = array(
                'message' => 'Data updated successfully.',
                'alert-type' => 'success'
            );

            return redirect()->route('conferences')->with($notification);
        } catch (Exception $exception) {
            // Rollback the transaction if an error occurs
            DB::rollBack();

            $notification = array(
                'message' => 'Data update failed.'.$exception->getMessage(),
                'alert-type' => 'error'
            );

            return redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $conference = Conference::findOrFail($id);

        $image_path = public_path('uploads/conference/'.$conference->image);

        if (file_exists($image_path)) {
            unlink($image_path);
        }

        $research_innovation_image = ResearchInnovationImage::where('conference_id', $id)->get();

        foreach ($research_innovation_image as $image) {
            $image_path2 = public_path('uploads/conference/sub-images/'.$image->image);

            if (file_exists($image_path2)) {
                unlink($image_path2);
            }

            $image->delete();
        }

        $conference->delete();

        $notification = array(
            'message' => 'Data deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroyImage(string $id)
    {
        $research_innovation_image = ResearchInnovationImage::findOrFail($id);

        $image_path = public_path('uploads/conference/sub-images/'.$research_innovation_image->image);

        if (file_exists($image_path)) {
            unlink($image_path);
        }

        $research_innovation_image->delete();

        $notification = array(
            'message' => 'Image removed successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
