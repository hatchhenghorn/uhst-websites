<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\PresidentMessage;
use Illuminate\Http\Request;

class PresidentMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $president_messages = PresidentMessage::latest()->paginate(10);

        return view('backend.president-message.index', compact('president_messages'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.president-message.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required'
        ]);

        $president_message = new PresidentMessage();

        $president_message->description = $request->description;
        $president_message->activation = $request->has('activation') ? $request->activation : '0';

        $president_message->save();

        $notification = array(
            'message' => 'Data created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('president-messages')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $president_message = PresidentMessage::findOrFail($id);

        return view('backend.president-message.edit', compact('president_message'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'description' => 'required'
        ]);

        $president_message = PresidentMessage::findOrFail($id);

        $president_message->description = $request->description;
        $president_message->activation = $request->has('activation') ? $request->activation : '0';

        $president_message->save();

        $notification = array(
            'message' => 'Data updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('president-messages')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $president_message = PresidentMessage::findOrFail($id);

        $president_message->delete();

        $notification = array(
            'message' => 'Data updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
