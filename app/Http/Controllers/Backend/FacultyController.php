<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Faculty;
use Illuminate\Http\Request;

class FacultyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $faculties = Faculty::latest()->paginate(10);

        return view('backend.faculty.index', compact('faculties'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.faculty.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'description' => 'required',
        ]);

        $faculty = new Faculty();

        $faculty->title_en = $request->title_en;
        $faculty->title_kh = $request->title_kh;
        $faculty->description = $request->description;
        $faculty->activation = $request->has('activation') ? $request->activation : '0';

        if ($request->file('image')) {
            $file = $request->file('image');
            $file_name = 'faculty-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/faculty/'), $file_name);
            $faculty->image = $file_name;
        }

        $faculty->save();

        $notification = array(
            'message' => 'Data created successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('faculties')->with($notification);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $faculty = Faculty::findOrFail($id);

        return view('backend.faculty.edit', compact('faculty'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title_en' => 'required|string|max:255',
            'title_kh' => 'required|string|max:255',
            'description' => 'required',
        ]);

        $faculty = Faculty::findOrFail($id);

        $faculty->title_en = $request->title_en;
        $faculty->title_kh = $request->title_kh;
        $faculty->description = $request->description;
        $faculty->activation = $request->has('activation') ? $request->activation : '0';

        if ($request->file('image')) {
            $file = $request->file('image');
            @unlink(public_path('uploads/faculty/'.$faculty->image));
            $file_name = 'faculty-img-'.date('YmdHi').'.'.$file->getClientOriginalExtension();
            $file->move(public_path('uploads/faculty/'), $file_name);
            $faculty->image = $file_name;
        }

        $faculty->save();

        $notification = array(
            'message' => 'Data updated successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('faculties')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $faculty = Faculty::findOrFail($id);

        $image_path = public_path('uploads/faculty/'.$faculty->image);

        if (file_exists($image_path)) {
            unlink($image_path);
        }

        $faculty->delete();

        $notification = array(
            'message' => 'Data deleted successfully.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
