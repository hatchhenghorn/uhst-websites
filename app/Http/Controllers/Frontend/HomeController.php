<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Faculty;
use App\Models\Institute;
use App\Models\Partner;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $faculty = Faculty::where('activation', 1)->get();
        $institute = Institute::where('activation', 1)->get();
        $partner = Partner::where('activation', 1)->get();

        return view('frontend.home.index', compact('faculty', 'institute', 'partner'));
    }
}
