<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\ContactLocation;
use Illuminate\Http\Request;

class ContactLocationController extends Controller
{
    public function index()
    {
        $contactLocation = ContactLocation::where('activation', 1)->get();

        return view('frontend.contact-location.index', compact('contactLocation'));
    }
}
