<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\PublicationBook;
use Illuminate\Http\Request;

class PublicationBookController extends Controller
{
    public function index()
    {
        $publicationBook = PublicationBook::where('activation', 1)->get();

        return view('frontend.publication-book.index', compact('publicationBook'));
    }
}
