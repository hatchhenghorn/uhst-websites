<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\NewsActivities;
use Illuminate\Http\Request;

class NewsActivitiesController extends Controller
{
    public function index()
    {
        $newsActivity = NewsActivities::where('activation', 1)->get();

        return view('frontend.news-activity.index', compact('newsActivity'));
    }
}
