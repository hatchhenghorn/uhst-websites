<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\TrainingWorkshop;
use Illuminate\Http\Request;

class TrainingWorkshopController extends Controller
{
    public function index()
    {
        $trainingWorkshop = TrainingWorkshop::where('activation', 1)->get();

        return view('frontend.training-workshop.index', compact('trainingWorkshop'));
    }
}
