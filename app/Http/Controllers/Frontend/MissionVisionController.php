<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\MissionVision;
use Illuminate\Http\Request;

class MissionVisionController extends Controller
{
    public function index()
    {
        $missionVision = MissionVision::where('activation', 1)->get();

        return view('frontend.mission-vision.index', compact('missionVision'));
    }
}
