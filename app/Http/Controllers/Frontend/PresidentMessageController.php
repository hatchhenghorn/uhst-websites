<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\PresidentMessage;
use Illuminate\Http\Request;

class PresidentMessageController extends Controller
{
    public function index()
    {
        $presidentMessage = PresidentMessage::where('activation', 1)->get();

        return view('frontend.president-message.index', compact('presidentMessage'));
    }
}
