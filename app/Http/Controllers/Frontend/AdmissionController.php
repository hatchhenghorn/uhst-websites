<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Admission;
use Illuminate\Http\Request;

class AdmissionController extends Controller
{
    public function index()
    {
        $admission = Admission::where('activation', 1)->get();

        return view('frontend.admission.index', compact('admission'));
    }
}
