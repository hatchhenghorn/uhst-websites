<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Conference;
use Illuminate\Http\Request;

class ConferenceController extends Controller
{
    public function index()
    {
        $conference = Conference::where('activation', 1)->get();

        return view('frontend.conference.index', compact('conference'));
    }
}
