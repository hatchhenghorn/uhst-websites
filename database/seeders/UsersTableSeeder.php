<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            [
                'first_name_en' => 'Chhenghorn',
                'last_name_en' => 'Hat',
                'tel' => '099707101',
                'email' => 'hatchhenghorn@gmail.com',
                'password' => Hash::make('1234'),
                'role' => 'admin',
                'staff_type_id' => 1,
                'faculty_id' => 1,
                'activation' => 1
            ]
        ]);
    }
}
