<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('publication_books', function (Blueprint $table) {
            $table->id();
            $table->string('title_en')->nullable();
            $table->string('title_kh')->nullable();
            $table->string('image')->nullable();
            $table->text('url')->nullable();
            $table->date('date_published')->nullable();
            $table->string('author')->nullable();
            $table->string('doi')->nullable();
            $table->text('description')->nullable();
            $table->boolean('activation')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('publication_books');
    }
};
