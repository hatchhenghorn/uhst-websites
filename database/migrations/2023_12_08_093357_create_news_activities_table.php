<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('news_activities', function (Blueprint $table) {
            $table->id();
            $table->string('title_en')->nullable();
            $table->string('title_kh')->nullable();
            $table->string('sub_title_en')->nullable();
            $table->string('sub_title_kh')->nullable();
            $table->dateTime('date_time')->nullable();
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->boolean('activation')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('news_activities');
    }
};
