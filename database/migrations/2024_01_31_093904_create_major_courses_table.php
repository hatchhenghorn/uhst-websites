<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('major_courses', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->foreignId('major_id')->constrained('majors');
            $table->foreignId('course_id')->constrained('courses');
            $table->integer('credit')->nullable();
            $table->integer('year')->nullable();
            $table->integer('semester')->nullable();
            $table->text('description')->nullable();
            $table->boolean('activation')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('major_courses');
    }
};
