<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name_en', 50)->nullable();
            $table->string('middle_name_en', 50)->nullable();
            $table->string('last_name_en', 50)->nullable();
            $table->string('first_name_kh', 50)->nullable();
            $table->string('middle_name_kh', 50)->nullable();
            $table->string('last_name_kh', 50)->nullable();
            $table->char('sex', 10)->nullable();
            $table->date('dob')->nullable();
            $table->string('tel', 20)->nullable();
            $table->string('photo')->nullable();
            $table->string('email')->unique();
            $table->string('birth_place', 300)->nullable();
            $table->string('present_address', 300)->nullable();
            $table->enum('role', ['admin', 'manager', 'editor'])->default('manager');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('activation')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
